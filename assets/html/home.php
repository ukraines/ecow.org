
<div class="container m-container">
<!-- Tiles -->
<div class="row" id="tiles">
    <div class="row11 companies">
        <!-- col-12 col-md-4 col-sm-6 -->
		
        <?php 
			if($isAdmin){
				include_once  PARTS_ADMIN_DIR."/filtersCounters.php"; 
			}else{
				echo "<div class='' id='cntPagesClient'></div>";
			}
			
			if (isset($companies) && is_array($companies)) {?>
        <script>
        $().ready(function(){
           updatePager(<?=$pages_number?>,1);
        });
        </script>
            <?php 
			global $lang_code;
					
			foreach($companies as $company_card){
            ?>
      <div class="div-tile" >
            <div class="tile" <?echo "id = 'adt_{$company_card['company_id']}'"; if($company_card['isblocked'] && $isAdmin) echo "style = 'background-color:#ddd' "; ?>>
				<?
					global $isAdmin;
					if($isAdmin){
				?>		
						<div onclick = "openDl(this)" class="btn-mc-nav btn-mc-nav-mini">                   
							</div>
						<ul class="mc-nav mc-nav-mini mc-close">
								<?
									$companyName = str_replace("\"","",$company_card['short_name']);
									if($company_card['isblocked'])
										echo "<li><a onclick = 'unBlockCompanies(".$company_card['company_id'].")' href='javascript:void(0)'>Разблокировать</a></li>";
									else
										echo "<li><a onclick ='blockCompanies({$company_card['company_id']})' href='javascript:void(0)'>Заблокировать</a></li>";
									echo "<li><a onclick ='deleteCompanies({$company_card['company_id']})' href='javascript:void(0)'>Удалить</a></li>";
									if($company_card['status'] == WAIT_STATUS){
										echo "<li><a onclick = 'setBadCompanies(".$company_card['company_id'].")' href='javascript:void(0)'>Отклонить</a></li>";
										echo "<li><a onclick ='setGoodCompanies({$company_card['company_id']})' href='javascript:void(0)'>Принять</a></li>";
									}
									else if($company_card['status'] == BAD_STATUS)
										echo "<li><a onclick ='setGoodCompanies({$company_card['company_id']})' href='javascript:void(0)'>Принять</a></li>";
								?>
								
								<li><a href="<?echo $lang_code.$company_card['check_details_url']?>">Уточнить</a></li>
						 </ul>
					<?}?>
                <a href="<?=$lang_code.$company_card['company_url']?>" class="a-firm-cart">
                    <span id="c_data_shortname_<?=$company_card['company_id']?>" class="t-title"><?=$company_card['short_name']?></span>
                    <span class="t-addr"><?=$company_card['address']?></span>
                    <span class="t-tel"><?=$company_card['phone']?></span>
                </a>
				<?
					if(!empty($company_card['manager'])){
					echo "
						<p class='send-params'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a href='$lang_code{$company_card['send_parameters_url']}'>$lang_send_value</a></p>
						<p class='send-message'><i class='fa fa-envelope'></i>&nbsp;&nbsp;<a href='$lang_code{$company_card['request_url']}'>$lang_send_email</a></p>
						<p class='send-params hide'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a class = 'checkd' href='$lang_code{$company_card['check_details_url']}'>$lang_send_details</a></p>";
					}else{
						echo "<p class='send-params'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a class = 'checkd' href='$lang_code{$company_card['check_details_url']}'>$lang_send_details</a></p>";
					}
					if($isAdmin){
						$stat = "Активна";
						switch( $company_card['status']){
							case WAIT_STATUS:
								$stat = "Непроверена";
								break;
							case BAD_STATUS:
								$stat = "Отклонена";
								break;
						}
						if($company_card['isblocked'])
							$stat = "Заблокирована";
						
						echo "
						<p>Просмотров: {$company_card[viewed]}</p>
						<p>Отправлено показаний: {$company_card[sent_request]}</p>
						<p>Отправлено сообщений: {$company_card[sent_values]}</p>
						<div style='text-align:right;'>
						<label>
							<span class='company_status'>$stat </span>
							<input class='check-action' dt_id='{$company_card['company_id']}' type='checkbox'/>
						</label>
						</div>
						";
						
					}
				?>
            </div>
        </div>
        <?php }} 
			else if(!$isAdmin) 
				echo "<div class='' id='cntPagesClientEmptyStart'>В этом регионе управляющих компаний не найдено. Попробуйте изменить параметры поиска выше или же добавить компанию вручную</div>";
			else
				echo "<div style = 'position:absolute; top:170px;' id='cntPagesEmptyStart'>Ничего не найдено</div>";
		?>
		
        <!-- Don't touch! -->
        <div class="div-tile helper"></div>
        <div class="div-tile helper"></div>
        <div class="div-tile helper"></div>
        <!-- Don't touch! -->
    </div>
</div>
<!-- /Tiles -->
<nav><ul class="pagination"></ul></nav>
</div>
<script>
    //var updateCities = true;
</script>

<?
if(!$isAdmin){
		include_once  PARTS_USER_DIR."/companyTemplate.php";
	}else{
		include_once  PARTS_ADMIN_DIR."/companyTemplate.php"; 
	}

?>