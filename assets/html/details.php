<div id = "item_wrap" class="container m-container m-container-form">
        <a href="<? global $lang_code; echo $lang_code. $company['company_url'] ?>" class="a-go-back"><?= $lang_back_company ?></a>
        
        <!--login modal-->

<!-- STEP1 FORM --->
    <div id="loginModal" class="modal params-form ext-step1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header ext-data-header">
		  <?php if (isset($error)) {?>
                    <div class="alert <?=$error_type?>" role="alert"><?=$error?></div>
                    <?php }?>
          <img src="<?echo SITE_URL;?>assets/img/ext-logo.png" class="modal-header-logo-img">
		  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?echo SITE_URL;?>assets/js/script_details.js?<?= RANDI ?>"></script>
          <h1 class="text-center h-title">Вот точные данные</h1>
      </div>
      <div class="modal-body">
			<?
				global $isAdmin;
				if($isAdmin){
					$name = $company['name'];
					$short_name = $company['short_name'];
					$site = $company['site'];
					$email = $company['email'];
					if($site == "Нет данных")
						$site = "";
					if($email == "Нет данных")
						$email = "";
				}else {
					$name = "";
					$short_name = "";
					$site = "";
					$email = "";
				}
				
				$address = $company['address'];
				$manager = $company['manager'];
				$phone = $company['phone'];
					
				//print_r($company);
			?>
          <form class="form col-md-12 center-block" action="<?=$company['check_details_url']?>">
            <div class="form-group">
              <input id = "manager" class="form-control input-lg" placeholder="<?= $lang_fullname ?>" value="<?= $manager ?>" type="text">
            </div>
            <div class="form-group">
              <input id = "address" class="form-control input-lg" placeholder="<?= $lang_address ?>" value="<?= $address ?>" type="text">
            </div>
            <div class="form-group">
              <input id = "email" type="email" class="form-control input-lg" placeholder="<?= $lang_email_translated ?>" value="<?= $email ?>" type="text">
            </div>
            <div class="form-group">
              <input id = "phone" class="form-control input-lg" placeholder="<?= $lang_phone ?>" value="<?= $phone ?>" type="text">
            </div>
			<?
				if($isAdmin){
			?>
					<div class="form-group">
					  <input id = "name" class="form-control input-lg" placeholder="<?= "Название" ?>" value='<?= $name ?>' type="text">
					</div>
					<div class="form-group">
					  <input id = "short_name" class="form-control input-lg" placeholder="<?= "Короткое название" ?>" value='<?= $short_name ?>' type="text">
					</div>
					<div class="form-group">
					  <input id = "site" class="form-control input-lg" placeholder="<?= "Сайт" ?>" value="<?= $site ?>" type="text">
					</div>
					<div class="form-group">
					  <input type="checkbox" id = "agree" class="hide" checked="checked">
					</div>
					<div class="form-group">
					  <button onclick="sendDetails('Admin',<?echo $company['company_id']?>); adminCrossStep();" type="button" class="btn btn-primary btn-lg btn-block"><?= $lang_send ?></button>
					</div>
			<?
				}
				else{
			?>
					<div class="form-group">
					  <input id = "name_sender" class="form-control input-lg" placeholder="<?= $name_sender ?>" value="" type="text" required>
					</div>
					<div class="form-group">
					  <input id = "mail_sender" type="email" class="form-control input-lg" placeholder="<?= $mail_sender ?>" value="" type="text" required>
					</div>
					<div class="form-group checkbox ext-agree">
					  <label class="l-rules-agree-checkbox"><input type="checkbox" name="agree" <?=isset($data['agree'])?'checked="checked"':''?> id="agree" value=""></label><label for="agree" class="lac"><?= $lang_agree_with ?> <a href="agreement"><b><?= $lang_agree_conditions ?></b></a></label>
					</div>
					<div class="form-group">
					  <button type="button" class="btn btn-primary btn-lg btn-block btn-ext-s1"><?= $lang_send ?></button>
					</div>
			<?
				}
			?>
			
            
          </form>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
 
		  </div>	
      </div>
      </div>
      </div>
  </div>
<!-- /STEP1 FORM --->


  <!-- STEP2 FORM -->
  
      <div id="loginModal" class="modal params-form ext-step2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header ext-data-header">

          <img src="<?echo SITE_URL;?>assets/img/ext-s12-logo.png" class="modal-header-logo-img">
          <h1 class="text-center h-title">Спасибо!</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block">
                <p>
                    Укажите пожалуйста кто Вы, <br> 
                    директор или клиент
                </p>
                
                <div class="col-md-12 ext-s2-btns">
                    <div onclick="sendDetails('Director',<?echo $company['company_id']?>)" class="col-md-6 ext-s2-btn1"><span>Я директор</span></div>
                    <div onclick="sendDetails('Client',<?echo $company['company_id']?>)" class="col-md-6 ext-s2-btn2"><span>Я клиент</span></div>
                </div>
                <div style="clear: both"></div>
          </form>
      </div>
      <div class="modal-footer">

      </div>
      <div style="clear: both"></div>
      </div>
      </div>
  </div>
  
  <!-- /STEP2 FORM -->
  
    <!-- STEP3 FORM -->
  
      <div id="loginModal" class="modal params-form ext-step3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header ext-data-header">

          <img src="<?echo SITE_URL;?>assets/img/ext-s12-logo.png" class="modal-header-logo-img">
          <h1 class="text-center h-title">Спасибо!</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block">
                <p>
                   Благодарим Вас за сотрудничество! <br> 
                   Ваши данные будут переданы куда нужно <br>
                   <span>*зловещий смех*</span>
                   <img src="<?echo SITE_URL;?>assets/img/step2-cat.jpg" alt="cat">
                </p>

                <div style="clear: both"></div>
          </form>
      </div>
      <div class="modal-footer">
                <?
					echo "<a href='$lang_code{$company['company_url']}' class='col-xs-12 ext-s3-btn'>";
				?>
Вернуться на карточку предприятия
                </a>
      </div>
      <div style="clear: both"></div>
      </div>
      </div>
  </div>
  
  <!-- /STEP3 FORM -->
   </div>
        <!-- /m-container -->