<div id = "item_wrap" class="container m-container m-container-form">

    <!--login modal-->
    <div id="loginModal" class="modal show params-form" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <?php if (isset($error)) {?>
                    <div class="alert <?=$error_type?>" role="alert"><?=$error?></div>
                    <?php }?>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <img src="assets/img/logo-request.jpg" class="modal-header-logo-img">-->
                    <h1 class="text-center h-title"><?=$lang_contacts; ?></h1>
                </div>
                <div class="modal-body">
                    <form method="post" class="form col-md-12 center-block" action="contacts">
                        <div class="form-group">
                            <input name="fullname" class="form-control input-lg" placeholder="<?= $lang_fullname ?>" type="text" required/>
                        </div>
                        <div class="form-group">
                            <input name="email" class="form-control input-lg" placeholder="<?= $lang_email_translated ?>" type="email" required/>
                        </div>
                        <div class="form-group">
                            <input name="phone" class="form-control input-lg" placeholder="<?= $lang_phone ?>" type="text"/>
                        </div>

                        <div class="form-group">
                            <textarea required="" name="message" class="form-control input-lg" placeholder="<?= $lang_your_message ?>" type="text"></textarea>
                        </div>
                        
                        <div class="form-group checkbox">
                            <label class="l-rules-agree-checkbox"><input name="agree" <?=isset($data['agree'])?'checked="checked"':''?> type="checkbox" id="rules-agree-checkbox" value="1" required=""></label><label for="rules-agree-checkbox" class="lac"><?= $lang_agree_with ?> <a href="agreement"><?= $lang_agree_conditions ?></a></label>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary btn-lg btn-block"><?= $lang_send ?></button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">

                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /m-container -->