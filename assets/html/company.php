<div id = "item_wrap" class="container m-container m-container-form">
	<?
		$parts = explode("/",$company['company_url']);
		$c_url = SITE_URL . $lang_code;
		$left = 1;
		if(strpos($company['company_url'],"/c/") !== false)
			$left = 2;
		for($i=0; $i<count($parts)-$left; $i++){
			$c_url .= $parts[$i]."/";
		}
		
	?>
    <a href="<?= $c_url ?>" class="a-go-back"><?= $lang_back_companies_list ?></a>
    <?php if (isset($company['company_id'])) { ?>
	
        <!-- Cart -->
        <div class="container-fluid">
            <div class="cart-block">
                <div class="row-fluid cart-main-block" <? if($company['isblocked'] && $isAdmin) echo "style = 'background-color:#ddd' "; ?>>
					<div class="btn-mc-nav ">                   
                    </div>
				   <ul class="mc-nav mc-close">
						<?
							global $isAdmin;
							if($isAdmin){
								if($company['isblocked'])
										echo "<li><a onclick = 'unBlockCompanies(".$company['company_id'].")' href='javascript:void(0)'>Разблокировать</a></li>";
									else
										echo "<li><a onclick ='blockCompanies({$company['company_id']})' href='javascript:void(0)'>Заблокировать</a></li>";
							}
						?>
                       <li><a href="<?echo $lang_code.$company['check_details_url']?>">Уточнить</a></li>
                   </ul>
                    <div class="col-xs-12 col-sm-3 cart-logo">
                        <img src="<?echo SITE_URL;?>assets/img/cart-logo.jpg" alt="">
                    </div>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
					<!-- Include all compiled plugins (below), or include individual files as needed -->
					<script src="<?echo SITE_URL;?>assets/js/script_company.js?<?= RANDI ?>"></script>
                    <div class="col-xs-12 col-sm-9 org-contacts">
                        <div itemscope itemtype="http://schema.org/Organization"> 
                            <h3 id="c_data_shortname_<?=$company['company_id']?>" itemprop="name"><?= $company['short_name'] ?></h3> 
                            <p class="boss"><?= $lang_company_manager ?>: <?= $company['manager'] ?></p>
                            <?= $lang_located_at ?> 
                            <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <span itemprop="streetAddress"><?= $company['address'] ?></span>
                                <span class="hide" itemprop="addressLocality"><?= $company['region'] ?></span><?php if (strlen($company['city'])) {?>
                                <span class="hide" itemprop="addressRegion"><?= $company['city'] ?></span><?php }?>
                            </p>
                           <!--<img itemprop="logo" src="http://www.example.com/logo.png" />-->
                            <p class="tel" itemprop="telephone"><?= $company['phone'] ?></p>
                        </div>
                        <!--<h3><?= $company['short_name'] ?></h3>
                        
                        <p class="addr"><?= $company['country'] ?> - <?= $company['region'] ?> - <?= $company['city'] ?></p>
                        <p><?= $company['address'] ?></p>
                        <p class="tel"><?= $company['phone'] ?></p>
                        <?php if ($company['url']) { ?>
                            <a href="#" class="site"><?= $company['site'] ?></a>
                        <?php } ?>
                        -->

					</div><div style="clear:both"></div>
					<script type="text/javascript">(function() {												<!-- SHARE -->
					  if (window.pluso)if (typeof window.pluso.start == "function") return;
					  if (window.ifpluso==undefined) { window.ifpluso = 1;
						var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
						s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
						s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
						var h=d[g]('body')[0];
						h.appendChild(s);
					  }})();</script>
					 <div style="text-align:right; padding-right:30px;">
						<div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,counter,theme=04" data-services="vkontakte,facebook,twitter,google" data-user="1516627923"></div>
					</div>
				</div>
				 <div class="row-fluid row-cart-btns">
                    <div class="row-fluid btn btn-group">
						<?
							//print_r($company);
							global $lang_code;
							if(!empty($company['email'])){
								echo "
									<a href='$lang_code{$company['send_parameters_url']}' class='btn btn-default btn-lg col-xs-12 col-sm-6'><i class='fa fa-tint'></i>&nbsp;&nbsp;{$lang_send_value}</a>
									<a href='$lang_code{$company['request_url']}' class='btn btn-default btn-lg col-xs-12 col-sm-6'><i class='fa fa-envelope'></i>&nbsp;&nbsp;{$lang_send_request}</a>";
							}else{
								//echo "<a href='detail.php?c={$company['company_id']}'>SEND</a>"
								echo "<a href='$lang_code{$company['check_details_url']}' class='btn btn-default btn-lg col-xs-12'><i class='fa fa-tint'></i>&nbsp;&nbsp;{$lang_send_details}</a>";
								
							}
						?>
                    </div>
                </div>
            </div>
        </div>

        <!-- /Cart -->
    <?php } ?>

</div>

<!-- /m-container -->