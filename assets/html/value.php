<div class="row-fluid do_not_show_row">
    <div class="alert alert-error my-alert hidden-sm hidden-xs">
        <a href="#" class="close do_not_show" data-dismiss="alert"><?=$lang_do_not_show_anymore?></a>
        <?=  sprintf($lang_value_form_warning, $company['short_name'])?>
    </div>
</div>

<div id = "item_wrap" class="container m-container m-container-form">
    <a href="<?= $company['company_url'] ?>" class="a-go-back"><?=$lang_back_company?></a>

    <!--login modal-->
    <div id="loginModal" class="modal show params-form" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <?php if (isset($error)) {?>
                    <div class="alert <?=$error_type?>" role="alert"><?=$error?></div>
                    <?php }?>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
                    <span class="text-center h-tint"><i class="fa fa-tint"></i></span>
                    <h1 class="text-center h-title"><?=$lang_sending_value?></h1>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?global $lang_code; echo $lang_code.$company['send_parameters_url']?>" class="form col-md-12 center-block">
                        <div class="form-group">
                            <input name="fullname" value="<?=isset($data['fullname'])?$data['fullname']:''?>" class="form-control input-lg" placeholder="<?=$lang_fullname?>" type="text" required />
                        </div>
                        <div class="form-group">
                            <input name="address" value="<?=isset($data['address'])?$data['address']:''?>" class="form-control input-lg" placeholder="<?=$lang_address?>" type="text" required>
                        </div>
                        <div class="form-group">
                            <input name="email" value="<?=isset($data['email'])?$data['email']:''?>" class="form-control input-lg" placeholder="<?=$lang_email_translated?>" type="text" required>
                        </div>
                        <div class="form-group">
                            <input name="phone" value="<?=isset($data['phone'])?$data['phone']:''?>" class="form-control input-lg" placeholder="<?=$lang_phone?>" type="text" required>
                        </div>
                        <div class="row row-padding-0 form-group">
                            <div class="btn-g-plus col-xs-1 col-sm-2">
                                <button type="button" class="btn btn-default btn-plus">+</button>
                            </div>
                            <div class="input-g-plus col-sm-10 col-xs-11">
                                <input name="valueshot[]" value="<?=isset($data['valueshot'][0])?$data['valueshot'][0]:''?>" class="form-control input-lg input-plus input-hot" placeholder="<?=$lang_hot_water_value?>" type="text" required>
                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <div class="row row-padding-0 form-group">
                            <div class="btn-g-plus col-xs-1 col-sm-2">
                                <button type="button" class="btn btn-default btn-plus">+</button>
                            </div>
                            <div class="input-g-plus col-sm-10 col-xs-11">
                                <input name="valuescold[]" value="<?=isset($data['valuescold'][1])?$data['valuescold'][1]:''?>" class="form-control input-lg input-plus input-cold" placeholder="<?=$lang_cold_water_value?>" type="text" required>
                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <?php if (isset($data['values']) && is_array($data['values']) && count($data['values']) > 2) {
                                $n = count($data['values']);
                                for($i = 2; $i < $n; $i++) {?>
                        <div class="row row-padding-0 form-group">
                            <div class="btn-g-plus col-xs-1 col-sm-2">
                                <button type="button" class="btn btn-default btn-plus btn-plus-extension">-</button>
                            </div>
                            <div class="input-g-plus col-sm-10 col-xs-11">
                                <input name="values[]" value="<?=$data['values'][$i]?>" class="form-control input-lg input-plus input-cold" placeholder="<?=$lang_cold_water_value?>" type="text" required>
                            </div>
                            <div style="clear: both"></div>
                        </div>
                            <?php }
                        }?>

                        <div class="form-group">
                            <input name="comment" value="<?=isset($data['comment'])?$data['comment']:''?>" class="form-control input-lg" placeholder="<?=$lang_your_comment?>" type="text">
                        </div>

                        <div class="form-group checkbox">
                            <label class="l-rules-agree-checkbox"><input name="agree" <?=isset($data['agree'])?'checked="checked"':''?> type="checkbox" id="rules-agree-checkbox" value="1" required=""></label><label for="rules-agree-checkbox" class="lac"><?= $lang_agree_with ?> <a href="agreement"><?= $lang_agree_conditions ?></a></label>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary btn-lg btn-block"><?=$lang_send?></button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">

                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /m-container -->