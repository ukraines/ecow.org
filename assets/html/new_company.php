<div id = "item_wrap" class="container m-container m-container-form">
        
        <!--login modal-->

<!-- STEP1 FORM --->
    <div id="loginModal" class="modal params-form ext-step1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header ext-data-header">
		  <?php if (isset($error)) {?>
                    <div class="alert <?=$error_type?>" role="alert"><?=$error?></div>
                    <?php }?>
          <img src="<?echo SITE_URL;?>assets/img/ext-logo.png" class="modal-header-logo-img">
		  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?echo SITE_URL;?>assets/js/script_add_new.js?<?= RANDI ?>"></script>
          <h1 class="text-center h-title">Новая компания</h1>
      </div>
      <div class="modal-body">
			
          <form onsubmit = "return false;" class="form col-md-12 center-block" action="">
			<div class="form-group">
			  <input id = "name" name="fullname" class="form-control input-lg" placeholder="<?= "Название" ?>" value='' type="text" required>
			</div>
			<div class="form-group">
			  <input id = "short_name" class="form-control input-lg" placeholder="<?= "Короткое название" ?>" value='' type="text" required>
			</div>
			<div class="form-group add-sel-wrap">
				<select id="add-sel-country" class="add-select-country add-select" required>
					<option value="0"><?=$lang_select_country?></option>
					<?
						if($countries){
							foreach($countries as $c)
							 echo "<option value='{$c['country_id']}'>{$c['name']}</option>";
						}
					?>
				</select>
				<select id="add-sel-region" class="add-select-region add-select" required>
					<option value="0">Регион-область</option>
				</select>
				<select id="add-sel-city"  class="add-select-city add-select" required>
					<option value="0">Город</option>
				</select>
			</div>
			<div class="clear"></div>
			<div class="form-group">
              <input id = "address" class="form-control input-lg" placeholder="<?= $lang_address ?>" value="" type="text" required>
            </div>
            <div class="form-group">
              <input id = "email" type="email" class="form-control input-lg" placeholder="<?= $lang_email_translated ?>" value="" type="text">
            </div>
			 <div class="form-group">
              <input id = "manager" class="form-control input-lg" placeholder="<?= $lang_fullname_manager ?>" value="" type="text">
            </div>
            <div class="form-group">
              <input id = "phone" class="form-control input-lg" placeholder="<?= $lang_phone ?>" value="" type="text">
            </div>
			
			<div class="form-group">
			  <input id = "site" class="form-control input-lg" placeholder="<?= "Сайт" ?>" value="" type="text">
			</div>
			<div class="form-group">
				<input id = "comment" name="comment" value="<?=isset($data['comment'])?$data['comment']:''?>" class="form-control input-lg" placeholder="<?=$lang_your_comment?>" type="text">
			</div>
					
			<?
				if($isAdmin){
			?>
					<div class="form-group">
					  <input type="checkbox" id = "agree" class="hide" checked="checked">
					</div>
					<div class="form-group">
					  <button onclick="addCompany('Admin'); adminCrossStep();"  class="btn btn-primary btn-lg btn-block"><?= $lang_send ?></button>
					</div>
			<?
				}
				else{
			?>
					<div class="form-group">
					  <input id = "name_sender" class="form-control input-lg" placeholder="<?= $name_sender ?>" value="" type="text">
					</div>
					<div class="form-group">
					  <input id = "mail_sender" type="email" class="form-control input-lg" placeholder="<?= $mail_sender ?>" value="" type="text">
					</div>
					<div class="form-group checkbox ext-agree">
					  <label class="l-rules-agree-checkbox"><input type="checkbox" name="agree" <?=isset($data['agree'])?'checked="checked"':''?> id="agree" value=""></label><label for="agree" class="lac"><?= $lang_agree_with ?> <a href="agreement"><b><?= $lang_agree_conditions ?></b></a></label>
					</div>
					<div class="form-group">
					  <button type="button" class="btn btn-primary btn-lg btn-block btn-ext-s1"><?= $lang_send ?></button>
					</div>
			<?
				}
			?>
			
            
          </form>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
 
		  </div>	
      </div>
      </div>
      </div>
  </div>
<!-- /STEP1 FORM --->


  <!-- STEP2 FORM -->
  
      <div id="loginModal" class="modal params-form ext-step2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header ext-data-header">

          <img src="<?echo SITE_URL;?>assets/img/ext-s12-logo.png" class="modal-header-logo-img">
          <h1 class="text-center h-title">Спасибо!</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block">
                <p>
                    Укажите пожалуйста кто Вы, <br> 
                    директор или клиент
                </p>
                
                <div class="col-md-12 ext-s2-btns">
                    <div onclick="addCompany('Director')" class="col-md-6 ext-s2-btn1"><span>Я директор</span></div>
                    <div onclick="addCompany('Client')" class="col-md-6 ext-s2-btn2"><span>Я клиент</span></div>
                </div>
                <div style="clear: both"></div>
          </form>
      </div>
      <div class="modal-footer">

      </div>
      <div style="clear: both"></div>
      </div>
      </div>
  </div>
  
  <!-- /STEP2 FORM -->
  
    <!-- STEP3 FORM -->
  
      <div id="loginModal" class="modal params-form ext-step3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header ext-data-header">

          <img src="<?echo SITE_URL;?>assets/img/ext-s12-logo.png" class="modal-header-logo-img">
          <h1 class="text-center h-title">Спасибо!</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block">
                <p>
                   Благодарим Вас за сотрудничество! <br> 
                   Ваши данные будут переданы куда нужно <br>
                   <span>*зловещий смех*</span>
                   <img src="<?echo SITE_URL;?>assets/img/step2-cat.jpg" alt="cat">
                </p>

                <div style="clear: both"></div>
          </form>
      </div>
      <div class="modal-footer">
                <?
					echo "<a href='". SITE_URL ."' class='col-xs-12 ext-s3-btn'>";
				?>
Вернуться на главную
                </a>
      </div>
      <div style="clear: both"></div>
      </div>
      </div>
  </div>
  
  <!-- /STEP3 FORM -->
   </div>
        <!-- /m-container -->