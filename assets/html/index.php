<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?= isset($meta_description) ? $meta_description : '' ?>">
        <title><?= $title ?></title>
        <base href="<?= SITE_URL ?>">
        <link href="assets/css/lib.css?<?= RANDI ?>" rel="stylesheet">
        <link href="assets/css/style.css?<?= RANDI ?>" rel="stylesheet">
        <link href="assets/css/media.css?<?= RANDI ?>" rel="stylesheet">
		<script src="assets/js/lib.js?<?= RANDI ?>"></script>
		<?global $isAdmin; if($isAdmin){?>
			<link href="assets/css/admin.css?<?= RANDI ?>" rel="stylesheet">
		<?}?>
        <?php 
		if (SITE_MODE != 'DEV') { ?>
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-59846622-4', 'auto');
                ga('send', 'pageview');

            </script>
<?php } ?>
<script>
    var lang_select_country = '<?= $lang_select_country ?>';
    var lang_select_region = '<?= $lang_select_region ?>';
    var lang_select_city = '<?= $lang_select_city ?>';
	var lang_header_title_empty = '<?= $lang_header_title_empty ?>';
	var lang_select_all_regions = '<?= $lang_select_all_regions ?>';
	var lang_select_all_cities = '<?= $lang_select_all_cities ?>';
	var pathUrl = '<? global $lang_code; echo PATH_URL . $lang_code ?>';
	
</script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
		<?
			global $isAdmin;
			if($isAdmin){
				echo "<a style ='position:absolute; color:red; right:15px; top:10px; z-index:1000000; font-size:18px; cursor:pointer;' onclick = 'logOut();' href = 'javascript:void(0)'>Log out</a>";
				
			}
		?>
        <div class="container-fluid m-container-fluid">
            <div class="row" id="m-header">
                <a href="/" class="col-xs-2 col-sm-1 h-home"><i class="fa fa-home"></i></a>
                <div class="col-xs-10 col-sm-5 header_left"><?= $header_title ?></div>
                <div class="col-xs-12 col-sm-6 header_right">
                    <div class="search-form">
                        <button type="submit" class="btn btn-primary show_company"><span class='glyphicon glyphicon-search' aria-hidden='true'></span></button>
                        <input autocomplete="off" type="text" name="query" value="<?= isset($s) ? $s : '' ?>" class="q_search" placeholder="<?= $lang_search_company_name ?>" required/>
                        <ul class="suggest">
                        </ul>
                    </div>
                    <a href="#"  class="select_region"><span><?=isset($choosen_place)?$choosen_place:$lang_choose_region?></span>&nbsp;<i class="fa fa-angle-down"></i></a>
                    <!--<a href="#" class="btn-login">Вход</a>-->
                </div>
            </div> 
        </div>


        <div class="row-fluid">
            <div class="alert alert-error my-alert my-alert-loc" <?if(isset($_COOKIE['alert'])) if(!$_COOKIE['alert']) echo "style='display:none;'";?>>
                <a href="#" class="close-loc">x</a>

                <!-- Location -->
                <div class="row row-location">
                    <div class="col-xs-12 col-md-1" id="div-globe">
                        <button class="btn btn-primary btn-lg btn-glode"><i class="fa fa-globe"></i></button>
                    </div>
                    <div class="col-xs-12 col-md-4" id="div-country">
                        <div class="btn btn-group" id="country_dropdown" class="btn btn-group dropdown" data-value="<?= $country_id ?>">
                            <?php
                            if (is_array($countries)) {
                                $selected = $lang_select_country;
                                foreach ($countries as $v) {
                                    if ($v['country_id'] == $country_id) {
                                        $selected = $v['name'];
                                        break;
                                    }
                                    ?>
                                    <?php
                                    }
                                }
                                ?>
                            <button class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown"><span class="btn-title span-title-country"><?= $selected ?></span> <span class="caret"></span></button>
                            <ul id="country" class="dropdown-menu" data-value="">
                                <li class="li-padding-h20"><input type="text" class="my-country-text input-md" placeholder="<?= $lang_type_name ?>"></li>
                                <hr>
                                <?php
                                if (is_array($countries)) {
                                    foreach ($countries as $v) {
                                        $selected = '';
                                        if ($v['country_id'] == $country_id) {
                                            $selected = 'class="active"';
                                        }
                                        ?>
                                        <li <?= $selected ?>><a data-value="<?= $v['country_id'] ?>"><?= $v['name'] ?></a></li>
    <?php
    }
}
?>

                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div id="region_dropdown" class="btn btn-group" data-country_id="<?=$country_id?>" data-value="<?=$region_id?>">
                            <button class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown"><span class="btn-title span-title-region"><?= $selected_region ?></span> <span class="caret"></span></button>
                            <ul id="region" class="dropdown-menu">
                                <li class="li-padding-h20"><input type="text" class="my-country-text input-md" placeholder="<?= $lang_type_name ?>"></li>
                                <hr>
<?php
if (is_array($regions)) {
										$cc = isset($_COOKIE['country_id'])?intval($_COOKIE['country_id']):3159;
										echo "<li ><a id='all_regions_filter' data-country_id='$cc' data-value='0'>$lang_select_all_regions</a></li>";
    foreach ($regions as $v) {
        $selected = '';
                                        if ($v['region_id'] == $region_id) {
                                            $selected = 'class="active"';
                                        }
        ?>
                                        <li <?= $selected ?>><a data-country_id="<?= $v['country_id'] ?>" data-value="<?= $v['region_id'] ?>"><?= $v['name'] ?></a></li>
                                    <?php }
                                } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div id="city_dropdown" class="btn btn-group dropdown" data-country_id="<?=$country_id?>" data-region_id="<?=$region_id?>" data-value="<?=$city_id?>">
                            <button class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown"><span class="btn-title span-title-city"><?= $selected_city ?></span><span class="caret"></span></button>
                            <ul id="city" class="dropdown-menu">
                                <li class="li-padding-h20"><input type="text" class="my-country-text input-md" placeholder="<?= $lang_type_name ?>"></li>
                                <hr>
<?php
if (is_array($cities)) {
    $n = count($cities);
										$cc = isset($_COOKIE['country_id'])?intval($_COOKIE['country_id']):3159;
										$cc = isset($_COOKIE['region_id'])?intval($_COOKIE['region_id']):0;
										echo "<li ><a id='all_cities_filter' data-country_id='$cc' data-region_id='$rr' data-value='0'>$lang_select_all_cities</a></li>";
    foreach ($cities as $v) {
        $selected = '';
                                        if ($v['city_id'] == $city_id) {
                                            $selected = 'class="active"';
                                        }
        ?>
                                        <li <?= $selected ?>><a data-country_id="<?= $v['country_id'] ?>" data-region_id="<?= $v['region_id'] ?>" data-value="<?= $v['city_id'] ?>"><?= $v['name'] ?></a></li>
            <?php
            }
        }
        ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /Location -->

            </div>
        </div>

<?php include $page; if(strpos($page,"home.php") === false) { $isHide = true; include_once  PARTS_DIR."/companiesList.php"; }?>
        <!-- Footer
        <div class="navbar-bottom container-fluid" id="footer-fluid">
            <div class="container" id="footer">
                <div class="row">
                    <div class="col-sm-9 col-xs-12 footer-left">
                        <ul class="row footer-nav-left">
                            <li><a href="./">$lang_home ?></a></li>
                            <li><a href="about">$lang_about ?></a></li>
                            <li><a href="contacts">$lang_contacts ?></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3 col-xs-12 footer-right">
                    </div>
                </div>
            </div>
        </div>
        /Footer -->
        
        <!-- Footer -->
           <div class="navbar-bottom container-fluid" id="footer-fluid">
               <div class="container" id="footer">
                   <div class="row">
                       <div class="col-sm-9 col-xs-12 footer-left">
                           <ul class="row footer-nav-left">
                                <li><a href="./"><?= $lang_home ?></a></li>
                                <li><a href="about"><?= $lang_about ?></a></li>
                                <li><a href="contacts"><?= $lang_contacts ?></a></li>
								
                           </ul>
                       </div>
                       <div class="col-sm-3 col-xs-12 footer-right">

    <div class="btn-group dropup">
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle lang-drop"><?global $lang_now; echo $lang_now?> <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="<?echo $_SERVER['REQUEST_URI']?>" onclick="setLang('Молдовеняскэ');">Молдовеняскэ</a></li>
                <li><a href="<?echo $_SERVER['REQUEST_URI']?>" onclick="setLang('Русский');">Русский</a></li>
                <li><a href="<?echo $_SERVER['REQUEST_URI']?>" onclick="setLang('Українська');">Українська</a></li>
            </ul>
        </div>
    </div>
                       </div>
                   </div>
               </div>
           </div>
       <!-- /Footer -->
		<script src="assets/js/script.js?<?= RANDI ?>"></script>
        <script>
            var lang_show_more_results = '<?= $lang_show_more_results ?>';
            var lang_companies_search = '<?= $lang_companies_search ?>';
            var lang_header_title = '<?= $lang_header_title ?>';
        </script>
		<div id = "ipper" class="hide"><?global $ip; echo $ip;?></div>
		<div id = "isaadm" class="hide"><?global $isAdmin; if($isAdmin) echo "iiss";?></div>
		<div class="msg">
			<span>Действие выполнено!</span>
		</div>
		<?
			if($isAdmin){
		?>	
			<script src="assets/js/admin.js?<?= RANDI ?>"></script>
         
			<div class="adminPanel hide">
				<div class="block_close" onclick="admClose()">Close</div>
				<h3>AdminPanel</h3>
				<div class = "ttle"></div>
					
				<div class = "cmp_block hide">
					<div style='color:black;'>Причина блокировки:</div>
					<select id='block_why' name ='why'>
						<option value=''></option>
						<option value='not_exist'>Компания не существует</option>
						<option value='by_ask'>По просьбе компании</option>
					</select>
					<textarea  id='block_more' style='margin-top:10px;' type='text' placeholder='Другая причина'></textarea>
					<br>
					<div id="block_id" class="hide"></div>
					<div id="block_btn" onclick="blockCompany()" class="btn_adm">Заблокировать</div>
				</div>
			</div>
		<?		
			}
		?>
		<?if(!empty($_COOKIE['msg'])){ echo "<script>showMsg('".$_COOKIE['msg']."'); setCookie('msg','',86400*365);</script>"; }?>
	</body>
</html>
