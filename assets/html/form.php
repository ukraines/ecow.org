<div class="page-header">
                <h1>Форма отправки показаний счетчиков воды</h1>
            </div>
            <?php 
            $hidden = 'hidden';
            if (isset($error)) {
                $hidden = 'alert-warning';
            }
            ?>
            <div class="alert <?=$hidden?>"><?=isset($error)?$error:''?></div>
            <div class="well">Внимание! Форма активна и позволяет отправить данные счетчиков на емейлы <?= $company ?></div>

            <div class="row">
                <div class="col-md-9">
                    <br/><br/>
                    <?php
                    // Если статус успех
                    if (isset($error_msg['status']) && $error_msg['status'] == "1")

                    // То отображаем успех отправки данных
                        echo "<div class='error_msg' id='id" . $error_msg['status'] . "'>" . $error_msg['text'] . "</div><div class='emailContent'><p>Нами были получены такие данные</p> " . $error_msg['emailBody'] . "<p>Спасибо за сотрудничество!</p></div>";

                    else {?> 

                        <form method="post" class="form" role="form" action="<?= $url ?>">
                            <div class="form-group">
                                <label for="fullname" class="control-label">* Фамилия Имя Отчество:</label>
                                <input type="text" name="fullname" class="form-control" value="<?php getValueFor('fullname') ?>"/>
                            </div>

                            <div class="form-group">
                                <label for="address" class="control-label">* Адрес:</label>
                                <input type="text" name="address" class="form-control" value="<?php getValueFor('address') ?>" />
                            </div>

                            <div class="form-group">
                                <label for="email" class="control-label">* Адрес электропочты:</label>
                                <input type="text" id="input" name="email" class="form-control" value="<?php getValueFor('email') ?>" />
                            </div>

                            <div class="form-group">
                                <label for="phone" class="control-label">* Телефон:</label>
                                <input type="text" name="phone" class="form-control" value="<?php getValueFor('phone') ?>"/>
                            </div>

                            <div class="form-group">
                                <label for="cold_water" class="control-label">* Показания счетчика холодной воды:</label>
                                <input type="text" name="cold_water" class="form-control" value="<?php getValueFor('cold_water') ?>"/>
                            </div>

                            <div class="form-group">
                                <label for="hot_water" class="control-label">* Показания счетчика горячей воды:</label>
                                <input type="text" name="hot_water" class="form-control" value="<?php getValueFor('hot_water') ?>"/>
                            </div>

                            <div class="form-group">
                                <label for="comments" class="control-label">Ваш комментарий (необязательно):</label>
                                <textarea class="form-control" name="comments" cols="" rows="3"><?php getValueFor('comments') ?></textarea>
                            </div>


                            <input type="hidden" name="dosometh" value="do_contact" />
                            <button type="submit" class="btn btn-default">Отправить</button>
                        </form>
<?php } ?>

                </div>
                <div class="col-md-3">
                    <br/><br/>
                    <div class="center">Обязательные поля <br/>отмечены звездочкой<br/><br/></div>
                    <div class="center"><img src='./assets/images/counter.jpg'><br/><br/></div>
                    <div class="center">Обращаем Ваше внимание! Вы должны отправлять показания самого счетчика, а не Ваши вычисления!</div>
                </div>
            </div>
            
            <script>
            <!--
        $().ready(function() {
                $('label[for="email"]').after('<input type="text" class="form-control" name="input" value="<?php getValueFor('input') ?>" />');
            })
//-->
        </script>