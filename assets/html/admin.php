<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?= $title?></title>
    <base href="<?=SITE_URL?>">
    <link href="assets/css/lib.css?<?=date('dY')?>" rel="stylesheet">
  </head>
  <body>
    
    <?php include $page;?>
      <script src="assets/js/lib.js?<?=date('dY')?>"></script>
    </body>
</html>
