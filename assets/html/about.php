<div id = "item_wrap" class="container m-container m-container-form">

<p>Система ECOW.org позволяет отправить показания приборов учета воды своей управляющей компании, минуя телефонные звонки, личные походы в контору, ожидания тети Вали из бухгалтерии и тд. Показания уходят им прямо на электронную почту.</p>

<p>ECOW расшифровывается как Электронная Система Отправки Уведомлений в предприятия ЖКХ. Система бесплатна как для потребителей услуг, так и провайдеров.</p>

</div>