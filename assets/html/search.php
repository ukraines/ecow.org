<div class="container m-container">
<?php if (isset($companies) && is_array($companies) && count($companies)){?>
<!-- Tiles -->
<div class="row" id="tiles">
    <div class="row11 companies">
        <!-- col-12 col-md-4 col-sm-6 -->
        <?php foreach($companies as $company_card){?>
        
        <div class="div-tile">
            <div class="tile">
                <a href="<?=$company_card['company_url']?>" class="a-firm-cart">
                    
                    <span class="t-title" <?=(mb_strlen($company_card['short_name'])>COMPANY_NAME_LENGTH)?'title="'.htmlentities($company_card['short_name']).'"':'';?>><?=(mb_strlen($company_card['short_name'])>COMPANY_NAME_LENGTH)?mb_substr($company_card['short_name'], 0, COMPANY_NAME_LENGTH).'...':$company_card['short_name'];?></span>
                    <span class="t-addr" <?=(mb_strlen($company_card['address'])>COMPANY_ADDRESS_LENGTH)?'title="'.htmlentities($company_card['address']).'"':'';?>><?=(mb_strlen($company_card['address'])>COMPANY_ADDRESS_LENGTH)?mb_substr($company_card['address'], 0, COMPANY_ADDRESS_LENGTH).'...':$company_card['address'];?></span>
                    <span class="t-tel"><?=$company_card['phone']?></span>
                </a>

                <p class="send-params"><i class="fa fa-tint"></i>&nbsp;&nbsp;<a href="<?= $company_card['send_parameters_url']?>"><?=$lang_send_value?></a></p>
                <p class="send-message"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="<?= $company_card['request_url']?>"><?=$lang_send_email?></a></p>
            </div>
        </div>
        <?php }?>

        <!-- Don't touch! -->
        <div class="div-tile helper"></div>
        <div class="div-tile helper"></div>
        <div class="div-tile helper"></div>
        <!-- Don't touch! -->
    </div>
</div>
<!-- /Tiles -->
<?php }?>
<?php if (isset($pages_number) && $pages_number) {?>
<nav>
    <ul class="pagination search_mode" data-pages="<?=$pages_number?>">

    </ul>
</nav>
<?php }?>
</div>
<script>
    var updateCities = true;
</script>

<div id="template_company" class="hide">
    <div class="div-tile">
        <div class="tile">
            <a href="{{company_url}}" class="a-firm-cart">
                <span class="t-title" title="{{#printShortAsTitle}}{{short_name}}{{/printShortAsTitle}}">{{#limitLength}}{{short_name}}{{/limitLength}}</span>
                <span class="t-addr" title="{{#printAddressAsTitle}}{{address}}{{/printAddressAsTitle}}">{{#limitLength}}{{address}}{{/limitLength}}</span>
                <span class="t-tel">{{phone}}</span>
            </a>

            <p class="send-params"><i class="fa fa-tint"></i>&nbsp;&nbsp;<a href="{{send_parameters_url}}"><?=$lang_send_value?></a></p>
            <p class="send-message"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="{{request_url}}"><?=$lang_send_email?></a></p>
        </div>
    </div>
</div>