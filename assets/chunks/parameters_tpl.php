<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?= $title?></title>
  </head>
  <body>
    <p>Добрый день %company%</p>
    <p>Примите показания счетчиков от</p>
    <p>%fullname%</p>
    <p>%address%</p>
	<p>%phone%</p>
    <p>%counters%</p>
    <p>------------------------</p>
	<p>%signature%</p>
  </body>
</html>
