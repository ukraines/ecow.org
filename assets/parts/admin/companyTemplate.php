<?
	echo "
			<div id='template_company' mode = 'admin' class='hide'>
				<div class='div-tile'>
					<div class='tile' id = 'adt_{{company_id}}'>
						<div onclick = 'openDl(this)' class='btn-mc-nav btn-mc-nav-mini'>                   
							</div>
						<ul class='mc-nav mc-nav-mini mc-close'>
								<li><a onclick ='blockCompanies({{company_id}})' class = 'bl-ubl' href='javascript:void(0)'>Заблокировать</a></li>
								<li><a onclick ='setBadCompanies({{company_id}})' class = 'gd-bd1' href='javascript:void(0)'>Отклонить</a></li>
								<li><a onclick ='setGoodCompanies({{company_id}})' class = 'gd-bd2' href='javascript:void(0)'>Принять</a></li>
								<li><a onclick ='deleteCompanies({{company_id}})' href='javascript:void(0)'>Удалить</a></li>
								<li><a href='$lang_code{{check_details_url}}'>Уточнить</a></li>
								
						   </ul>
						<a href='$lang_code{{company_url}}' class='a-firm-cart'>
							<span id='c_data_shortname_{{company_id}}' class='t-title' title='{{#printShortAsTitle}}{{short_name}}{{/printShortAsTitle}}'>{{#limitLength}}{{short_name}}{{/limitLength}}</span>
							<span class='t-addr' title='{{#printAddressAsTitle}}{{address}}{{/printAddressAsTitle}}'>{{#limitLength}}{{address}}{{/limitLength}}</span>
							<span class='t-tel'>{{phone}}</span>
						</a>

						<p class='send-params'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a href='$lang_code{{send_parameters_url}}'>$lang_send_value</a></p>
						<p class='send-message'><i class='fa fa-envelope'></i>&nbsp;&nbsp;<a href='$lang_code{{request_url}}'>$lang_send_email</a></p>
						<p class='send-params hide'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a class = 'checkd' href='$lang_code{{check_details_url}}'>$lang_send_details</a></p>
						
						<p>Просмотров: {{viewed}}</p>
						<p>Отправлено показаний: {{sent_request}}</p>
						<p>Отправлено сообщений: {{sent_values}}</p>
						<div style='text-align:right;'>
							<label>
								<span class='company_status'></span>
								<input class='check-action' dt_id='{{company_id}}' type='checkbox'/>
							</label>
						</div>
						
					</div>
				</div>
			</div>
			<div id='template_company_check' mode = 'admin' class='hide'>
				<div class='div-tile'>
					<div class='tile' id = 'adt_{{company_id}}' >
						<div onclick = 'openDl(this)' class='btn-mc-nav btn-mc-nav-mini'>                   
							</div>
						<ul class='mc-nav mc-nav-mini mc-close'>
								<li><a onclick ='blockCompanies({{company_id}})' class = 'bl-ubl' href='javascript:void(0)'>Заблокировать</a></li>
								<li><a onclick ='setBadCompanies({{company_id}})' class = 'gd-bd1' href='javascript:void(0)'>Отклонить</a></li>
								<li><a onclick ='setGoodCompanies({{company_id}})' class = 'gd-bd2' href='javascript:void(0)'>Принять</a></li>
								<li><a onclick ='deleteCompanies({{company_id}})' href='javascript:void(0)'>Удалить</a></li>
								<li><a href='$lang_code{{check_details_url}}'>Уточнить</a></li>
								
						   </ul>
						<a href='$lang_code{{company_url}}' class='a-firm-cart'>
							<span id='c_data_shortname_{{company_id}}' class='t-title' title='{{#printShortAsTitle}}{{short_name}}{{/printShortAsTitle}}'>{{#limitLength}}{{short_name}}{{/limitLength}}</span>
							<span class='t-addr' title='{{#printAddressAsTitle}}{{address}}{{/printAddressAsTitle}}'>{{#limitLength}}{{address}}{{/limitLength}}</span>
							<span class='t-tel'>{{phone}}</span>
						</a>

						<p class='send-params'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a class = 'checkd' href='$lang_code{{check_details_url}}'>$lang_send_details</a></p>
						<p>Просмотров: {{viewed}}</p>
						<p>Отправлено показаний: {{sent_request}}</p>
						<p>Отправлено сообщений: {{sent_values}}</p>
						<div style='text-align:right;'>
							<label>
								<span class='company_status'></span>
								<input class='check-action' dt_id='{{company_id}}'  type='checkbox'/>
							</label>
						</div>
					</div>
				</div>
			</div>";
?>