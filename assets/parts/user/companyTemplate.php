<?
	echo "
		<div id='template_company' class='hide'>
			<div class='div-tile'>
				<div class='tile'>
					<a href='$lang_code{{company_url}}' class='a-firm-cart'>
						<span class='t-title' title='{{#printShortAsTitle}}{{short_name}}{{/printShortAsTitle}}'>{{#limitLength}}{{short_name}}{{/limitLength}}</span>
						<span class='t-addr' title='{{#printAddressAsTitle}}{{address}}{{/printAddressAsTitle}}'>{{#limitLength}}{{address}}{{/limitLength}}</span>
						<span class='t-tel'>{{phone}}</span>
					</a>

					<p class='send-params'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a href='$lang_code{{send_parameters_url}}'>$lang_send_value</a></p>
					<p class='send-message'><i class='fa fa-envelope'></i>&nbsp;&nbsp;<a href='$lang_code{{request_url}}'>$lang_send_email</a></p>
					<p class='send-params hide'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a class = 'checkd' href='$lang_code{{check_details_url}}'>$lang_send_details</a></p>
				</div>
			</div>
		</div>
		<div id='template_company_check' class='hide'>
			<div class='div-tile'>
				<div class='tile'>
					<a href='$lang_code{{company_url}}' class='a-firm-cart'>
						<span class='t-title' title='{{#printShortAsTitle}}{{short_name}}{{/printShortAsTitle}}'>{{#limitLength}}{{short_name}}{{/limitLength}}</span>
						<span class='t-addr' title='{{#printAddressAsTitle}}{{address}}{{/printAddressAsTitle}}'>{{#limitLength}}{{address}}{{/limitLength}}</span>
						<span class='t-tel'>{{phone}}</span>
					</a>

					<p class='send-params'><i class='fa fa-tint'></i>&nbsp;&nbsp;<a class = 'checkd' href='$lang_code{{check_details_url}}'>$lang_send_details</a></p>
				</div>
			</div>
		</div>";
?>