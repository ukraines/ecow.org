$(function(){
    
});



function blockCompany() {
    
    var url = 'set_block_json';
	
	var cid = $("#block_id").attr("adt");
	var why = $("#block_why").val();
	var whymore = $("#block_more").val();
	var ip =  $("#ipper").text();
	var params = 'company_id='+cid+'&why='+why+'&why_more='+whymore+'&who='+ip;
	var status_obj = "#status_"+cid;
    $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        data: params,
        beforeSend: function() {
            
        },
        success: function(response) {
			showMsg("Компания заблокирована");
			$("#block_more").val("");
			$("#block_why").val("");
			$("#block_id").attr("adt","");
			updateCompaniesList(1);
        }
    });
}


function setFilter(filter){
	if(filter != curFilter){
		curFilter = filter;
		setCookie("filter",filter,86400*365);
		updateCompaniesList(1);
		
		$("#action_panel").find(".top-nav-adm-l").each(function(){
			$(this).css("font-weight","normal");
		});
		$("#l_"+filter).css("font-weight","bold");
	}
}

function massAction(){
	var action = $("#make-action").val();
	var str = '';
	var url = '';
	$(".companies").find(".check-action:checked").each(function() {
	  str += $(this).attr("dt_id") + ",";
	});
	var params = "companies="+str;
	switch(action){
		case "good":
			url = 'set_good_company_mass_json';
			break;
		case "bad":
			url = 'set_bad_company_mass_json';
			break;
		case "del":
			url = 'delete_company_mass_json';
			break;
	}
	//console.log(params);
	 $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        data: params,
        beforeSend: function() {
            
        },
        success: function(response) {
			showMsg("Действие выполнено");
			updateCompaniesList(1);
		}
    });
}

function unBlockCompanies(cid) {
    
    var url = 'set_unblock_json';
	var params = "company_id="+cid;
	 $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        data: params,
        beforeSend: function() {
            
        },
        success: function(response) {
			showMsg("Компания разблокирована");
			updateCompaniesList(1);
		}
    });
}

function setBadCompanies(cid) {
    
    var url = 'set_bad_company_json';
	var params = "company_id="+cid;
	 $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        data: params,
        beforeSend: function() {
            
        },
        success: function(response) {
			showMsg("Компания отклонена");
			updateCompaniesList(1);
		}
    });
}

function setGoodCompanies(cid) {
    
    var url = 'set_good_company_json';
	var params = "company_id="+cid;
	 $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        data: params,
        beforeSend: function() {
            
        },
        success: function(response) {
			showMsg("Компания проверена и активирована");
			updateCompaniesList(1);
		}
    });
}

function deleteCompanies(cid) {
    
    var url = 'delete_company_json';
	var params = "company_id="+cid;
	 $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        data: params,
        beforeSend: function() {
            
        },
        success: function(response) {
			showMsg("Компания удалена");
			updateCompaniesList(1);
		}
    });
}

function admClose(){
	$(".adminPanel").addClass("hide");
	$(".cmp_block").addClass("hide");
}

function logOut(){
	setCookie("isAdmin_d3","0_0",86400*365);
	var newLoc = window.location + "";
	newLoc = newLoc.replace("?admin=admin","");
	newLoc = newLoc.replace("&admin=admin","");
	window.location = newLoc;
}

function blockCompanies(id){
	var title = $("#c_data_shortname_"+id).text();
	console.log("t="+title);
	$(".adminPanel").removeClass("hide");
	$(".cmp_block").removeClass("hide");
	$(".ttle").text(title);
	$("#block_id").attr("adt",id);
	
}
