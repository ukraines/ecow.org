//test date
var cities, load_companies = false,k;
var curFilter = "";
var inAlert = false;
var alertHard = false;
var clearId = 0;
var waitAlert = 5000;

$().ready(function () {
	
	jQuery.expr[':'].contains = function(a, i, m) {
		return jQuery(a).text().toLowerCase()
		  .indexOf(m[3].toLowerCase()) >= 0;
	};

    offAlert();
	
	$('.my-alert-loc').mouseenter(function(e){
		inAlert = true;
	});
	
	$('.my-alert-loc').mouseleave(function(e){
		inAlert = false;
		offAlert();
	});
	
	$('.my-alert-loc input[type=text]').focus(function(e){
		alertHard = true;
	});
	
	$('.my-alert-loc').click(function(e){
		alertHard = true;
	});
	
    $(".select_region,.close-loc").click(function(e){
        e.preventDefault();
		alertHard = false;
		inAlert = false;
		setCookie('alert', 1, 86400*365);
		offAlert();
        if(!$('.my-alert-loc.alert:visible').length)
            $('.my-alert-loc.alert').fadeIn(200);
        else
            $('.my-alert-loc.alert').fadeOut(200);  
    });
	
	
    $(".close-loc").click(function(e){
       setCookie('alert', 0, 86400*365);
    });
    $('#send_parameters_form').on('submit', function(){
        return checkSendParametersForm();
    });
    if ($('nav ul.pagination.search_mode').length) {
        updatePager($('nav ul.pagination.search_mode').data('pages'), 1);
    }
    $('body').on('click', 'nav ul.pagination li', function(e){
        e.preventDefault();
        if ($(this).hasClass('active')) {
            return false;
        }
        var v = 0;
        var search_mode = '';
        if ($pagination.hasClass('search_mode')) {
            search_mode = 'search_mode';
        }
        
        if ($(this).hasClass('prev')) {
            v = $(this).next().find('a:first').html()
            updatePager($pagination.data('pages'), v, true, search_mode)
        } else if ($(this).hasClass('next')) {
            v = $(this).prev().find('a:first').html();
            updatePager($pagination.data('pages'), v, true, search_mode)
        } else {
            v = parseInt($(this).find('a:first').html());
            $pagination.data('active', v).find('li.active').removeClass('active');
            $(this).addClass('active');
            if ($pagination.hasClass(search_mode)) {
                updateCompaniesList(v, search_mode, $('.q_search').val());
            } else {
                updateCompaniesList(v);
            }
        }
    })
    var do_not_show = getCookie('do_not_show');
    if (do_not_show == 1) {
        $('.do_not_show_row').addClass('hide');
    }
    $('.do_not_show').click(function(){
        setCookie('do_not_show', 1, 86400*365);
    });
    init();

    $(window).resize(function(){
        MegaSmallScreen();
    });

    MegaSmallScreen();
  
    $(".row-location .dropdown-menu li input").click(function(e){
        e.stopPropagation();
    });
    /*$(".row-location .dropdown-menu li input").keyup(function(e){
                $(this).parent().parent().prev().find('.btn-title').html($(this).val());
        if($(this).parent().parent().prev().find('.btn-title-country').html() == "") $(this).parent().parent().prev().find('.btn-title-country').html("Страна")
        else if($(this).parent().parent().prev().find('.span-title-region').html() == "") $(this).parent().parent().prev().find('.span-title-region').html("Регион - область")
        else if($(this).parent().parent().prev().find('.span-title-city').html() == "") $(this).parent().parent().prev().find('.span-title-city').html("Город")
    });*/
    
    $(".row-location .dropdown-menu li a").click(function(e){
        var parent = $(this).parent().parent();
        var text = $(this).html();
        var btn = parent.prev().find('.btn-title');
        btn.html(text);
    });
    
    $(".q_search").focus(function(){
        if ($(".suggest li").length) {
            //$(".suggest li:not(:contains('" + $(this).val() + "'))").remove();
            $(".suggest").css({"visibility" : "visible"});
        } else if ($(this).val() != '') {
            $(this).trigger('keyup');
        }
    }).keyup(function(e){
        $(".q_search").data('company_id', 0);
        var s = $.trim($(this).val());
        $(".suggest").css({"visibility" : "hidden"});
        if (s == '') {
            $(".suggest li").remove();
            return true;
        }
        $.ajax({
            url: 'search_companies_json?s=' + s,
            cache: false,
            dataType: 'json',
            beforeSend: function(){
                
            },
            success: function(response) {
                $(".suggest li,.suggest div").remove();
                if (response.status == 'OK' && typeof response.companies == 'object') {
                    var i, n = response.companies.length;
                    for(i = 0; i < n; i++) {
                        if (i == 9) {
                            $('.suggest').append('<div class="more-suggests msgt-close"><a href="search?s=' + s + '">' + lang_show_more_results + '</a></div>');
                        } else {
                            $('.suggest').append('<li data-company_id=' + response.companies[i].company_id + ' data-url="' + escape(response.companies[i].company_url) + '"><span class="f-name company-place">' + response.companies[i].short_name + ' </span><span class="f-city company-title">' + response.companies[i].city + '</span></li>');
                        }
                    }
                    if ($(".suggest li").length) {
                        $(".suggest").css({"visibility" : "visible"});
                    }
                }
            }
        });
    });
	
	$("#add-sel-country").on('change', function(e){
		var country_id = $("#add-sel-country").val().trim();
		var url = "regions_by_country_json";
		var params = "country_id="+country_id;
		$.ajax({
			url: url,
			cache: false,
			dataType: 'json',
			data: params,
			beforeSend: function() {
				
			},
			success: function(response) {
				if(response.status == "OK"){
					var regions = response.message;
					$("#add-sel-region").html("<option value='0'>Регион-область</option>");
					for(var i=0; i<regions.length; i++){
						$("#add-sel-region").append("<option value='"+regions[i].region_id+"'>"+regions[i].name+"</option>");
					}
				}
			}
		});
	
	});
	
	$("#add-sel-region").on('change', function(e){
		var region_id = $("#add-sel-region").val().trim();
		var url = "cities_by_region_json";
		var params = "region_id="+region_id;
		$.ajax({
			url: url,
			cache: false,
			dataType: 'json',
			data: params,
			beforeSend: function() {
				
			},
			success: function(response) {
				if(response.status == "OK"){
					var cities = response.message;
					$("#add-sel-city").html("<option value='0'>Город</option>");
					for(var i=0; i<cities.length; i++){
						$("#add-sel-city").append("<option value='"+cities[i].city_id+"'>"+cities[i].name+"</option>");
					}
				}
			}
		});
	
	});
    
    $("body").on( 'click', '.suggest li', function(e){
        var company_id = $(this).data('company_id');
        document.location = $(this).data('url');
        /*$(".q_search").data('company_id', company_id).
                data('url', $(this).data('url')).
                val($(this).
                find('.company-title').html());*/
        $(".suggest").css("visibility", "hidden");
    });
    $('.show_company').click(function(){
        //$('.q_search').focus();
        //return false;
        var counter_id = $(".suggest li").size();
		if (counter_id > 0) {
            document.location = "/search?s="+$("input[name=query]").val();
        }
    });
    
    $(document).click(function(event) {
        if (($(event.target).closest(".search-form").length) || $(".suggest").css('visibility')=='hidden'){return;} 
            if($(".suggest").css('visibility')=='visible'){ 
                $(".suggest").css("visibility", "hidden");
                event.stopPropagation(); 
            }
    });
    

    $('body').on("click", ".btn-plus", function(){
        if($(this).hasClass("btn-plus-extension")){
            $(this).parent().parent().remove();
            return;
        }
        var currentRow = $(this).parent().parent();
        currentRow.after(
            "<div class='row row-padding-0 form-group'>" 
            + currentRow.html()
            + "</div>"
        );
        var newRow = currentRow.next();
        newRow.find(".btn-plus").addClass("btn-plus-extension").html("-"); 
    });
})

function openDl(card){
	if($(card).next().hasClass('mc-close')) $(card).next().removeClass('mc-close');
    else $(card).next().addClass('mc-close');
}

function showMsg(msg){
	$(".msg").text(msg);
	animate(".msg","goMsg");
}
//animate(".demo", 'bounce');
function animate(element, animation) {
	$(element).addClass(animation);
	var wait = window.setTimeout( function(){
		$(element).removeClass(animation)}, 5000
	);
}

function offAlert(){
	clearInterval(clearId);
	//console.log("off " + alertHard );
	clearId = setTimeout(function(){
		 if($('.my-alert-loc.alert:visible').length && !alertHard && !inAlert){
            $('.my-alert-loc.alert').fadeOut(200); 
			setCookie('alert', 0, 86400*365);
		 }
    }, waitAlert);
}

function updateRegionList(country_id) {
	//setLoc();
	$("#all_regions_filter").attr("data-country_id",country_id);
	$("#all_cities_filter").attr("data-country_id",country_id);
	
	
	var cnt=0;														//@FIX
    if (country_id > 0) {
		 $region.children('li').each(function () {
			if (!$(this).find('a').length) {
                return true;
            }
            if ($(this).find('a').data('country_id') == country_id ) {
                $(this).removeClass('hide');
				$(this).removeClass('hide_filtered');
				cnt++;
			
            } else {
                $(this).addClass('hide');
            }
			if(cnt>10){
				 $(this).addClass('hide_filtered');
			}
        });
		//console.log(cnt);
   
    } else {
        $region.children('li').removeClass('hide');
    }
    if ($region_dropdown.data('country_id') != country_id) {
        $region_dropdown.data('value', 0);
        $region_dropdown.find('span.btn-title').html(lang_select_region)
    }
    $region_dropdown.data('country_id', country_id);
}

function updateCityList(country_id, region_id) {
	//var selected_country_id = $city_dropdown.data('country_id');
    var selected_region_id = $city_dropdown.data('region_id');
    //var selected_value = $city_dropdown.data('value');
    //$city.find('li.city_el').remove();
	
	$city.find('li').addClass('hide');
    $city.find('li input').parent().removeClass('hide');
	$("#all_regions_filter").parent().removeClass('hide');
	$("#all_cities_filter").parent().removeClass('hide');
	$("#all_regions_filter").parent().removeClass('active');
	$("#all_cities_filter").parent().removeClass('active');
	var cnt=0;														//@FIX
    $city.find('li').each(function(){
		if (region_id > 0) {
            if (region_id == $(this).find('a:first').data('region_id')) {
                $(this).removeClass('hide');
				 $(this).removeClass('hide_filtered');
				cnt++;
			}
        } else if (country_id > 0) {
            if (country_id == $(this).find('a:first').data('country_id')) {
                $(this).removeClass('hide');
				$(this).removeClass('hide_filtered');
				cnt++;
			}
        }
		
       
		if(cnt>10){
			$(this).addClass('hide_filtered');
		}
    })
	
    if (selected_region_id != region_id) {
        $city_dropdown.data('country_id', 0).
                data('region_id', 0).
                data('value', 0).
                find('span.btn-title').html(lang_select_city);
    }
    if (load_companies) {
        
    }
}


function updateCompaniesList(page, mode, s) {
	
    if (!$('.companies').length) {
        return false;
    }
	
	$('#item_wrap').fadeOut(300);
	$('#list_wrap').fadeIn(700);
	$('#list_wrap').removeClass("hide");
	//console.log($('.header_left').get(0));
	$('.header_left').each(function(){
		$(this).html("<a>"+ lang_header_title_empty +"</a>");
	});
	
    var params = '';
    var url = '';
    var search_mode = '';
    if (typeof mode != 'undefined' && mode == 'search_mode') {
        params = '&s=' + s + '&page=' + page;
        url = 'search_companies_json';
        search_mode = mode;
    } else {
        params = 'country_id=' + $country_dropdown.data('value') + 
            '&region_id=' + $region_dropdown.data('value') + '&city_id=' + $city_dropdown.data('value') + '&page=' + page;
        url = 'companies_json';
    }
    $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        data: params,
        beforeSend: function() {
            
        },
        success: function(response) {
			$('.companies > div.div-tile:not(.helper)').remove();
            var tpli = '#template_company';
			var tpl;
			updatePager(0, 0);
			
			if (typeof response.filters == 'object') {
				var filters = response.filters;
					if(filters){
						//console.log("arr");
						for (var key in filters) {
							var tid = "#cb_"+key;
							var value = filters[key];
							$(tid).text(value);
							//console.log(tid + " " + value);
						
						}
					}
             }
			 
            if (typeof response.companies == 'object') {
					
                var i, n = response.companies.length;
				 for(i = 0; i < n; i++) {
                    var company = response.companies[i];
					if(company.manager == ""){						//no mail
						tpli = '#template_company_check';
					}
					if($(tpli).attr("mode") == "admin"){
						
						var stat = "Активная";
						switch(company.status){
							case "bd_st":
								stat = "Отклонена";
								break;
							case "wt_st":
								stat = "Непроверена";
								break;
						}
						
						if(parseInt(company.isblocked,10) == 1){
							console.log("unbl");
							$(tpli).find('.tile',0).attr("style", "background-color:#ddd");			//error
							$(tpli).find('.bl-ubl',0).text("Разблокировать");
							$(tpli).find('.bl-ubl',0).attr("onclick", "unBlockCompanies("+company.company_id+")");	
						}else{
							$(tpli).find('.tile',0).attr("style", "");
							$(tpli).find('.bl-ubl',0).text("Заблокировать");
							$(tpli).find('.bl-ubl',0).attr("onclick", "blockCompanies("+company.company_id+")");	
						}
						if(company.status == "ch_st"){
							$(tpli).find('.gd-bd1',0).addClass("hide");
							$(tpli).find('.gd-bd2',0).addClass("hide");
						}else if(company.status == "bd_st"){
							$(tpli).find('.gd-bd1',0).addClass("hide");
						}
						
						if(parseInt(company.isblocked,10) == 1)
							stat = "Заблокирована";
						
						$(tpli).find('.company_status',0).text(stat);
						
						
					}
					tpl = $(tpli).html();
					
					//console.log(company);
                    company.limitLength = function() {
                        return function(text, render) {
                            var n = 33;
                            if (text == '{{short_name}}') {
                                n = 25
                            }
                            var v = render(text);
                            v = unescape(v);
                            if (v.length > n) {
                                v = htmlentities.decode(v);
                                return v.substr(0, n) + '...';
                            } else {
                                return render(text);
                            }
                        }
                    }
                    company.printShortAsTitle = function() {
                        return function(text, render) {
                            var v = render(text);
                            if (v.length < 28) {
                                return '';
                            } else {
                                return render(text);
                            }
                        }
                    }
                    company.printAddressAsTitle = function() {
                        return function(text, render) {
                            var v = render(text);
                            if (v.length < 33) {
                                return '';
                            } else {
                                return render(text);
                            }
                        }
                    }
                    var html = Mustache.render(tpl, company);
					
                    $('.companies').prepend(html);
                }
                if ( typeof response.pages_number == 'number') {
                    updatePager(response.pages_number, page, false, search_mode);
                }
            }
			
				alertHard = false;
				offAlert();
        }
    });

}

function sendDetails(_who, cid){
	if(_who !== "Admin"){
		if(!checkFormDetails() ){
			showMsg("Заполните пожалуйста обязательные поля");
			return false;
		}
	}
	var url = 'set_new_details_json';
	var who = _who;
	var manager = $("#manager").val();
	var address = $("#address").val();
	var phone = $("#phone").val();
	var email = $("#email").val();
	if(!isValidMail(email)  && $("#isaadm").text() !== "iiss"){
		showMsg("Вы ввели некорректный email предприятия, пожалуйста, введите Вашу реальную почту");
		return;
	}
	var name = $("#name").val();
	var short_name = $("#short_name").val();
	var site = $("#site").val();
	var name_sender = $("#name_sender").val();
	var mail_sender = $("#mail_sender").val();
	if(!isValidMail(mail_sender) && $("#isaadm").text() !== "iiss"){
			showMsg("Вы ввели некорректный email, пожалуйста, введите Вашу реальную почту");
			return;
		}
	var agree = $("#agree").is(':checked');
	var ip = $("#ipper").text();
	if(agree){
		if(typeof(site) == "undefined")
			site = "";
		if(typeof(short_name) == "undefined")
			short_name = "";
		if(typeof(name) == "undefined")
			name = "";
		var params = "cid="+cid+"&who="+who+"&manager="+manager+"&address="+address+"&phone="+
					 phone+"&email="+email+"&name="+name+"&short_name="+short_name+"&site="+
					 site+"&agree="+agree+"&mail_sender="+mail_sender+"&name_sender="+name_sender+"&ip="+ip;
		
		$.ajax({
			url: url,
			cache: false,
			dataType: 'json',
			data: params,
			beforeSend: function() {
				
			},
			success: function(response) {
				if(response.status == "FAIL")
					showMsg(response.message);
			}
		});
	}else{
		showMsg("Вам необходимо согласится с условиями для продолжения");
	}
}

function isValidMail(mail){
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(mail)) {
       return false;
    }
	return true;
}

function checkFormDetails(){
	var mail_sender = $("#mail_sender").val();
	var name_sender = $("#name_sender").val();
	if(mail_sender.trim() == "" || name_sender.trim() == "")
		return false;
	return true;
}


function addCompany(_who){
	if(checkForm() || $("#isaadm").text() === "iiss"){
		var url = 'add_new_company_json';
		var who = _who;
		var manager = $("#manager").val();
		var address = $("#address").val();
		var phone = $("#phone").val();
		var email = $("#email").val();
		if(!isValidMail(email)  && $("#isaadm").text() !== "iiss"){
			showMsg("Вы ввели некорректный email предприятия, пожалуйста, введите реальную почту");
			return;
		}
		var name = $("#name").val();
		var short_name = $("#short_name").val();
		var site = $("#site").val();
		var name_sender = $("#name_sender").val();
		var mail_sender = $("#mail_sender").val();
		if(!isValidMail(mail_sender) && $("#isaadm").text() !== "iiss"){
			showMsg("Вы ввели некорректный email, пожалуйста, введите Вашу реальную почту");
			return;
		}
		var agree = $("#agree").is(':checked');
		var ip = $("#ipper").text();
		var country_id = $("#add-sel-country").val();
		var region_id = $("#add-sel-region").val();
		var city_id = $("#add-sel-city").val();
		var comment = $("#comment").val();
		
		if(agree){
			if(typeof(site) == "undefined")
				site = "";
			if(typeof(short_name) == "undefined")
				short_name = "";
			if(typeof(name) == "undefined")
				name = "";
			var params = "who="+who+"&manager="+manager+"&address="+address+"&phone="+
						 phone+"&email="+email+"&name="+name+"&short_name="+short_name+"&site="+
						 site+"&agree="+agree+"&mail_sender="+mail_sender+"&name_sender="+name_sender+
						 "&ip="+ip+"&country_id="+country_id+"&region_id="+region_id+"&city_id="+city_id+"&comment="+comment;
			
			$.ajax({
				url: url,
				cache: false,
				dataType: 'json',
				data: params,
				beforeSend: function() {
					
				},
				success: function(response) {
					if(response.status == "FAIL")
						showMsg(response.message);
				}
			});
		}else{
			showMsg("Вам необходимо согласится с условиями для продолжения");
		}
	}else{
		showMsg("Заполните пожалуйста обязательные поля");
	}
}		

function checkForm(){
	var name = $("#name").val();
	var short_name = $("#short_name").val();
	var country_id = $("#add-sel-country").val();
	var region_id = $("#add-sel-region").val();
	var city_id = $("#add-sel-city").val();
	var address = $("#address").val();
	if(name.trim() == "" || short_name.trim() == "" || 
		address.trim() == "" || country_id == 0 || region_id == 0 ||
		city_id == 0)
		return false;
	return true;
}

function init() {
    if ($('#country').length && $('#region').length) {
        $country = $('#country');
        $region = $('#region');
        $city = $('#city');
        $country_dropdown = $('#country_dropdown')
        $region_dropdown = $('#region_dropdown')
        $city_dropdown = $('#city_dropdown')
        
        updateRegionList($country_dropdown.data('value'));
        updateCityList($country_dropdown.data('value'), $region_dropdown.data('value'));
        load_companies = true;
        
        $('#country li a').click( function(){
            $('#country li.active').removeClass('active');
            $(this).parent().addClass('active');
            setLocation('country_id', $(this).data('value'));
            $('#country_dropdown').data('value', $(this).data('value'));
            updateRegionList($(this).data('value'));
            updateCityList($(this).data('value'), $region_dropdown.data('value'));
			updateCompaniesList(1);
            updateSelectedLocationTitle();
		});
        $('#region li a').click( function(){
			
            $('#region li.active').removeClass('active');
            $(this).parent().addClass('active');
            var value = $(this).data('value');
            setLocation('region_id', value);
            $region_dropdown.data('value', value).data('country_id', $(this).data('country_id'));
            updateCityList($country_dropdown.data('value'), value);
			updateCompaniesList(1);
            updateSelectedLocationTitle();
		});
        $('#city li a').click( function(){
            $('#city li.active').removeClass('active');
            $(this).parent().addClass('active');
            var value = $(this).data('value');
            setLocation('city_id', value);
            var country_id = $(this).data('country_id');
            var region_id = $(this).data('region_id');
            $city_dropdown.data('value', value).
                    data('region_id', region_id).
                    data('country_id', country_id);
            updateSelectedLocationTitle();
            if ($region_dropdown.data('value') == 0 && value > 0) {
                $region_dropdown.data('country_id', country_id).data('value', region_id)
                $region.find('li a').each(function(){
                    if ($(this).data('value') == region_id) {
                        $(this).parent().addClass('active');
                        $('span.span-title-region').html($(this).html());
                        return false;
                    }
                });
                updateCityList(country_id, region_id);
            }
			updateCompaniesList(1);
		})
																						
        $('.dropdown-menu input[type="text"]').keyup(function(e){
			//alert("update2");
            var text = $(this).val();
			//document.title= text;
			if(text == ""){
				$(this).closest('.dropdown-menu').find("li a").parent().addClass('hide_filtered');
				$(this).closest('.dropdown-menu').find("li a").parent().not(".hide").slice(0,10).removeClass('hide_filtered');
			}else{
				$(this).closest('.dropdown-menu').find("li a:not(:Contains('" + text + "'))").parent().addClass('hide_filtered');
				$(this).closest('.dropdown-menu').find("li a:Contains('" + text + "')").parent().not(".hide").slice(0,10).removeClass('hide_filtered');
			}
            return false;
        });
    }
}


function setLoc(){
	
	
    var country = ($('.span-title-country').html().trim() == lang_select_country) ? '' : $('.span-title-country').html().trim();
    var region = ($('.span-title-region').html().trim() == lang_select_region) ? '' : $('.span-title-region').html().trim();
    var city = ($('.span-title-city').html().trim() == lang_select_city) ? '' : $('.span-title-city').html().trim();
   
    region = (region == lang_select_all_regions.trim()) ? '' : region;
    city = (city == lang_select_all_cities.trim()) ? '' : city;
  
	var params = pathUrl;
	
	var redirect = params+translit(country)+translit(region)+translit(city);
	redirect = redirect.replace("/[\/{2,}]/g","/");
	//console.log("redir = " + redirect);
	history.pushState('', '', redirect);
}

function translit(word){
	word = decodeURI(word).toLowerCase().trim();
	//console.log(word);
	var retStr = word;
	var arr = getTransiltArr();
    for (var x in arr) {
        retStr = retStr.replace(new RegExp(x, 'g'), arr[x]);
    }
	
	retStr = retStr.replace('-', '_');
	retStr = retStr.replace(/\s/g, '-');
	//console.log(retStr);
	retStr = retStr.replace(/[^0-9a-z\-\_]+/g, '');
	if(retStr.length > 0)
		retStr = retStr + "/";
	return retStr;
}

function getTransiltArr(){
	 return {
        'а' : 'a', 'б' : 'b', 'в' : 'v',
        'г' : 'g', 'д' : 'd', 'е' : 'e',
        'ё' : 'ei', 'ж' : 'zh', 'з' : 'z',
        'и' : 'i', 'й' : 'y', 'к' : 'k',
        'л' : 'l', 'м' : 'm', 'н' : 'n',
        'о' : 'o', 'п' : 'p', 'р' : 'r',
        'с' : 's', 'т' : 't', 'у' : 'u',
        'ф' : 'f', 'х' : 'h', 'ц' : 'c',
        'ч' : 'ch', 'ш' : 'sh', 'щ' : 'sch',
        'ь' : 'ui', 'ы' : 'uy', 'ъ' : 'uu',
        'э' : 'eyi', 'ю' : 'yu', 'я' : 'ya',
        "ї" : "yi", "є" : "ye",
        'А' : 'A', 'Б' : 'B', 'В' : 'V',
        'Г' : 'G', 'Д' : 'D', 'Е' : 'E',
        'Ё' : 'EI', 'Ж' : 'Zh', 'З' : 'Z',
        'И' : 'I', 'Й' : 'Y', 'К' : 'K',
        'Л' : 'L', 'М' : 'M', 'Н' : 'N',
        'О' : 'O', 'П' : 'P', 'Р' : 'R',
        'С' : 'S', 'Т' : 'T', 'У' : 'U',
        'Ф' : 'F', 'Х' : 'H', 'Ц' : 'C',
        'Ч' : 'Ch', 'Ш' : 'Sh', 'Щ' : 'Sch',
        'Ь' : 'UI', 'Ы' : 'UY', 'Ъ' : 'UU',
        'Э' : 'EYI', 'Ю' : 'Yu', 'Я' : 'Ya',
        "Ї" : "YI", "Є" : "ye"
	 };
}


function MegaSmallScreen(){
if($(window).width() <= "590" && $("#div-globe").hasClass("col-xs-1")){
    $("#div-globe").removeClass("col-xs-1");
    $("#div-globe").addClass("col-xs-12");
    $("#div-country").removeClass("col-xs-11");
    $("#div-country").addClass("col-xs-12");
} else if($(window).width() > "590") {
    $("#div-globe").addClass("col-xs-1");
    $("#div-globe").removeClass("col-xs-12");
    $("#div-country").addClass("col-xs-11");
    $("#div-country").removeClass("col-xs-12");
    $(".input-hot").attr("placeholder", "Показания счётчика горячей воды");
    $(".input-cold").attr("placeholder", "Показания счётчика холодной воды");
} 
if($(window).width() <= "590"){
    $(".input-hot").attr("placeholder", "Горячая вода");
    $(".input-cold").attr("placeholder", "Холодная вода");
}
}

function setCookie(cname, cvalue, seconds) {
	//console.log(cname+" "+cvalue);
    var d = new Date();
    d.setTime(d.getTime() + (seconds*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "'; path=/";
}

function setLang(lang){
	setCookie("lang",lang,86400*365);
}



function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}



function updatePager(total_pages, page, active_data) {
	var st = (page-1)*16;
	var nd = page*16;
	var allp = total_pages*16;
	if(allp != 0){
		$('#cntPages').text("Всего страниц - "+total_pages+" показаны "+st+ " - " + nd + " из ~ "+allp);
		$('#cntPagesClient').addClass("hide");
		$('#cntPagesClientEmptyStart').addClass("hide");
	}
	else{
		$('#cntPages').text("Ничего не найдено");
		$('#cntPagesClient').removeClass("hide");
		$('#cntPagesClientEmptyStart').addClass("hide");
		$('#cntPagesEmptyStart').addClass("hide");
		
		$('#cntPagesClient').text("В этом регионе управляющих компаний не найдено. Попробуйте изменить параметры поиска выше или же добавить компанию вручную");
	}
	
	$pagination = $('nav ul.pagination');
    $pagination.html('');
    var class_name = '', html = '', d, i, k, n = 3, active_page = page;
    
    if (typeof active_data == 'undefined') {
        active_data = false;
    }
    if (active_data) {
        active_page = $pagination.data('active');
    }
    if (total_pages < n) {
        n = total_pages;
    }
    if (n > 1) {
        for(i = 0; i < n; i++) {
            if (page == 1) {
                d = 0;
            } else {
                d = 1;
            }
            if (page == total_pages && total_pages > 2) {
                d = 2;
            } else if (page == total_pages && total_pages <= 2) {
                $d = 0;
            }
            k = page - d + i;
            class_name = '';
            /*if (total_pages < k || k < 1) {
                continue;
            }*/
            if (active_page == k) {
                class_name = ' class="active"';
            }
            html += '<li' + class_name + '><a>' + k + '</a></li>';
        }
        if (total_pages > 3 && page >= 3) {
            html = '<li class="prev"><a>&laquo;</a></li>' + html;
        }
        if (total_pages > 3 && (total_pages - page) > 1) {
            html += '<li class="next"><a>&raquo;</a></li>';
        }
    }
    $('nav .pagination').data('pages', total_pages).html(html);
}

function switchMode(mode) {
    if (mode == 'search') {
        $('.row-location').hide();
        $('#m-header .header_left a').html(lang_companies_search);
    } else {
        $('.row-location').show();
        $('#m-header .header_left').html(lang_header_title.replace(' %s',''));
    }
}

function checkSendParametersForm() {
    var warn_text = '';
    if($.trim($('[name="fullname"]').val()) == ''){
        warn_text = lang_fullname_empty + '<br/>';
    }
    if($.trim($('[name="address"]').val()) == ''){
        warn_text = lang_address_empty + '<br/>';
    }

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test($('[name="email"]').val())) {
        warn_text += lang_invalid_email + '<br/>';
    }

    if($.trim($('[name="phone"]').val()) == ''){
        warn_text = lang_phone_empty + '<br/>';
    }
    
    var values = 0;
    $('[name^="values"]').each(function(){
        if (intValue($(this).val()) <= 0) {
            values++;
        }
    })
    if ($('[name^="values"]').length <= values) {
        warn_text = lang_value_empty + '<br/>';
    }
    if (!$('[name="agree"]:checked').length) {
        warn_text = lang_agree_not_selected + '<br/>';
    }
    
    
    if(warn_text != ''){
        
        showAlertWarning(warn_text);
        
        //window.scrollTo(0, 0);
        return false;
    }
    return false;
}

function showAlertWarning(text){
    alert(text)
    $('.alert').text(text).removeClass(ERROR_TYPE_SUCCESS).addClass(ERROR_TYPE_WARNING).removeClass('hide').show();
    //scrollToAnchor('alert');
    setTimeout("$('.alert').hide()", 7000);
}

function showAlertSuccess(text){
    $('.alert').text(text).removeClass(ERROR_TYPE_WARNING).addClass(ERROR_TYPE_SUCCESS).removeClass('hide').show();
    scrollToAnchor('alert');
    setTimeout("$('.alert').hide()", 7000);
}
function scrollToAnchor(aid){
    var aTag = $("#"+ aid);
    $('html,body').animate({scrollTop: aTag.offset().top}, 800);
}
function intValue(value) {
    var v = parseInt(value);
    if (isNaN(v)) {
        v = 0;
    }
    return v;
}
function setLocation(var_name, value){
    var params = var_name + '=' + value;
    $.ajax('set_location_json?' + params, {cache: false});
}
                         

var kkkk;
function updateSelectedLocationTitle(){
	if ($city_dropdown.data('value') > 0) {
        $city_dropdown.on('hidden.bs.dropdown', function(){
			var city = $('.span-title-city').html();
			if(city.trim() == lang_select_all_cities.trim())
				city = $('.span-title-region').html();
			$('.select_region span').html(city);
			setLoc();
		 })
    } else if ($region_dropdown.data('value') > 0) {
        $region_dropdown.on('hidden.bs.dropdown', function(){
			var region = $('.span-title-region').html();
			if(region.trim() == lang_select_all_regions.trim())
				region = $('.span-title-country').html();
			 $('.select_region span').html(region);
			 setLoc();
		})
    } else if ($country_dropdown.data('value') > 0) {
        $country_dropdown.on('hidden.bs.dropdown', function(){
			 $('.select_region span').html($('.span-title-country').html());
			 setLoc();
		})
    }
	setLoc();        
}
/*test date*/