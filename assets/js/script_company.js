$(window).resize(function(){
   //MegaSmallScreen();
});

$(document).ready(function(){
    //MegaSmallScreen();
   mystarRating.create('.myStars');
   starRating.create('.stars-firm');
    starRating.create('.stars-u1');
     starRating.create('.stars-u2');
    starRating.create('.stars-u3');
    starRating.create('.stars-u4');
    starRating.create('.stars-u5');
    
    $(".row-location .dropdown-menu li input").click(function(e){
        e.stopPropagation();
    });
    $(".row-location .dropdown-menu li input").keyup(function(e){
                $(this).parent().parent().prev().find('.btn-title').html($(this).val());
        if($(this).parent().parent().prev().find('.btn-title-country').html() == "") $(this).parent().parent().prev().find('.btn-title-country').html("Страна")
        else if($(this).parent().parent().prev().find('.span-title-region').html() == "") $(this).parent().parent().prev().find('.span-title-region').html("Регион - область")
        else if($(this).parent().parent().prev().find('.span-title-city').html() == "") $(this).parent().parent().prev().find('.span-title-city').html("Город")
    });
    
    $(".row-location .dropdown-menu li a").click(function(e){
        var parent = $(this).parent().parent();
        var text = $(this).html();
        var btn = parent.prev().find('.btn-title');
        btn.html(text);
    });
    
    $(".q_search").focus(function(){
        $(".suggest").css({"visibility" : "visible"});
    });
    
    $(".suggest li").click(function(e){
        $(".q_search").val($(this).find('.f-name').html());
        $(".suggest").css("visibility", "hidden");
    });
    
    $(document).click(function(event) {
    if (($(event.target).closest(".search-form").length) || $(".suggest").css('visibility')=='hidden'){return;} 
            if($(".suggest").css('visibility')=='visible'){ 
                        $(".suggest").css("visibility", "hidden");
    event.stopPropagation(); 
            }
    });
    
    

    $('.btn.dropdown-toggle').click(function(){ 
        var input = $(this).next().find('input');
        setTimeout(function(){
            input.focus();
        }, 100);
    });
    
    
    $(".close-loc").click(function(e){
        e.stopPropagation();
        $('.my-alert-loc').fadeOut(200);
    });
    
   /* $(".select_region").click(function(){													close top bar
        if($('.my-alert-loc.alert').css("display") == 'none')
            $('.my-alert-loc.alert').fadeIn(200);
        else
            $('.my-alert-loc.alert').fadeOut(200);  
    });*/
    
    $('.btn-write-coment').click(function(){
        $('.view-review').fadeOut(200);
        $('.write-review').fadeIn(200);
    });
    
    $('.btn-sr').click(function(){
        $('.view-review').fadeIn(200);
        $('.write-review').fadeOut(200);
    });
    
    $('.btn-glode').click(function(){
        $('.my-alert-loc').fadeOut(200);
    });
    
    $('.v-param').keydown(function(){
        if($(this).val()=="") $(this).parent().removeClass('has-error');
    });
    
    $('.btn-send-params').click(function(){
        var r = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;
if (!r.test($('.p-email').val())) {
    $('.p-email').val('');
        $('.p-email').parent().addClass('has-error');
}
        
        var re = /^\d[\d\(\)\ -]{4,14}\d$/;
    if (!re.test($('.p-tel').val())) {
           $('.p-tel').val('');
        $('.p-tel').parent().addClass('has-error');
    }


    });
    
    $('.more-comments').click(function(){
        if($(this).hasClass("mci-close")){
                $(this).html('Скрыть отзывы').removeClass('mci-close');
                $('.com-hidden').show(100);
                
        }   else {
                $(this).html('Показать ещё отзывы').addClass('mci-close');
                $('.com-hidden').hide(200);
        }

    });
    
    $('.more-suggests').click(function(){
                if($(this).hasClass("msgt-close")){
                $(this).html('Скрыть результаты').removeClass('msgt-close');
                $('.sgt-hidden').show(100);
                
        }   else {
                $(this).html('Показать ещё результаты...').addClass('msgt-close');
                $('.sgt-hidden').hide(100);
        }
    });
    

    
        $(document).click(function(event) {
    if ($(event.target).closest(".mc-nav").length || $(event.target).closest(".btn-mc-nav").length ||  $(".mc-nav").hasClass('mc-close')){ return;} 
            if(!$(".mc-nav").hasClass('mc-close')){
                       $('.mc-nav').addClass('mc-close');
    event.stopPropagation(); 
            }
    });
    
        
    $('.btn-mc-nav').click(function(){
		console.log($(this).next().hasClass('mc-close'));
        if($(this).next().hasClass('mc-close')) $(this).next().removeClass('mc-close');
        else $(this).next().addClass('mc-close');
    });
    
    
    $('.btn-ext-s1').click(function(){
        $('.ext-step1').fadeOut(200);
        setTimeout(function(){$('.ext-step2').fadeIn(200);}, 200);
    });
    
    $('.ext-s2-btns div').click(function(){
         $('.ext-step2').fadeOut(200);
        setTimeout(function(){$('.ext-step3').fadeIn(200);}, 200);
    });

    
    $('.ext-s2-btns div').mouseover(function(){
        $(this).find('span').css('background', 'url("img/ext-s2-btnH.jpg") no-repeat left center');
    });
    
    $('.ext-s2-btns div').mouseleave(function(){
        $(this).find('span').css('background', 'url("img/ext-s2-btn.jpg") no-repeat left center');
    });
    
});



function MegaSmallScreen(){
if($(window).width() <= "590" && $("#div-globe").hasClass("col-xs-1")){
    $("#div-globe").removeClass("col-xs-1");
    $("#div-globe").addClass("col-xs-12");
    $("#div-country").removeClass("col-xs-11");
    $("#div-country").addClass("col-xs-12");
} else if($(window).width() > "590") {
    $("#div-globe").addClass("col-xs-1");
    $("#div-globe").removeClass("col-xs-12");
    $("#div-country").addClass("col-xs-11");
    $("#div-country").removeClass("col-xs-12");
    $(".input-hot").attr("placeholder", "Показания счётчика горячей воды");
    $(".input-cold").attr("placeholder", "Показания счётчика холодной воды");
} 
if($(window).width() <= "590"){
    $(".input-hot").attr("placeholder", "Горячая вода");
    $(".input-cold").attr("placeholder", "Холодная вода");
}
}

/* RATING */

// MYSTARS!!!
var mystarRating = {
  create: function(selector) {
    // loop over every element matching the selector
    $(selector).each(function() {
      var $list = $('<div></div>');
      // loop over every radio button in each container
      $(this)
        .find('input:radio')
        .each(function(i) {
          var rating = $(this).parent().text();
          var $item = $('<a href="#"></a>')
            .attr('title', rating)
            .addClass(i % 2 == 1 ? 'rating-right' : '')
            .text(rating);
          mystarRating.addHandlers($item);
          $list.append($item);
          
          if($(this).is(':checked')) {
            $item.prevAll().andSelf().addClass('rating');
          }
        });
        // Hide the original radio buttons
        $(this).append($list).find('label').hide();
    });
  },
  addHandlers: function(item) {
    $(item).click(function(e) {
      // Handle Star click
      var $star = $(this);
      var $allLinks = $(this).parent();
      
      // Set the radio button value
      $allLinks
        .parent()
        .find('input:radio[value=' + $star.text() + ']')
        .attr('checked', true);
        
      // Set the ratings
      $allLinks.children().removeClass('rating');
      $star.prevAll().andSelf().addClass('rating');
      
      // prevent default link click
      e.preventDefault();
          
    }).hover(function() {
      // Handle star mouse over
      $(this).prevAll().andSelf().addClass('rating-over');
    }, function() {
      // Handle star mouse out
      $(this).siblings().andSelf().removeClass('rating-over')
    });    
  }
  
}
// /MYSTARS!!!

// The widget STARS
var starRating = {
  create: function(selector) {
          var myRating = $(selector).attr('rating');
          console.log(myRating);
    // loop over every element matching the selector
    $(selector).each(function() {
      var $list = $('<div></div>');
      // loop over every radio button in each container
      $(this)
        .find('input:radio')
        .each(function(i) {
          var rating = $(this).parent().text();
          var $item = $('<a href="#"></a>')
            .attr('title', rating)
            .addClass(i % 2 == 1 ? 'rating-right' : '')
            .text(rating);
          
          starRating.addHandlers($item);
          $list.append($item);


          if($item.attr('title').indexOf(myRating+" ")+1) $item.prevAll().andSelf().addClass('rating-over'); 
          if($(this).is(':checked')) {
            $item.prevAll().andSelf().addClass('rating');
          }
        });
        // Hide the original radio buttons
        $(this).append($list).find('label').hide();
    });
  },
  addHandlers: function(item) {
    $(item).click(function(e) {
      // Handle Star click
      var $star = $(this);
      var $allLinks = $(this).parent();
      
      // Set the radio button value
      $allLinks
        .parent()
        .find('input:radio[value=' + $star.text() + ']')
        .attr('checked', true);
        
      // Set the ratings
      $allLinks.children().removeClass('rating');
      $star.prevAll().andSelf().addClass('rating');
      
      // prevent default link click
      e.preventDefault();
          
    }).hover(function() {

    }, function() {

    });    
  }
  
}

/*test date 2*/