<?php

namespace CabinetForm;

include_once ROOT_DIR . '/libs/mandrill/Mandrill.php';

use Mandrill;
use CabinetForm\Companies;

// Обязательные к заполнению поля
$CountersRequiredFields = array(
    "fullname" => "Ваше имя",
    "address" => "Домашний адрес",
    "input" => "Адрес электронной почты",
    "phone" => "Номер вашего телефона",
    "cold_water" => "Показатель счетчика холодной воды",
    "hot_water" => "Показатель счетчика горячей воды",
);

function get_client_ip() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	$arr = explode(",",$ipaddress);
	if($arr)
		$ipaddress = trim($arr[0]);
    return $ipaddress;
}

function checkEmail($email) {

    $email = str_replace(" ", "", $email);
    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex) {
        $isValid = false;
    } else {
        $domain = substr($email, $atIndex + 1);
        $local = substr($email, 0, $atIndex);
        $localLen = strlen($local);
        $domainLen = strlen($domain);
        if ($localLen < 1 || $localLen > 64) {
            // local part length exceeded
            $isValid = false;
        } else if ($domainLen < 1 || $domainLen > 255) {
            // domain part length exceeded
            $isValid = false;
        } else if ($local[0] == '.' || $local[$localLen - 1] == '.') {
            // local part starts or ends with '.'
            $isValid = false;
        } else if (preg_match('/\\.\\./', $local)) {
            // local part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
            // character not valid in domain part
            $isValid = false;
        } else if (preg_match('/\\.\\./', $domain)) {
            // domain part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/\\./', $domain)) {
            $isValid = false;
        } else if
        (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
            // character not valid in local part unless
            // local part is quoted
            if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
                $isValid = false;
            }
        }
        /* if ($isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
          // domain not found in DNS
          $isValid = false;
          } */
    }
    return $isValid;
}

function getValueFor($field) {

    if (!empty($_POST[$field]))
        echo $_POST[$field];
}

// Проверка заполненности полей
function formValidated($RequiredFields, $emailBody = "") {

    foreach ($RequiredFields as $key => $val) {
        if (empty($_POST[$key]))
            $emailBody .= "<span>$val</span><br>\n";
    }

    if (!empty($_POST['input']) && !checkEmail($_POST['input']))
        $emailBody .= "<span>$RequiredFields[input]</span><br>\n";

    $emailBodyT = array("status" => "0", "text" => "Форма заполнена неверно", "emailBody" => $emailBody);

    if (empty($emailBody)) {
        return array("status" => "1");
    } else
        return $emailBodyT;
}

function SubmitCounters($company) {

    global $CountersRequiredFields;

    // Уборка ненужной переменной
    unset($_POST['dosometh']);

    $error_msg = formValidated($CountersRequiredFields);

    if ($error_msg['status'] == "1") {

        // Берем временную метку
        $timestamp = date("d-m-Y, H:i:s");

        $_POST['email'] = $_POST['input'];
        unset($_POST['input']);
        $emailBody = '';
        // Разбор массива с переданными данными
        foreach ($_POST as $key => $val) {

            // Формируем текст письма
            $emailBody .= "<div><span>$key:</span> $val</div>\n";
        }

        // Стартовые заголовки
        $headers = "From: {$_POST['fullname']} <{$_POST['email']}>\r\n" .
                "CC: {$_POST['fullname']} <{$_POST['email']}>\r\n" .
                "BCC: <yuriy.yatsiv@gmail.com>\r\n" .
                "MIME-version: 1.0\n" .
                "Content-type: text/plain; charset=\"UTF-8\"";

        $file = fopen(FILELOCATION, "a");

        // fseek($file, 0, SEEK_END);/
        fwrite($file, "\n\n******************************************************************\n" .
                "*** $_POST[Дата] " .
                "\n******************************************************************\n" .
                "\n" . strip_tags($emailBody) .
                "\n******************************************************************\n\n\n");
        fflush($file);

        // Отправляем почту
        if (checkEmail($company['email'])) {
            mail($company['email'], SUBJECT, strip_tags($emailBody . "\n\nОтправлено с сайта 141400.ru/eirc/counter.php"), $headers);
        }
        // Возвразщаем массив с данными
        return array("status" => "1", "text" => "Показания счетчиков успешно отправлены", "emailBody" => $emailBody);
    }

    // И снова показываем форму		
    return "<div class='error_msg' id='id$error_msg[status]'>$error_msg[text]</div><div class='emailContent' id='id$error_msg[status]'><p  id='id$error_msg[status]'>Пожалуйста, введите правильные значения в таких полях</p> $error_msg[emailBody]</div>";
}

function printForm($company) {

    // Устанавливаем стартовые переменные
    $error_msg = array("status" => "0", "text" => "");

    $_POST['Дата'] = date("d-m-Y H:i:s");
    // Добавляем IP адрес отправителя
    $_POST['IP'] = $_SERVER['REMOTE_ADDR'];


// Проверяем, была ли отправлена форма
    if (isset($_POST['dosometh']) && $_POST['dosometh'] === "do_contact") {
        $error_msg = SubmitCounters($company);
    }

    if (!is_array($error_msg)) {
        $error = $error_msg;
    }

    $url = $company['url'];
    $company = $company['name'];
    include ASSETS_DIR . '/html/index.php';
    die;
}

function getCompanyDetails($company) {
    $result = false;
    $fh = fopen(DATA_FILE, 'r');
    while (($buffer = fgets($fh, 4096)) !== false) {
        $parts = explode('||', $buffer);
        if ($parts[0] == $company) {
            $result = array(
                'url' => $parts[0],
                'email' => $parts[1],
                'name' => $parts[2],
            );
            break;
        }
    }
    fclose($fh);
    return $result;
}

function send_email($to = '', $from = '', $subject = '', $html_content = '', $text_content = '', $headers = '') {
    # Setup mime boundaryd
    $mime_boundary = 'Multipart_Boundary_x' . md5(time()) . 'x';


    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\r\n";
    $headers .= "Content-Transfer-Encoding: 7bit\r\n";

    # Add in plain text version
    $body = "--$mime_boundary\n";
    $body .= "Content-Type: text/plain; charset=\"UTF-8\"\n";
    $body .= "Content-Transfer-Encoding: 7bit\n\n";
    $body .= $text_content;
    $body .= "\n\n";

    # Add in HTML version
    $body .= "--$mime_boundary\n";
    $body .= "Content-Type: text/html; charset=\"UTF-8\"\n";
    $body .= "Content-Transfer-Encoding: 7bit\n\n";
    $body .= $html_content;
    $body .= "\n\n";

    # Attachments would go here
    # But this whole email thing should be turned into a class to more logically handle attachments,
    # this function is fine for just dealing with html and text content.
    # End email
    $body .= "--$mime_boundary--\n"; # <-- Notice trailing --, required to close email body for mime's
    # Finish off headers
    $headers .= "From: $from\r\n";
    if (isset($_SERVER['SERVER_ADDR'])) {
        $headers .= "X-Sender-IP: " . $_SERVER['SERVER_ADDR'] . "\r\n";
    }
    $headers .= "Reply-to: " . $from . "\r\n";
    # Mail it out
    return mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $body, $headers);
}

function emailingSendMessage($to = '', $from = '', $subject = '', $html_content = '', $text_content = '', $headers = '', $async = false) {
    if (defined('EMAIL_SEND_OVER_GATEWAY') && EMAIL_SEND_OVER_GATEWAY) {
        $mandrill = new Mandrill(MANDRILL_API_KEY);
        $options = array(
            'html' => $html_content,
            'subject' => $subject,
            'from_email' => $from,
            'to' => array(
                array(
                    'email' => $to,
                    'type' => 'to'
                ),
            ),
        );
        $text_content = trim($text_content);
        if (mb_strlen($text_content) == '') {
            $options['auto_text'] = true;
        } else {
            $options['text'] = $text_content;
        }

        if (is_array($headers)) {
            $options['headers'] = $headers;
        }

        try {
            $result = $mandrill->messages->send($options, $async);
        } catch (Exception $ex) {
            $result = false;
        }
    } else {
        if ($text_content == '') {
            $text_content = strip_tags($html_content);
        }
        $result = send_email($to, $from, $subject, $html_content, $text_content, $headers);
    }
    return $result;
}

function cleanName($text) {
    $text = preg_replace('/[^a-zA-Z0-9_]/', '*', mb_strtolower($text));
    $text = preg_split('/\*/', $text, -1, PREG_SPLIT_NO_EMPTY);
    $text = implode('-', $text);

    return $text;
}

function backTranslit($var){
	
	 $langtranslit = array(		// ь ъ ё ы э
      'sch' => 'щ',  'sh'=> 'ш', 'ch' => 'ч' , 'zh' => 'ж',
	  "yi"=> "ї" , 'ui' => 'ь' , 'uu' => 'ъ', 'ei' => 'ё', 'eyi' => 'э',
	  "ye" => "є" , 'yu' => 'ю' ,  'ya'=>'я', 'uy' => 'ы',
	  'a' => 'а' , 'b' => 'б' , 'v' => 'в', 'g' => 'г' , 'd'=> 'д',
	   'e' => 'е', 'z' => 'з' ,  'i' => 'и',  'y' => 'й',  'k' => 'к',
	   'l'=> 'л' , 'm' => 'м' ,  'n' => 'н', 'o' => 'о', 'p' => 'п' ,
	  'r' => 'р' , 's'=> 'с' , 't'=> 'т', 'u'=> 'у' , 'f' => 'ф' , 'h' => 'х',
	  'c' => 'ц', '-' => ' ', "_" => "-"
    );

    $var = strtr($var, $langtranslit);
	return $var;
}

function toTranslit($var, $lower = false, $punkt = false, $dash = true) {
    //global $langtranslit;

    if (is_array($var))
        return "";

    $langtranslit = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v',
        'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ё' => 'ei', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u',
        'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
        'ь' => 'ui', 'ы' => 'uy', 'ъ' => 'uu',
        'э' => 'eyi', 'ю' => 'yu', 'я' => 'ya',
        "ї" => "yi", "є" => "ye",
        'А' => 'A', 'Б' => 'B', 'В' => 'V',
        'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
        'Ё' => 'EI', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => 'Y', 'К' => 'K',
        'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U',
        'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
        'Ь' => 'UI', 'Ы' => 'UY', 'Ъ' => 'UU',
        'Э' => 'EYI', 'Ю' => 'Yu', 'Я' => 'Ya',
        "Ї" => "YI", "Є" => "ye"
    );

    $var = trim(strip_tags($var));
    //$var = preg_replace( "/\s+/ms", "_", $var );

    $var = strtr($var, $langtranslit);
	//echo $var."<br>";
    if ($punkt)
        $var = preg_replace("/[^a-z0-9\_ \-.]+/mi", "", $var);
    else
        $var = preg_replace("/[^a-z0-9\_ \-]+/mi", "", $var);

    if ($dash) {
        $var = preg_replace('#[\-]+#i', '_', $var);
    }
	
    if ($lower)
        $var = strtolower($var);

    $var = str_ireplace(".php", "", $var);
    $var = str_ireplace(".php", ".ppp", $var);

    if (strlen($var) > 200) {

        $var = substr($var, 0, 200);

        if (($temp_max = strrpos($var, '_')))
            $var = substr($var, 0, $temp_max);
    }
	//echo $var."<br>";
    return $var;
}

function getFolderFiles($dir, $filter = null) {
    if (!is_dir($dir)) {
        return false;
    }
    $result = false;
    if (($dh = opendir($dir))) {
        while (($filename = readdir($dh)) !== false) {
            $fullname = $dir . '/' . $filename;
            if (!is_file($fullname)) {
                continue;
            }
            if (is_array($filter) && count($filter)) {
                $pathinfo = pathinfo($filename);
                if (!in_array(strtolower($pathinfo['extension']), $filter)) {
                    continue;
                }
            }
            $result[] = $filename;
        }
        closedir($dh);
    }
    return $result;
}

class Cache {

    public static function put($filename, $variable) {
		//echo CACHE_DIR . '/' . $filename."<br>";
        if (@self::file_force_contents(CACHE_DIR . '/' . $filename, serialize($variable)) === false) {
            return false;
        }
        return true;
    }

    public static function get($filename) {
		if (!is_readable(CACHE_DIR . '/' . $filename)) {
			return false;
        }
        $string = @file_get_contents(CACHE_DIR . '/' . $filename);
	    return @unserialize($string);
    }

    public static function file_force_contents($dir, $contents) {
        $parts = explode('/', $dir);
        $file = array_pop($parts);
		//echo "file = $file<br>";
        $dir = '';
        foreach ($parts as $part) {
            if (!is_dir($dir .= "/$part")) {
                mkdir($dir);
            }
        }
		$dir = substr($dir,1);										//@FIX
		//echo "put to = $dir/$file<br>";
		
        return file_put_contents("$dir/$file", $contents);
    }

    public static function cacheGetAllFiles() {
        $cache_files = false;
        if (file_exists(CACHE_DIR)) {
            $cache_files[] = CACHE_DIR;
        }
        if (file_exists(CACHE_DIR . '/region')) {
            $cache_files[] = CACHE_DIR . '/region';
        }
        if (file_exists(CACHE_DIR . '/city')) {
            $cache_files[] = CACHE_DIR . '/country';
        }
        return $cache_files;
    }

    public static function cacheCheck() {
        $files = cacheGetAllFiles();
        $result['cache_dir'] = CACHE_DIR;
        $result['total_size'] = 0;
        $result['total_files'] = 0;
        foreach ($files as $k => $file) {
            if (is_file($file)) {
                $result['total_files'] ++;
                $result['total_size'] += filesize($file);
            }
            if (is_dir($file) && !is_executable($file)) {
                $result['permissions'][$k]['filename'] = $file;
                $result['permissions'][$k]['type'] = is_dir($file) ? 1 : 0;
                $result['permissions'][$k]['executable'] = 0;
            }

            if (!is_writable($file)) {
                $result['permissions'][$k]['filename'] = $file;
                $result['permissions'][$k]['type'] = is_dir($file) ? 1 : 0;
                $result['permissions'][$k]['writable'] = 0;
            }
            if (!is_readable($file)) {
                $result['permissions'][$k]['filename'] = $file;
                $result['permissions'][$k]['type'] = is_dir($file) ? 1 : 0;
                $result['permissions'][$k]['readable'] = 0;
            }
        }
        return $result;
    }

    public static function cacheCleanAll() {
        $files = cacheGetAllFiles();
        foreach ($files as $file) {
            if (is_file($file) && is_writable($file)) {
                unlink($file);
            }
        }
    }

}

class Sitemaps {

    private static $chunk = 1024;
    private static $links_num = 5000;

    public static function updateCompanies() {
        $files = getFolderFiles(CACHE_DIR . '/' . SITEMAPS_DIR);
        if (is_array($files)) {
            foreach($files as $file) {
                unlink(CACHE_DIR . '/' . SITEMAPS_DIR . '/' . $file);
            }
        }
        $companies = Companies::getCompaniesForSitemap(1, 1);
        if (!is_array($companies)) {
            echo 'Companies empty';
            die;
        }
        $n = $companies[0]['number'];
        $p = ceil($n / self::$chunk);
        $f_num = $step = $z = 0;
        $xml_header = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $xml_footer = '</urlset>';
        $put_new = true;
        for ($i = 1; $i <= $p; $i++) {
            if ($put_new) {
                $f_num++;
                $filename[] = 'sitemap.' . $f_num . '.xml.gz';
                //file_put_contents(end($filename), $xml_header);
                $put_new = false;
                $step = 0;
                $xml = '';
            }
            $companies = Companies::getCompaniesForSitemap($i, self::$chunk);
            if (!is_array($companies)) {
                break;
            }
            
            foreach ($companies as $company) {
                $xml .= '<url><loc>' . SITE_URL . $company['company_url'] . '</loc>' .
                        '<priority>0.8</priority></url>';
                $z++;
            }
            //file_put_contents($filename, $xml, FILE_APPEND);
            $step += self::$chunk;
            if ($step > self::$links_num || $i == $p) {
                $gz = gzopen(CACHE_DIR . '/' . SITEMAPS_DIR . '/' . end($filename), 'w9');
                gzwrite($gz, $xml_header . $xml . $xml_footer);
                gzclose($gz);
                $put_new = true;
            }
        }
        if (isset($filename)) {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            $t = date('c');
            foreach($filename as $fname){
                $xml .= '<sitemap>'.
                    '<loc>' . SITE_URL . 'cache/sitemaps/' . $fname . '</loc>'.
                    '<lastmod>' . $t . '</lastmod>'.
                    '</sitemap>';
            }
            $xml .= '</sitemapindex>';
            $file = SITE_URL . 'cache/sitemaps/sitemap.xml';
            $r = file_put_contents(CACHE_DIR . '/' . SITEMAPS_DIR . '/sitemap.xml', $xml);
            if ($r) {
                echo $file;
            }
        }
    }

}
