<?php

namespace CabinetForm;

use CabinetForm\Template;
use CabinetForm\Places;
use CabinetForm\Companies;
use CabinetForm\DB;

class Frontend {

    public static function home_page() {
        global $lang;
        $template = new Template();
        $template->setPage('home');

        $region_id = isset($_COOKIE['region_id']) ? intval($_COOKIE['region_id']) : 0;
        $city_id = isset($_COOKIE['city_id']) ? intval($_COOKIE['city_id']) : 0;
		$country_id = isset($_COOKIE['country_id']) ? intval($_COOKIE['country_id']) : DEFAULT_COUNTRY;
		$filter = isset($_COOKIE['filter']) ? $_COOKIE['filter'] : CHECK_STATUS;
		if ($region_id > 0 || $city_id > 0 || $country_id > 0) {
            $result = Companies::findCompanies(1, $country_id, $region_id, $city_id, $filter);

            if (is_array($result)) {
                foreach ($result as $i => $r) {
                    unset($result[$i]['email']);
                }

                $pages_number = ceil($result[0]['number'] / ITEMS_NUMBER);
                $template->setPlaceholder('pages_number', $pages_number);
            }
			$template->setPlaceholder('filters', Frontend::getFilterNums($country_id, $region_id,  $city_id));
            $template->setPlaceholder('companies', $result);
        }

        /* $companies = Companies::getCompaniesByCountryId(DEFAULT_COUNTRY);
          if (is_array($companies)) {
          shuffle($companies);
          $companies = array_slice($companies, 0, ITEMS_NUMBER);
          }
          $template->setPlaceholder('companies', $companies); */
        $template->setTitle(sprintf(strip_tags($lang['lang_header_title']), ''));
        $template->setPlaceholder('header_title', sprintf($lang['lang_header_title'], ''));
		$template->Output();
    }

    public static function cities_json_page() {
        $country_id = isset($_REQUEST['country_id']) ? intval($_REQUEST['country_id']) : 0;
        $region_id = isset($_REQUEST['region_id']) ? intval($_REQUEST['region_id']) : 0;

        if ($region_id > 0) {
            $cities = Places::getCitiesByRegions();
            if (isset($cities[$region_id])) {
                $result = array($region_id => $cities[$region_id]);
            } else {
                $result = $cities;
            }
        } else if ($country_id > 0) {
            $cities = Places::getCitiesByCountries();
            if (isset($cities[$country_id])) {
                $cities = $cities[$country_id];
                $result = array($country_id => $cities[$country_id]);
            } else {
                $result = Places::getCitiesByCountries();
            }
        } else {
            $result = Places::getCities();
        }
        $template = new Template();
        $template->setPlaceholder('cities', $result);
        $template->Output(JSON);
    }

    public static function companies_json_page() {
        $country_id = isset($_REQUEST['country_id']) ? intval($_REQUEST['country_id']) : 0;
        $region_id = isset($_REQUEST['region_id']) ? intval($_REQUEST['region_id']) : 0;
        $city_id = isset($_REQUEST['city_id']) ? intval($_REQUEST['city_id']) : 0;
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
		$filter = isset($_COOKIE['filter']) ? $_COOKIE['filter'] : CHECK_STATUS;
        $offset = $page * ITEMS_NUMBER;

        $result = Companies::findCompanies($page, $country_id, $region_id, $city_id, $filter);

        $template = new Template();
        if (is_array($result)) {
            foreach ($result as $i => $r) {
                unset($result[$i]['email']);
            }

            $pages_number = ceil($result[0]['number'] / ITEMS_NUMBER);
            $template->setPlaceholder('pages_number', $pages_number);
        }

        $template->setPlaceholder('companies', $result);
	
       
	    $template->setPlaceholder('filters', Frontend::getFilterNums($country_id, $region_id,  $city_id));
		
        $template->Output(JSON);
    }
	
	public static function getFilterNums($country_id, $region_id,  $city_id){
			
		$as = 0;	//all
		$cs = 0;	//checked
		$bs = 0; 	//bad
		$ws = 0;	//wait
		
		$db = DB::getInstance();
		
		$where = "";
		
		
		if(!empty($country_id))
			$where = "country_id = ".$country_id;
		else
			$where = "country_id = 3159";
		if(!empty($region_id)){
			if(!empty($where))
				$where .= " AND ";
			$where .= "region_id = ".$region_id;
		}
		if(!empty($city_id)){
			if(!empty($where))
				$where .= " AND ";
			$where .= "city_id = ".$city_id;
		}
		$where1 = "WHERE status != ''";
		if(!empty($where)){
			$where1 .= " AND " . $where;
			$where = " AND " . $where;
		}
		
		
		//echo "SELECT COUNT(company_id) as cnt FROM `". TABLE_COMPANY ."` $where1<br";
		
		$res = $db->query("SELECT COUNT(company_id) as cnt FROM `". TABLE_COMPANY ."` $where1");
		if($res && mysql_num_rows($res)){
			$res = mysql_fetch_assoc($res);
			$as = $res['cnt'];
		}
		$res = $db->query("SELECT COUNT(company_id) as cnt FROM `". TABLE_COMPANY ."` WHERE status = '". CHECK_STATUS ."' $where");
		if($res && mysql_num_rows($res)){
			$res = mysql_fetch_assoc($res);
			$cs = $res['cnt'];
		}
		$res = $db->query("SELECT COUNT(company_id) as cnt FROM `". TABLE_COMPANY ."` WHERE status = '". WAIT_STATUS ."' $where");
		if($res && mysql_num_rows($res)){
			$res = mysql_fetch_assoc($res);
			$ws = $res['cnt'];
		}
		$res = $db->query("SELECT COUNT(company_id) as cnt FROM `". TABLE_COMPANY ."` WHERE status = '". BAD_STATUS ."' $where");
		if($res && mysql_num_rows($res)){
			$res = mysql_fetch_assoc($res);
			$bs = $res['cnt'];
		}
		//echo "SELECT COUNT(company_id) as cnt FROM `". TABLE_COMPANY ."` $where";
		return array(ALL_STATUS => $as, CHECK_STATUS => $cs, WAIT_STATUS => $ws, BAD_STATUS => $bs);
	}

    public static function search_companies_json_page() {
        global $search_bad_words;
        $template = new Template();
        if (!isset($_REQUEST['s']) || in_array(mb_strtolower(trim($_REQUEST['s'])), $search_bad_words)) {
            $template->setPlaceholder('status', 'FAIL');
        } else {
            $s = urldecode($_REQUEST['s']);
            if (isset($_REQUEST['page'])) {
                $n = ITEMS_NUMBER;
                $page = intval($_REQUEST['page']);
            } else {
                $n = ITEMS_SUGGESTS_NUMBER + 1;
                $page = 1;
            }

            $offset = $page * ITEMS_NUMBER;
            $companies = Companies::findCompaniesByName($page, $s, $n);
            $template->setPlaceholder('companies', $companies);
            $template->setPlaceholder('status', 'OK');
            if (is_array($companies)) {
                foreach ($companies as $i => $r) {
                    unset($companies[$i]['email']);
                }

                $pages_number = ceil($companies[0]['number'] / ITEMS_NUMBER);
                $template->setPlaceholder('pages_number', $pages_number);
            }
        }

        $template->Output(JSON);
    }

    public static function company_page() {
        global $lang;
        $company_id = 0;
        if (isset($_REQUEST['company_id'])) {
            $company_id = intval($_REQUEST['company_id']);
        } else {
            $parts = preg_split('#/#', $_REQUEST['q'], -1, PREG_SPLIT_NO_EMPTY);
            if (isset($parts[4])) {
                $id_parts = explode('/', $parts[4]);
                $company_id = intval(reset($id_parts));
            }
        }

        if ($company_id <= 0) {
            header('Location: ./');
            die;
        }
        $company = Companies::getCompanyFromCache($company_id);
        if (!isset($company['company_id'])) {
            header('Location: ./');
            die;
        }

        $db = DB::getInstance();
        $db->query("UPDATE `" . TABLE_COMPANY . "` SET `viewed`=`viewed`+1 WHERE `company_id`=" . $company['company_id']);
        $fields = array('%company%', '%city%', '%region%');
        $values = array(htmlentities($company['short_name']), htmlentities($company['city']), htmlentities($company['region']));
        $template = new Template();
        $template->setPlaceholder('meta_description', str_replace($fields, $values, $lang['lang_meta_description_company']));
        $template->setTitle(sprintf(strip_tags($lang['lang_company_header_title']), $company['short_name']));
        $template->setPage('company');
        $template->setPlaceholder('company', $company);
        $template->setPlaceholder('header_title', sprintf($lang['lang_company_header_title'], $company['short_name']));
        $template->Output();
    }

    public static function send_request_page() {
        session_start();
        global $lang;
		 $company_id = 0;
        if (isset($_REQUEST['company_id'])) {
            $company_id = intval($_REQUEST['company_id']);
        } else {
            $parts = preg_split('#/#', $_REQUEST['q'], -1, PREG_SPLIT_NO_EMPTY);
            if (isset($parts[4])) {
                $id_parts = explode('/', $parts[4]);
                $company_id = intval(reset($id_parts));
            }
        }
		  if ($company_id <= 0) {
            header('Location: ./');
            die;
        }
		$template = new Template();
		$company = Companies::getCompanyFromCache($company_id);
        if (!isset($company['company_id'])) {
            header('Location: ./');
            die;
        }
		 if (isset($_POST['fullname']) && isset($_POST['address']) && isset($_POST['email']) &&
                isset($_POST['phone']) && isset($_POST['message']) && isset($_POST['agree'])) {
			 $error = '';
           		
			session_start();
			if (!isset($_SESSION['req'])) {
			  $_SESSION['req'] = time();
			} else {
				if((time() - $_SESSION['req']) > REQUEST_LIMIT)
					unset($_SESSION['req']);
				else
					$error .= $lang['lang_send_form_limit'] . '<br/>';
			}
            if (!checkEmail($_POST['email'])) {
                $error .= $lang['lang_invalid_email'] . '<br/>';
            }
            $message = trim(strip_tags($_POST['message']));
            if ($message == '') {
                $error .= $lang['lang_message_empty'] . '<br/>';
            }
			
			$fullname = trim(strip_tags($_POST['fullname']));
				$email = trim(strip_tags($_POST['email']));
                $address = trim(strip_tags($_POST['address']));
                $phone = trim(strip_tags($_POST['phone']));
                $message = trim(strip_tags($_POST['message']));
				
			
			$db = DB::getInstance(); 
            $answ = $db->query("SELECT id FROM `". TABLE_REQUEST . "` WHERE fullname = '$fullname' AND address = '$address' AND email = '$email' AND phone = '$phone' AND message = '$message'");
			if(mysql_num_rows($answ)){
				$error = $lang['lang_send_form_dublicate'];
			}
			
			
			 if ($error == '') {
                
				print_r($_POST);
				
				$signature = str_replace('%url%', SITE_URL . $company['company_url'], $lang['lang_message_signature']);
				//$details = str_replace('%url%', SITE_URL . $company['check_details_url'], $lang['lang_message_details']);
				
                $subject = sprintf($lang['lang_request_subject'], $_POST['fullname']);
                $f = array('%fullname%', '%address%', '%phone%', '%message%', '%signature%');
                $v = array($fullname, $address, $phone, $message, $signature);
                $message = str_replace($f, $v, $lang['lang_request_body']);
               // $message .= $signature;
				//echo $message;
				//die;
                if (SITE_MODE == 'DEV') {
                    $company['email'] = "iruboss@mail.ru";
                }
                $result = emailingSendMessage($company['email'], $_POST['email'], $subject, $message);
                if ($result) {
                    $db = DB::getInstance();
                    $db->query("UPDATE `" . TABLE_COMPANY . "` SET `sent_request`=`sent_request`+1 WHERE `company_id`=" . $company['company_id']);
					
					$event_type = REQ_EVENT;
					$res = $db->query("SELECT status FROM `". TABLE_LOG_SETTINGS ."` WHERE type = '$event_type'");
					if($res && mysql_num_rows($res)){
						$res = mysql_fetch_assoc($res);
						if($res['status']){
							$db->query("INSERT INTO `" . TABLE_REQUEST . "` (fullname, address, email, phone, message, company_id)
										VALUES ('$fullname', '$address', '$email', '$phone', '$message', {$company['company_id']})");
										
							$miid = mysql_insert_id();
							$dttm = date("Y-m-d H:i:s");
							$db->query("INSERT INTO `" . TABLE_LOG . "` (dttm, event_id, event_type)
										VALUES ('$dttm', $miid, '$event_type')");
						}
					}
                }
                //$template->setError($lang['lang_request_sent']);
                $_SESSION['error'] = $lang['lang_request_sent'];
                $_SESSION['error_type'] = ERROR_TYPE_SUCCESS;
                $url = SITE_URL . $company['request_url'];
				setcookie('msg', "Данные успешно отправлены", time() + 10, COOKIE_WAY);
				$_COOKIE['msg'] = "Данные успешно отправлены";
				header('Location: ' . $url);
                die;
            }
        }


        if (isset($error) && $error != '') {
            $template->setError($error, ERROR_TYPE_WARNING);
        }
        if (isset($_SESSION['error']) && isset($_SESSION['error_type'])) {
            $template->setError($_SESSION['error'], $_SESSION['error_type']);
            unset($_SESSION['error']);
            unset($_SESSION['error_type']);
        }
        $fields = array('%company%', '%city%', '%region%');
        $values = array(htmlentities($company['short_name']), htmlentities($company['city']), htmlentities($company['region']));
        $template->setPlaceholder('meta_description', str_replace($fields, $values, $lang['lang_deta_description_request']));
        $template->setTitle(sprintf(strip_tags($lang['lang_send_request_form_title']), $company['short_name'], $company['city']));
        $template->setPage('request');
		
        $template->setPlaceholder('company', $company);
		 $template->setPlaceholder('header_title', sprintf($lang['lang_send_request_form_title'], $company['short_name'], $company['city']));
        $template->Output();
    }

    public static function send_parameters_page() {

        session_start();
        global $lang;
		
        $company_id = 0;
        if (isset($_REQUEST['company_id'])) {
            $company_id = intval($_REQUEST['company_id']);
        } else {
            $parts = preg_split('#/#', $_REQUEST['q'], -1, PREG_SPLIT_NO_EMPTY);
            if (isset($parts[4])) {
                $id_parts = explode('/', $parts[4]);
                $company_id = intval(reset($id_parts));
            }
        }

        if ($company_id <= 0) {
            header('Location: ./');
            die;
        }
		 $company = Companies::getCompanyFromCache($company_id);
        if (!isset($company['company_id'])) {
            header('Location: ./');
            die;
        }
		$template = new Template();
        if (isset($_POST['fullname']) && isset($_POST['address']) && isset($_POST['email']) &&
                isset($_POST['phone']) && isset($_POST['comment']) && isset($_POST['valueshot']) && isset($_POST['valuescold']) && isset($_POST['agree'])) {
            $error = '';
			
			session_start();
			if (!isset($_SESSION['par'])) {
				$_SESSION['par'] = time();
			} else {
				if((time() - $_SESSION['par']) > VALUES_LIMIT)
					unset($_SESSION['par']);
				else
					$error .= $lang['lang_send_form_limit'] . '<br/>';
			}
			
            foreach ($_POST as $f => $v) {
                if (!is_array($v)) {
                    $_POST[$f] = trim($v);
                }
            }
            $data = array(
                'fullname' => $_POST['fullname'],
                'address' => $_POST['address'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'comment' => $_POST['comment'],
                'valueshot' => $_POST['valueshot'],
				'valuescold' => $_POST['valuescold'],
                'agree' => $_POST['agree'],
            );
            if (empty($_POST['fullname'])) {
                $error .= $lang['lang_fullname_empty'] . '<br/>';
            }
            if (empty($_POST['address'])) {
                $error .= $lang['lang_address_empty'] . '<br/>';
            }
            if (!checkEmail($_POST['email'])) {
                $error .= $lang['lang_invalid_email'] . '<br/>';
            }
            if (empty($_POST['phone'])) {
                $error .= $lang['lang_phone_empty'] . '<br/>';
            }

            $valueshot = $_POST['valueshot'];
			$c = 0;
            foreach ($valueshot as $i => $v) {
                $v = intval($v);
                if ($v > 0) {
					if($c>0)
						$values_to_send[] = $lang['lang_value_hot'] . ' '.$c.': ' . $v;
					else
						$values_to_send[] = $lang['lang_value_hot'] . ': ' . $v;
					$c++;
                }
            }
			$c = 0;
            $valuescold = $_POST['valuescold'];
            foreach ($valuescold as $i => $v) {
				if ($v > 0) {
					if($c>0)
						$values_to_send[] = $lang['lang_value_cold'] . ' '.$c.': ' . $v;
					else
						$values_to_send[] = $lang['lang_value_cold'] . ': ' . $v;
					$c++;
                }
            }
            if (!isset($values_to_send)) {
                $error .= $lang['lang_values_invalid'] . '<br/>';
            }
			
			$fullname = trim(strip_tags($_POST['fullname']));
			$address = trim(strip_tags($_POST['address']));
			$email = trim(strip_tags($_POST['email']));
			$phone = trim(strip_tags($_POST['phone']));
			$comment = trim(strip_tags($_POST['comment']));
               
            if ($error == '') {
                $values = '';
                foreach ($values_to_send as $v) {
                    $values .=  $v . '<br/>';
                }

                $signature = str_replace('%url%', SITE_URL . $company['company_url'], $lang['lang_message_signature']);
				//$details = str_replace('%url%', SITE_URL . $company['check_details_url'], $lang['lang_message_details']);
				
                $subject = sprintf($lang['lang_values_subject'], $_POST['fullname']);
                $f = array('%company%', '%fullname%', '%address%', '%email%', '%phone%', '%counters%', '%comment%', '%signature%');
                $v = array($company['short_name'], $fullname, $address, $email, $phone, $values, $comment, $signature);

                ob_start();
                include ASSETS_DIR . '/chunks/parameters_tpl.php';
                $html = ob_get_clean();
                ob_end_clean();

                $message = str_replace($f, $v, $html);
				//echo $message;
				//die;
                if (SITE_MODE == 'DEV') {
                    $company['email'] = ADMIN_EMAIL;
                }
				$result = emailingSendMessage($company['email'], $_POST['email'], $subject, $message);
                if ($result) {
                    $db = DB::getInstance();
                    $db->query("UPDATE `" . TABLE_COMPANY . "` SET `sent_values`=`sent_values`+1 WHERE `company_id`=" . $company['company_id']);
					
					
					$event_type = PARAM_EVENT;
					$res = $db->query("SELECT status FROM `". TABLE_LOG_SETTINGS ."` WHERE type = '$event_type'");
					if($res && mysql_num_rows($res)){
						$res = mysql_fetch_assoc($res);
						if($res['status']){
							$db->query("INSERT INTO `" . TABLE_PARAMETR . "` (fullname, address, email, phone, comment, company_id)
										VALUES ('$fullname', '$address', '$email', '$phone', '$comment', {$company['company_id']})");
							$miid = mysql_insert_id();
							$dttm = date("Y-m-d H:i:s");
						
							$db->query("INSERT INTO `" . TABLE_LOG . "` (dttm, event_id, event_type)
										VALUES ('$dttm', $miid, '$event_type')");
						}
					}
                }
                $_SESSION['error'] = $lang['lang_values_sent'];
                $_SESSION['error_type'] = ERROR_TYPE_SUCCESS;
                $url = SITE_URL . $company['send_parameters_url'];
				setcookie('msg', "Данные успешно отправлены", time() + 10, COOKIE_WAY);
				$_COOKIE['msg'] = "Данные успешно отправлены";
                header('Location: ' . $url);
                die;
            } else {
                $template->setPlaceholder('data', $data);
            }
        }

        if (isset($error) && $error != '') {
            $template->setError($error, ERROR_TYPE_WARNING);
        }
        if (isset($_SESSION['error']) && isset($_SESSION['error_type'])) {
            $template->setError($_SESSION['error'], $_SESSION['error_type']);
            unset($_SESSION['error']);
            unset($_SESSION['error_type']);
        }
        $fields = array('%company%', '%city%', '%region%');
        $values = array(htmlentities($company['short_name']), htmlentities($company['city']), htmlentities($company['region']));
        $template->setPlaceholder('meta_description', str_replace($fields, $values, $lang['lang_meta_description_send_parameters']));
        $template->setTitle(sprintf(strip_tags($lang['lang_send_parameter_form_title']), $company['short_name']));
        $template->setPage('value');
        $template->setPlaceholder('company', $company);
        $template->setPlaceholder('header_title', sprintf($lang['lang_send_parameter_form_title'], $company['short_name']));
        $template->Output();
    }
	
	 public static function send_details_page() {

        session_start();
        global $lang;
		
        $company_id = 0;
        if (isset($_REQUEST['company_id'])) {
            $company_id = intval($_REQUEST['company_id']);
        } else {
            $parts = preg_split('#/#', $_REQUEST['q'], -1, PREG_SPLIT_NO_EMPTY);
            if (isset($parts[4])) {
                $id_parts = explode('/', $parts[4]);
                $company_id = intval(reset($id_parts));
            }
        }

        if ($company_id <= 0) {
            header('Location: ./');
            die;
        }
		 $company = Companies::getCompanyFromCache($company_id);
        if (!isset($company['company_id'])) {
            header('Location: ./');
            die;
        }
		$template = new Template();
        if (isset($_POST['fullname']) && isset($_POST['address']) && isset($_POST['email']) &&
                isset($_POST['phone']) && isset($_POST['comment']) && isset($_POST['values']) && isset($_POST['agree'])) {
            $error = '';
			
			
			
            foreach ($_POST as $f => $v) {
                if (!is_array($v)) {
                    $_POST[$f] = trim($v);
                }
            }
            $data = array(
                'fullname' => $_POST['fullname'],
                'address' => $_POST['address'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'comment' => $_POST['comment'],
                'values' => $_POST['values'],
                'agree' => $_POST['agree'],
            );
            if (empty($_POST['fullname'])) {
                $error .= $lang['lang_fullname_empty'] . '<br/>';
            }
            if (empty($_POST['address'])) {
                $error .= $lang['lang_address_empty'] . '<br/>';
            }
            if (!checkEmail($_POST['email'])) {
                $error .= $lang['lang_invalid_email'] . '<br/>';
            }
            if (empty($_POST['phone'])) {
                $error .= $lang['lang_phone_empty'] . '<br/>';
            }

            $values = $_POST['values'];
            foreach ($values as $i => $v) {
                $v = intval($v);
                if ($v > 0) {
                    $values_to_send[] = $v;
                }
            }
            if (!isset($values_to_send)) {
                $error .= $lang['lang_values_invalid'] . '<br/>';
            }

            if ($error == '') {
                $fullname = trim(strip_tags($_POST['fullname']));
                $address = trim(strip_tags($_POST['address']));
                $email = trim(strip_tags($_POST['email']));
                $phone = trim(strip_tags($_POST['phone']));
                $comment = trim(strip_tags($_POST['comment']));
                $values = '';
                foreach ($values_to_send as $v) {
                    $values .= $lang['lang_value'] . ': ' . $v . '<br/>';
                }

                $signature = str_replace('%url%', SITE_URL, $lang['lang_message_signature']);
                $subject = sprintf($lang['lang_values_subject'], $_POST['fullname']);
                $f = array('%company%', '%fullname%', '%address%', '%email%', '%phone%', '%counters%', '%comment%', '%signature%');
                $v = array($company['short_name'], $fullname, $address, $email, $phone, $values, $comment, $signature);

              
                /*if (SITE_MODE == 'DEV') {
                    $company['email'] = ADMIN_EMAIL;
                }*/
				
               // $result = emailingSendMessage($company['email'], $_POST['email'], $subject, $message);
               /* if ($result) {
                    $db = DB::getInstance();
                    $db->query("UPDATE `" . TABLE_COMPANY . "` SET `sent_values`=`sent_values`+1 WHERE `company_id`=" . $company['company_id']);
                }*/
                $_SESSION['error'] = $lang['lang_values_sent'];
                $_SESSION['error_type'] = ERROR_TYPE_SUCCESS;
                $url = SITE_URL . $company['send_parameters_url'];
                header('Location: ' . $url);
                die;
            } else {
                $template->setPlaceholder('data', $data);
            }
        }

        if (isset($error) && $error != '') {
            $template->setError($error, ERROR_TYPE_WARNING);
        }
        if (isset($_SESSION['error']) && isset($_SESSION['error_type'])) {
            $template->setError($_SESSION['error'], $_SESSION['error_type']);
            unset($_SESSION['error']);
            unset($_SESSION['error_type']);
        }
        $fields = array('%company%', '%city%', '%region%');
        $values = array($company['short_name'], $company['city'], $company['region']);
		$template->setTitle(sprintf(strip_tags(str_replace($fields, $values, $lang['lang_send_details_form_title']))));
        $template->setPage('details');
        $template->setPlaceholder('company', $company);
        $template->setPlaceholder('header_title', str_replace($fields, $values, $lang['lang_send_details_form_title']));
		$template->setPlaceholder('meta_description', str_replace($fields, $values, $lang['lang_meta_description_details']));
        
        $template->Output();
    }
	
	public static function new_company_page() {
        global $lang;

        $template = new Template();
        $template->setTitle($lang['add_company']);
        $template->setPage('new_company');
        $template->setPlaceholder('header_title', $lang['add_company']);
		$template->setPlaceholder('countries', Places::getCountries());
        $template->Output();
    }
	
	public static function regions_by_country_json_page(){
		if (isset($_REQUEST['country_id'])) {
			$cid =  trim(addslashes($_REQUEST['country_id'])); 
			
			
			$arr = Places::getRegions();
			$res = null;
			foreach($arr as $c){
				if($c["country_id"] == $cid)
					$res[] = array("region_id"=>$c["region_id"], "name"=>$c["name"]);
			}
			if(!$res)
				Template::JSONFailResult('');
			Template::JSONOKResult($res);
		}
		
	}
	
	public static function cities_by_region_json_page(){
		if (isset($_REQUEST['region_id'])) {
			$rid =  trim(addslashes($_REQUEST['region_id'])); 
			
			
			$arr = Places::getCities();
			$res = null;
			foreach($arr as $c){
				if($c["region_id"] == $rid)
					$res[] = array("city_id"=>$c["city_id"], "name"=>$c["name"]);
			}
			if(!$res)
				Template::JSONFailResult('');
			Template::JSONOKResult($res);
		}
		
	}

    public static function agreement_page() {
        global $lang;

        $template = new Template();
        $template->setTitle($lang['lang_agreement']);
        $template->setPage('agreement');
        $template->setPlaceholder('header_title', $lang['lang_agreement']);
        $template->Output();
    }

    public static function about_page() {
        global $lang;

        $template = new Template();
        $template->setTitle($lang['lang_about']);
        $template->setPage('about');
        $template->setPlaceholder('header_title', '<a>' . $lang['lang_about'] . '</a>');
        $template->Output();
    }

    public static function contacts_page() {
        session_start();
        global $lang;

        $template = new Template();

        if (isset($_POST['fullname']) && isset($_POST['email']) &&
                isset($_POST['message'])) {
            $error = '';
            if (!checkEmail($_POST['email'])) {
                $error .= $lang['lang_invalid_email'] . '<br/>';
            }
            $message = trim(strip_tags($_POST['message']));
            if ($message == '') {
                $error .= $lang['lang_message_empty'] . '<br/>';
            }

            if ($error == '') {
                $fullname = trim(strip_tags($_POST['fullname']));
                $email = trim(strip_tags($_POST['email']));
                $phone = trim(strip_tags(isset($_POST['phone']) ? $_POST['phone'] : ''));
                $message = trim(strip_tags($_POST['message']));
                if ($phone == '') {
                    $phone = $lang['lang_not_set'];
                }

                $subject = sprintf($lang['lang_message_subject'], $fullname);
                $f = array('%fullname%', '%email%', '%phone%', '%message%');
                $v = array($fullname, $email, $phone, $message);
                $message = str_replace($f, $v, $lang['lang_message_body']);
                $message .= str_replace('%url%', SITE_URL, $lang['lang_message_signature']);
                $result = emailingSendMessage(ADMIN_EMAIL, $email, $subject, $message);
                $_SESSION['error'] = $lang['lang_message_sent'];
                $_SESSION['error_type'] = ERROR_TYPE_SUCCESS;
                header('Location: contacts');
                die;
            }
        }


        if (isset($error) && $error != '') {
            $template->setError($error, ERROR_TYPE_WARNING);
        }
        if (isset($_SESSION['error']) && isset($_SESSION['error_type'])) {
            $template->setError($_SESSION['error'], $_SESSION['error_type']);
            unset($_SESSION['error']);
            unset($_SESSION['error_type']);
        }
        $template->setTitle($lang['lang_contacts']);
        $template->setPage('contacts');
        $template->setPlaceholder('header_title', '<a>' . $lang['lang_contacts'] . '</a>');
        $template->Output();
    }

    public static function search_page() {
        global $lang, $search_bad_words;
        $template = new Template();
        if (!isset($_REQUEST['s']) || in_array(mb_strtolower(trim($_REQUEST['s'])), $search_bad_words)) {
            //$template->setPlaceholder('status', 'FAIL');
        } else {
            $companies = Companies::findCompaniesByName(1, $_REQUEST['s'], ITEMS_NUMBER);
			$template->setPlaceholder('companies', $companies);
            $pages_number = ceil($companies[0]['number'] / ITEMS_NUMBER);
            $template->setPlaceholder('pages_number', $pages_number);
			$s = str_replace("/","",strip_tags($_REQUEST['s']));
            $template->setPlaceholder('s', $s);
        }

        $template->setTitle($lang['lang_companies_search']);
        $template->setPage('search');
        $template->setPlaceholder('header_title', '<a>' . $lang['lang_companies_search'] . '</a>');
        $template->Output();
    }
	
	public static function set_block_json_page(){
		if (isset($_REQUEST['company_id']) && isset($_REQUEST['why']) && isset($_REQUEST['why_more']) && isset($_REQUEST['who'])) {
			$cid =  trim(addslashes($_REQUEST['company_id'])); 
			$why =  trim(addslashes($_REQUEST['why']));
			$why_more =  trim(addslashes($_REQUEST['why_more']));
			$who =  trim(addslashes($_REQUEST['who']));
			
			$bdate = date("Y-m-d H:i:s");
			
			$db = DB::getInstance();
					
			$sql = "INSERT INTO `blocked_companies` (companyid, who, why, why_more, bdate) VALUES($cid,'$who','$why','$why_more','$bdate')";
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			
			$event_type = BLOCK_EVENT;
			$res = $db->query("SELECT status FROM `". TABLE_LOG_SETTINGS ."` WHERE type = '$event_type'");
			if($res && mysql_num_rows($res)){
				$res = mysql_fetch_assoc($res);
				if($res['status']){
					
			
					$miid = mysql_insert_id();
					$dttm = date("Y-m-d H:i:s");
					$db->query("INSERT INTO `" . TABLE_LOG . "` (dttm, event_id, event_type)
								VALUES ('$dttm', $miid, '$event_type')");
				}
			}
						
			$sql = "UPDATE `form_company` SET isblocked = 1 WHERE company_id = $cid";
			$db = DB::getInstance();
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			
			Companies::updateCompanyCache($cid);
			Companies::updateCompaniesCache();
			
			Template::JSONOKResult('');
		}
		
	}
	
	public static function set_unblock_json_page(){
		if (isset($_REQUEST['company_id'])) {
			$cid =  trim(addslashes($_REQUEST['company_id'])); 
			
			$sql = "UPDATE `form_company` SET isblocked = 0 WHERE company_id = $cid";
			$db = DB::getInstance();
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			
			
			$event_type = UNBLOCK_EVENT;
			$res = $db->query("SELECT status FROM `". TABLE_LOG_SETTINGS ."` WHERE type = '$event_type'");
			if($res && mysql_num_rows($res)){
				$res = mysql_fetch_assoc($res);
				if($res['status']){
					$miid = $cid;
					$dttm = date("Y-m-d H:i:s");
					$db->query("INSERT INTO `" . TABLE_LOG . "` (dttm, event_id, event_type)
								VALUES ('$dttm', $miid, '$event_type')");
				}
			}
			
			Companies::updateCompanyCache($cid);
			Companies::updateCompaniesCache();
			
			Template::JSONOKResult('');
		}
		
	}
	
	public static function set_bad_company_json_page(){
		if (isset($_REQUEST['company_id'])) {
			$cid =  trim(addslashes($_REQUEST['company_id'])); 
			$status = BAD_STATUS;
			$sql = "UPDATE `form_company` SET status = '$status' WHERE company_id = $cid";
			$db = DB::getInstance();
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			
			Companies::updateCompanyCache($cid);
			Companies::updateCompaniesCache();
			
			Template::JSONOKResult('');
		}
		
	}
	
	public static function set_good_company_json_page(){
		if (isset($_REQUEST['company_id'])) {
			$cid =  trim(addslashes($_REQUEST['company_id'])); 
			$status = CHECK_STATUS;
			$sql = "UPDATE `form_company` SET status = '$status' WHERE company_id = $cid";
			$db = DB::getInstance();
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			$sql = "DELETE FROM `form_company_temp` WHERE main_form_id = $cid";
			$result = $db->query($sql);
			
			Companies::updateCompanyCache($cid);
			Companies::updateCompaniesCache();
			
			Template::JSONOKResult('');
		}
		
	}
	
	public static function set_good_company_mass_json_page(){
		if (isset($_REQUEST['companies'])) {
			$companies =  explode(",",trim(addslashes($_REQUEST['companies']))); 
			$status = CHECK_STATUS;
			$ids = "";
			if(is_array($companies)){
				foreach($companies as $c){
					if(!empty($c)){
						$ids .= $c.",";
					}
				}
				$ids = substr($ids,0, strlen($ids)-1);
				$sql = "UPDATE `form_company` SET status = '$status' WHERE company_id IN($ids)";
				$db = DB::getInstance();
				$result = $db->query($sql);
				if (!$result) {
					Template::JSONFailResult('');
				}
				$sql = "DELETE FROM `form_company_temp` WHERE main_form_id IN($ids)";
				$result = $db->query($sql);
				
				Companies::updateCompanyCache($cid);
				Companies::updateCompaniesCache();
				
				Template::JSONOKResult('');
			}else
				Template::JSONFailResult('');
		}
		
	}
	
	public static function set_bad_company_mass_json_page(){
		if (isset($_REQUEST['companies'])) {
			$companies =  explode(",",trim(addslashes($_REQUEST['companies']))); 
			$status = BAD_STATUS;
			$ids = "";
			if(is_array($companies)){
				foreach($companies as $c){
					if(!empty($c)){
						$ids .= $c.",";
					}
				}
				$ids = substr($ids,0, strlen($ids)-1);
				$sql = "UPDATE `form_company` SET status = '$status' WHERE company_id IN($ids)";
				$db = DB::getInstance();
				$result = $db->query($sql);
				if (!$result) {
					Template::JSONFailResult('');
				}
				
				Companies::updateCompanyCache($cid);
				Companies::updateCompaniesCache();
				
				Template::JSONOKResult('');
			}else
				Template::JSONFailResult('');
		}
		
	}
	
	public static function delete_company_mass_json_page(){
		if (isset($_REQUEST['companies'])) {
			$companies =  explode(",",trim(addslashes($_REQUEST['companies']))); 
			$status = BAD_STATUS;
			$ids = "";
			if(is_array($companies)){
				foreach($companies as $c){
					if(!empty($c)){
						$ids .= $c.",";
					}
				}
				$ids = substr($ids,0, strlen($ids)-1);
				$cid =  trim(addslashes($_REQUEST['company_id'])); 
				$sql = "DELETE FROM `form_company` WHERE company_id IN($ids)";
				$db = DB::getInstance();
				$result = $db->query($sql);
				
				$sql = "DELETE FROM `form_company_temp` WHERE main_form_id IN($ids)";
				$result = $db->query($sql);
				
				
				Companies::updateCompanyCache($cid);
				Companies::updateCompaniesCache();
				
				Template::JSONOKResult('');
			}else
				Template::JSONFailResult('');
		}
		
	}
	
	
	public static function delete_company_json_page(){
		if (isset($_REQUEST['company_id'])) {
			$cid =  trim(addslashes($_REQUEST['company_id'])); 
			$sql = "DELETE FROM `form_company` WHERE company_id = $cid";
			$db = DB::getInstance();
			$result = $db->query($sql);
			
			$sql = "DELETE FROM `form_company_temp` WHERE main_form_id = $cid";
			$result = $db->query($sql);
			
			
			Companies::updateCompanyCache($cid);
			Companies::updateCompaniesCache();
			
			Template::JSONOKResult('');
		}
		
	}
	
	
	public static function set_new_details_json_page(){
		
		if (isset($_REQUEST['manager']) && isset($_REQUEST['email']) && isset($_REQUEST['phone']) && isset($_REQUEST['who'])
										&& isset($_REQUEST['address']) && isset($_REQUEST['cid']) && isset($_REQUEST['name']) && isset($_REQUEST['short_name'])
										&& isset($_REQUEST['site']) && isset($_REQUEST['agree']) && isset($_REQUEST['mail_sender']) 
										&& isset($_REQUEST['name_sender']) &&  isset($_REQUEST['ip'])) {
			global $isAdmin;
			global $lang;
			if(!$isAdmin){								
				session_start();
				if (!isset($_SESSION['det'])) {
					$_SESSION['det'] = time();
				} else {
					if((time() - $_SESSION['det']) > DETAILS_LIMIT)
						unset($_SESSION['det']);
					else
						Template::JSONFailResult($lang['lang_send_form_limit']);
				}
			}
			
			$agree =  trim(addslashes($_REQUEST['agree'])); 								
			if(!$agree)
				Template::JSONFailResult('');
			
			
			$ip =  trim(addslashes($_REQUEST['ip'])); 
			$mail_sender =  trim(addslashes($_REQUEST['mail_sender'])); 
			$name_sender =  trim(addslashes($_REQUEST['name_sender'])); 
			$manager =  trim(addslashes($_REQUEST['manager'])); 
			$email =  trim(addslashes($_REQUEST['email']));
			$phone =  trim(addslashes($_REQUEST['phone']));
			$who =  trim(addslashes($_REQUEST['who']));
			$address =  trim(addslashes($_REQUEST['address']));
			$cid =  trim(addslashes($_REQUEST['cid']));
			$name =  trim(addslashes($_REQUEST['name']));
			$short_name =  trim(addslashes($_REQUEST['short_name']));
			$site =  trim(addslashes($_REQUEST['site']));
			$bdate = date("Y-m-d H:i:s");
			
			$db = DB::getInstance();
			
			$sql = "INSERT INTO `check_details` (manager, email, phone, who, bdate, address, company_id, name, short_name, site, name_sender, mail_sender, ip)
					VALUES('$manager','$email','$phone','$who','$bdate', '$address', $cid, '$name', '$short_name', '$site', '$name_sender', '$mail_sender', '$ip')";
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			
			
			$event_type = DETAIL_EVENT;
			$res = $db->query("SELECT status FROM `". TABLE_LOG_SETTINGS ."` WHERE type = '$event_type'");
			if($res && mysql_num_rows($res)){
				$res = mysql_fetch_assoc($res);
				if($res['status']){
					
					$miid = mysql_insert_id();
					$dttm = date("Y-m-d H:i:s");
					$db->query("INSERT INTO `" . TABLE_LOG . "` (dttm, event_id, event_type)
								VALUES ('$dttm', $miid, '$event_type')");
				}
			}
						
			if($isAdmin){
				$sql = "UPDATE `form_company` SET manager = '$manager', email = '$email',
						phone = '$phone', address = '$address', name = '$name', short_name = '$short_name',
						site = '$site', mod_date = '$bdate' WHERE company_id = $cid";
				$db = DB::getInstance();
				$result = $db->query($sql);
				if (!$result) {
					Template::JSONFailResult('');
				}
			}
			
			Companies::updateCompanyCache($cid);
			Companies::updateCompaniesCache();
			
			Template::JSONOKResult("");
		}
		
	}
	
	public static function add_new_company_json_page(){
		
		if (isset($_REQUEST['manager']) && isset($_REQUEST['email']) && isset($_REQUEST['phone']) && isset($_REQUEST['who'])
										&& isset($_REQUEST['address']) && isset($_REQUEST['name']) && isset($_REQUEST['short_name'])
										&& isset($_REQUEST['site']) && isset($_REQUEST['agree']) && isset($_REQUEST['mail_sender']) 
										&& isset($_REQUEST['name_sender']) &&  isset($_REQUEST['ip']) &&  isset($_REQUEST['country_id'])
										&&  isset($_REQUEST['region_id']) &&  isset($_REQUEST['city_id']) && isset($_REQUEST['comment'])) {
			global $isAdmin;
			global $lang;
			if(!$isAdmin){								
				session_start();
				if (!isset($_SESSION['add'])) {
					$_SESSION['add'] = time();
				} else {
					if((time() - $_SESSION['add']) > ADD_LIMIT)
						unset($_SESSION['add']);
					else
						Template::JSONFailResult($lang['lang_send_form_limit']);
				}
			}
			
			$agree =  trim(addslashes($_REQUEST['agree'])); 								
			if(!$agree)
				Template::JSONFailResult('');
			
			
			$region_id =  trim(addslashes($_REQUEST['region_id']));
			$comment =  trim(addslashes($_REQUEST['comment'])); 			
			$city_id =  trim(addslashes($_REQUEST['city_id'])); 
			$country_id =  trim(addslashes($_REQUEST['country_id'])); 
			$ip =  trim(addslashes($_REQUEST['ip'])); 
			$mail_sender =  trim(addslashes($_REQUEST['mail_sender'])); 
			$name_sender =  trim(addslashes($_REQUEST['name_sender'])); 
			$manager =  trim(addslashes($_REQUEST['manager'])); 
			$email =  trim(addslashes($_REQUEST['email']));
			$phone =  trim(addslashes($_REQUEST['phone']));
			$who =  trim(addslashes($_REQUEST['who']));
			$address =  trim(addslashes($_REQUEST['address']));
			$cid =  0;
			$name =  trim(addslashes($_REQUEST['name']));
			$short_name =  trim(addslashes($_REQUEST['short_name']));
			$site =  trim(addslashes($_REQUEST['site']));
			$bdate = date("Y-m-d H:i:s");
			$status = WAIT_STATUS;
			
			$db = DB::getInstance();
			
			$sql = "SELECT company_id FROM `form_company` WHERE manager = '$manager' AND  email = '$email' AND phone = '$phone' AND name = '$name' AND short_name = '$short_name'";
			$result = $db->query($sql);
			if($result){
				Template::JSONFailResult($lang['lang_send_form_dublicate']);
			}
			
			
			if(!empty($email)){
				$sql = "SELECT company_id FROM `form_company` WHERE email = '$email'";
				
				$result = $db->query($sql);
				if (mysql_num_rows($result)) {
					$result = mysql_fetch_assoc($result);
					$cid = $result['company_id'];
				}
			}
			
			$sql = "INSERT INTO `form_company` (manager, email, phone, address, name, short_name, site, country_id, region_id, city_id, status, add_date, mod_date)
					VALUES('$manager','$email','$phone','$address', '$name', '$short_name', '$site',  $country_id, $region_id, $city_id, '$status', '$bdate', '$bdate')";
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			
			$mfid = mysql_insert_id();
			$sql = "INSERT INTO `form_company_temp` (main_form_id, manager, email, phone, who, bdate, address, company_id_original, name, short_name, site, name_sender, mail_sender, ip, country_id, region_id, city_id, comment)
			VALUES($mfid,'$manager','$email','$phone','$who','$bdate', '$address', $cid, '$name', '$short_name', '$site', '$name_sender', '$mail_sender', '$ip', $country_id, $region_id, $city_id, '$comment')";
			$result = $db->query($sql);
			if (!$result) {
				Template::JSONFailResult('');
			}
			
			$event_type = ADD_NEW_EVENT;
			$res = $db->query("SELECT status FROM `". TABLE_LOG_SETTINGS ."` WHERE type = '$event_type'");
			if($res && mysql_num_rows($res)){
				$res = mysql_fetch_assoc($res);
				if($res['status']){
					
					$miid = mysql_insert_id();
					$dttm = date("Y-m-d H:i:s");
					$db->query("INSERT INTO `" . TABLE_LOG . "` (dttm, event_id, event_type)
								VALUES ('$dttm', $miid, '$event_type')");
				}
			}
			Companies::updateCompaniesCache();
			Template::JSONOKResult("");
		}
		
	}

    public static function set_location_json_page() {
        if (isset($_REQUEST['country_id'])) {
            $country_id = intval($_REQUEST['country_id']);
            setcookie('country_id', $country_id, time() + 31536000, COOKIE_WAY );
			if (isset($_COOKIE['region_id']) && intval($_COOKIE['region_id'])) {
                $region_id = intval($_COOKIE['region_id']);
                $regions_by_countries = Places::getRegionsByCountry();
                $regions = isset($regions_by_countries[$region_id]) ? $regions_by_countries[$region_id] : 0;
                if (!is_array($regions) || !in_array($region_id, $regions)) {
                    setcookie('region_id', 0, time() + 31536000, COOKIE_WAY);
				}
            }
            if (isset($_COOKIE['city_id']) && intval($_COOKIE['city_id'])) {
                $city_id = intval($_COOKIE['city_id']);
                $city = Places::getCity($city_id);
                setcookie('sdfsdf', print_r($city, true));
                if ($city['country_id'] != $country_id) {
                    setcookie('city_id', 0, time() + 31536000, COOKIE_WAY);
                }
            }
        }

        if (isset($_REQUEST['region_id'])) {
            setcookie('region_id', intval($_REQUEST['region_id']), time() + 31536000, COOKIE_WAY);
            if (isset($_COOKIE['city_id']) && intval($_COOKIE['city_id'])) {
                $city_id = intval($_COOKIE['city_id']);
                $city = Places::getCity($city_id);
                if ($city['country_id'] != intval($_COOKIE['region_id'])) {
                    setcookie('city_id', 0, time() + 31536000, COOKIE_WAY);
                }
            }
        }

        if (isset($_REQUEST['city_id'])) {
            if (!isset($_COOKIE['region_id']) || intval($_COOKIE['region_id']) == 0) {
                $city = Places::getCity($_REQUEST['city_id']);
                setcookie('region_id', $city['region_id'], time() + 31536000, COOKIE_WAY);
            }
            setcookie('city_id', intval($_REQUEST['city_id']), time() + 31536000, COOKIE_WAY);
        }
        Template::JSONOKResult('');
    }

    
    public static function services_page() {
        global $lang;
        $template = new Template();
        $template->setTitle($lang['lang_services_page']);
        $template->setPage('services');
        $template->setPlaceholder('header_title', '<a>' . $lang['lang_services_page'] . '</a>');
        $template->Output();
    }
}
