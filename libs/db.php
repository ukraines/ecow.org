<?php

namespace CabinetForm;

class DB {
    protected $db;
    
    public static function getInstance() {
        static $instance = null;
        if ($instance == null) {
            $instance = new static();
        }
        return $instance;
    }
    
    protected function __construct() {
        $this->db = mysql_pconnect(DB_HOST, DB_USERNAME, DB_PASSWORD);
        if ($this->db === false) {
            return;
        }
        mysql_select_db(DB_NAME);
        mysql_set_charset('utf8');
    }
    
    public function __call($name, $arguments) {
        $method = 'mysql_' . $name;
        if (function_exists($method)) {
            return call_user_func_array($method, $arguments);
            //return $method(reset($arguments));
        }
    }
    
    public function num_rows($result) {
        if (!is_resource($result) || !mysql_num_rows($result)) {
            return false;
        }
        return true;
    }
}
