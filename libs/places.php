<?php
use CabinetForm\DB;

namespace CabinetForm;

class Places {
    
    public static function updateCountriesCache() {
        $countries = Places::_getCountries();
        Cache::put(COUNTRIES_FILE, $countries);
    }
    public static function updateCountriesByCountryIdCache() {
        $countries = Places::_getCountriesByCountryId();
        Cache::put(COUNTRIES_BY_COUNTRY_ID_FILE, $countries);
    }
    public static function updateRegionsCache() {
        $regions = Places::_getRegions();
        Cache::put(REGIONS_FILE, $regions);
    }
    public static function updateRegionsByCountryCache() {
        $regions = Places::_getRegionsByCountry();
        Cache::put(REGIONS_BY_COUNTRIES_FILE, $regions);
    }
    public static function updateRegionsByRegionIdCache() {
        $regions = Places::_getRegionsByRegionId();
        Cache::put(REGIONS_BY_REGION_ID_FILE, $regions);
    }
    public static function updateCitiesCache() {
        $cities = Places::_getCities();
        Cache::put(CITIES_FILE, $cities);
    }
    public static function updateCitiesByCityIdCache() {
        $cities = Places::_getCitiesByCityId();
        Cache::put(CITIES_BY_CITY_ID_FILE, $cities);
    }
    public static function updateCitiesByRegionsCache() {
        $cities = Places::_getCitiesByRegions();
        Cache::put(CITIES_BY_REGIONS_FILE, $cities);
    }
    public static function updateCitiesByCountriesCache() {
        $cities = Places::_getCitiesByCountries();
        Cache::put(CITIES_BY_COUNTRIES_FILE, $cities);
    }
    
    protected static function _getCitiesByCityId() {
        $cities = self::_getCities();
        if ($cities === false) {
            return false;
        }
        $r = false;
        foreach($cities as $city) {
            $city_id = $city['city_id'];
            $r[$city_id] = array(
                'country_id' => $city['country_id'],
                'region_id' => $city['region_id'],
                'city_id' => $city['city_id'],
                'name' => $city['name'],
            );
        }
        return $r;
    }
    
    protected static function _getCitiesByCountries() {
        $cities = self::_getCities();
        if ($cities === false) {
            return false;
        }
        $r = false;
        foreach($cities as $city) {
            $country_id = $city['country_id'];
            $r[$country_id][] = array(
                'country_id' => $city['country_id'],
                'region_id' => $city['region_id'],
                'city_id' => $city['city_id'],
                'name' => $city['name'],
            );
        }
        return $r;
    }
    
    protected static function _getCitiesByRegions() {
        $cities = self::_getCities();
        if ($cities === false) {
            return false;
        }
        $r = false;
        foreach($cities as $city) {
            $region_id = $city['region_id'];
            $r[$region_id][] = array(
                'country_id' => $city['country_id'],
                'region_id' => $city['region_id'],
                'city_id' => $city['city_id'],
                'name' => $city['name'],
            );
        }
        return $r;
    }
    protected static function _getRegionsByCountry() {
        $regions = self::_getRegions();
        if ($regions === false) {
            return false;
        }
        $r = false;
        foreach($regions as $region) {
            $country_id = $region['country_id'];
            $r[$country_id][] = array(
                'region_id' => $region['region_id'],
                'name' => $region['name'],
            );
        }
        return $r;
    }
    protected static function _getRegionsByRegionId() {
        $regions = self::_getRegions();
        if ($regions === false) {
            return false;
        }
        $r = false;
        foreach($regions as $region) {
            $region_id = $region['region_id'];
            $r[$region_id] = $region;
        }
        return $r;
    }
    protected static function _getCountriesByCountryId() {
        $countries = self::_getCountries();
        if ($countries === false) {
            return false;
        }
        $r = false;
        foreach($countries as $country) {
            $country_id = $country['country_id'];
            $r[$country_id] = $country;
        }
        return $r;
    }
    
    protected static function _getCities() {
        global $allowed_countries;
        $clause = '';
        if (is_array($allowed_countries) && count($allowed_countries)) {
            foreach($allowed_countries as $i => $v) {
                $allowed_countries[$i] = intval($v);
            }
            $values = implode(',', $allowed_countries);
            $clause = "WHERE ct.country_id IN ($values) ";
        }
        $sql = "SELECT ct.* FROM `" . TABLE_REGION . "` r ".
		"JOIN `" . TABLE_CITY . "` ct ON ct.region_id=r.region_id ".
                $clause .
                "ORDER BY TRIM(ct.`name`)";
        $db = DB::getInstance();
        $result = $db->query($sql);
        if (!$db->num_rows($result)) {
            return false;
        }
        
        while($row = $db->fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }
    
    protected static function _getRegions() {
        global $allowed_countries;
        $clause = '';
        if (is_array($allowed_countries) && count($allowed_countries)) {
            foreach($allowed_countries as $i => $v) {
                $allowed_countries[$i] = intval($v);
            }
            $values = implode(',', $allowed_countries);
            $clause = "WHERE ct.country_id IN ($values) ";
        }
        $sql = "SELECT r.* FROM `" . TABLE_REGION . "` r ".
		"JOIN `" . TABLE_CITY . "` ct ".
		"ON ct.region_id=r.region_id ".
                $clause .
                "GROUP BY r.region_id ORDER BY r.`name`";
        $db = DB::getInstance();
        $result = $db->query($sql);
        if (!$db->num_rows($result)) {
            return false;
        }
        
        while($row = $db->fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }
    
    protected static function _getCountries() {
        global $allowed_countries;
        $clause = '';
        if (is_array($allowed_countries) && count($allowed_countries)) {
            foreach($allowed_countries as $i => $v) {
                $allowed_countries[$i] = intval($v);
            }
            $values = implode(',', $allowed_countries);
            $clause = "WHERE c.country_id IN ($values) ";
        }
        $sql = "SELECT c.country_id,c.`name` ".
                "FROM `" . TABLE_COUNTRY . "` c JOIN `" . TABLE_REGION . "` r ".
                "ON c.country_id = r.country_id ".
		"JOIN `" . TABLE_CITY . "` ct ON ct.region_id=r.region_id ".
                $clause .
                "GROUP BY c.country_id ORDER BY c.`name` ";
        $db = DB::getInstance();
        $result = $db->query($sql);
        if (!$db->num_rows($result)) {
            return false;
        }
        
        while($row = $db->fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }
    
    public static function getCountries() {
        $countries = Cache::get(COUNTRIES_FILE);
        if ($countries === false) {
            self::updateCountriesCache();
            $countries = Cache::get(COUNTRIES_FILE);
        }
        return $countries;
    }
    
    public static function getCountriesByCountryId() {
        $countries = Cache::get(COUNTRIES_BY_COUNTRY_ID_FILE);
        if ($countries === false) {
            self::updateCountriesByCountryIdCache();
            $countries = Cache::get(COUNTRIES_BY_COUNTRY_ID_FILE);
        }
        return $countries;
    }
    
    public static function getCities() {
        $cities = Cache::get(CITIES_FILE);
        if ($cities === false) {
            self::updateCitiesCache();
            $cities = Cache::get(CITIES_FILE);
        }
        return $cities;
    }
    
    public static function getCitiesByCityId() {
        $cities = Cache::get(CITIES_BY_CITY_ID_FILE);
        if ($cities === false) {
            self::updateCitiesByCityIdCache();
            $cities = Cache::get(CITIES_BY_CITY_ID_FILE);
        }
        return $cities;
    }
    
    public static function getRegions() {
        $regions = Cache::get(REGIONS_FILE);
        if ($regions === false) {
            self::updateRegionsCache();
            $regions = Cache::get(REGIONS_FILE);
        }
        return $regions;
    }
    
    public static function getRegionsByCountry() {
        $regions = Cache::get(REGIONS_BY_COUNTRIES_FILE);
        if ($regions === false) {
            self::updateRegionsByCountryCache();
            $regions = Cache::get(REGIONS_BY_COUNTRIES_FILE);
        }
        return $regions;
    }
    
    public static function getRegionsByRegionId() {
        $regions = Cache::get(REGIONS_BY_REGION_ID_FILE);
        if ($regions === false) {
            self::updateRegionsByRegionIdCache();
            $regions = Cache::get(REGIONS_BY_REGION_ID_FILE);
        }
        return $regions;
    }
    
    public static function getCitiesByRegions() {
        $cities = Cache::get(CITIES_BY_REGIONS_FILE);
        if ($cities === false) {
            self::updateCitiesByRegionsCache();
            $cities = Cache::get(CITIES_BY_REGIONS_FILE);
        }
        return $cities;
    }
    
    public static function getCitiesByCountries() {
        $cities = Cache::get(CITIES_BY_COUNTRIES_FILE);
        if ($cities === false) {
            self::updateCitiesByCountriesCache();
            $cities = Cache::get(CITIES_BY_COUNTRIES_FILE);
        }
        return $cities;
    }
    
    public static function getCity($city_id) {
        $city_id = intval($city_id);
        $db = DB::getInstance();
        $result = $db->query("SELECT * FROM `" . TABLE_CITY . "` WHERE `city_id`=$city_id");
        if (!$db->num_rows($result)) {
            return false;
        }
        
        return $db->fetch_assoc($result);
    }
}

class Companies {
    protected static function _getCompanies(){
		global $allowed_countries;
        $clause = '';
        $sql = "SELECT c.*,ct.country_id FROM `" . TABLE_COMPANY . "` c ".
		"JOIN `" . TABLE_CITY . "` ct ON c.region_id=ct.region_id AND c.city_id=ct.city_id ".
                "ORDER BY TRIM(c.`short_name`)";
        $db = DB::getInstance();
        $result = $db->query($sql);
        if (!$db->num_rows($result)) {
            return false;
        }
        
        while($row = $db->fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }
    
    protected static function _getCompaniesByCountries(&$companies){
        if ($companies === false) {
            return false;
        }
        $r = false;
        foreach($companies as $company) {
            $country_id = $company['country_id'];
            $r[$country_id][] = $company;
        }
        return $r;
    }
    
    protected static function _getCompaniesByRegions(&$companies){
        if ($companies === false) {
            return false;
        }
        $r = false;
        foreach($companies as $company) {
            $region_id = $company['region_id'];
            $r[$region_id][] = $company;
        }
        return $r;
    }
    
    protected static function _getCompaniesByCities(&$companies){
        if ($companies === false) {
            return false;
        }
        $r = false;
        foreach($companies as $company) {
            $city_id = $company['city_id'];
            $r[$city_id][] = $company;
        }
        return $r;
    }
    
    public static function updateCompaniesCache() {
        $companies = self::_getCompanies();
        Cache::put(COMPANIES_FILE, $companies);
        
        Cache::put(COMPANIES_BY_COUNTRIES_FILE, self::_getCompaniesByCountries($companies));
        Cache::put(COMPANIES_BY_REGIONS_FILE, self::_getCompaniesByRegions($companies));
        Cache::put(COMPANIES_BY_CITIES_FILE, self::_getCompaniesByCities($companies));
    }
    
    public static function getCompanyFromCache($company_id) {
        $company_id = intval($company_id);
        $company = Cache::get(COMPANY_DIR . '/' . $company_id);
        if (!is_array($company)) {
            if (!self::updateCompanyCache($company_id)) {
                return false;
            }
            $company = Cache::get(COMPANY_DIR . '/' . $company_id);
        }
        return $company;
    }

    public static function updateCompanyCache($company_id){
        if ($company_id <= 0) {
            return false;
        }
        $company = self::getCompany($company_id);
        if (!is_array($company)) {
            return false;
        }
        
        return Cache::put(COMPANY_DIR . '/' . $company['company_id'], $company);
    }
    
    private static function getCompany($company_id) {
        $company_id = intval($company_id);
        if ($company_id <= 0) {
            return false;
        }
        $sql = "SELECT co.*,cn.`name` AS `country`,r.`name` AS `region`,ct.`name` AS `city`".
                "FROM `" . TABLE_COMPANY . "` co LEFT JOIN ".
                "`" . TABLE_COUNTRY . "` cn ON co.country_id=cn.country_id LEFT JOIN ".
                "`" . TABLE_REGION . "` r ON co.region_id=r.region_id LEFT JOIN ".
                "`" . TABLE_CITY . "` ct ON co.city_id=ct.city_id ".
                "WHERE co.company_id=$company_id";
        $db = DB::getInstance();
        $result = $db->query($sql);
        if (!is_resource($result) || !$db->num_rows($result)) {
            return false;
        }
        $row = $db->fetch_assoc($result);
        $row['url'] = false;
        $url = parse_url($row['site']);
        if (isset($url['scheme'])) {
            $row['url'] = true;
        }
        self::seoNames($row);
        return $row;
    }
	    
    public static function getCompanies() {
        $companies = Cache::get(COMPANIES_FILE);
        if ($companies === false) {
            self::updateCompaniesCache();
            $companies = Cache::get(COMPANIES_FILE);
        }
        return $companies;
    }
    
    public static function getCompaniesByCountries() {
        $companies = Cache::get(COMPANIES_BY_COUNTRIES_FILE);
        if ($companies === false) {
            self::updateCompaniesCache();
            $companies = Cache::get(COMPANIES_BY_COUNTRIES_FILE);
        }
		//echo "get comp";
        return $companies;
    }
    
    public static function getCompaniesByRegions() {
		$companies = Cache::get(COMPANIES_BY_REGIONS_FILE);
        if ($companies === false) {
            self::updateCompaniesCache();
            $companies = Cache::get(COMPANIES_BY_REGIONS_FILE);
        }
        return $companies;
    }
    
    public static function getCompaniesByCities() {
        $companies = Cache::get(COMPANIES_BY_CITIES_FILE);
        if ($companies === false) {
            self::updateCompaniesCache();
            $companies = Cache::get(COMPANIES_BY_CITIES_FILE);
        }
        return $companies;
    }
    
    public static function getCompaniesByCountryId($country_id) {
        $country_id = intval($country_id);
        if ($country_id <= 0) {
            return false;
        }
        $companies = self::getCompaniesByCountries();
        if ($companies === false || !isset($companies[$country_id])) {
            return false;
        }
        
        return $companies[$country_id];
    }
    
    public static function findCompanies($page, $country_id, $region_id, $city_id, $filter) {
        $db = DB::getInstance();
        global $isAdmin;
		if(!$isAdmin)
			$filter = CHECK_STATUS;
        $country_id = intval($country_id);
        $region_id = intval($region_id);
        $city_id = intval($city_id);
        $page--;
        if ($page < 0) {
            $page = 0;
        }
        $start = $page * ITEMS_NUMBER;
        
        if ($country_id > 0) {
            $clauses[] = "`country_id`=$country_id";
        }
        if ($region_id > 0) {
            $clauses[] = "`region_id`=$region_id";
        }
        if ($city_id > 0) {
            $clauses[] = "`city_id`=$city_id";
        }
		if (!empty($filter) && $filter != ALL_STATUS ) {
            $clauses[] = "`status`='$filter'";
        }
        if (isset($clauses)) {
            $clauses = 'WHERE ' . implode(' AND ', $clauses);
        } else {
            $clauses = '';
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS *,"
                . "(SELECT `name` FROM `". TABLE_COUNTRY."` WHERE `country_id`=c.`country_id`) AS `country`, "
                . "(SELECT `name` FROM `". TABLE_REGION."` WHERE `region_id`=c.`region_id`) AS `region`, "
                . "(SELECT `name` FROM `". TABLE_CITY."` WHERE `city_id`=c.`city_id`) AS `city` "
                . "FROM `" . TABLE_COMPANY . "` c $clauses LIMIT $start," . ITEMS_NUMBER;
				
        //echo $sql;
        $result = $db->query($sql);
        if (!is_resource($result) || !$db->num_rows($result)) {
			
            return false;
        }
        
        while($row = $db->fetch_assoc($result)) {
            $row['short_name_short'] = false;
            if (mb_strlen($row['short_name']) > COMPANY_NAME_LENGTH) {
                $row['short_name_short'] = true;
            }
            $row['address_short'] = false;
            if (mb_strlen($row['address']) > COMPANY_ADDRESS_LENGTH) {
                $row['address_short'] = true;
            }
            self::seoNames($row);
            $rows[] = $row;
        }
        if (!isset($rows)) {
            return false;
        }
        $result = $db->query("SELECT FOUND_ROWS() AS `n`");
		if($rows){
			$arri = $db->fetch_assoc($result);
			$rows[0]['number'] = $arri['n'];
		}
		return $rows;
    }
    
    public static function findCompaniesByName($page, $name, $limit = ITEMS_SUGGESTS_NUMBER) {
        global $allowed_countries;
        $db = DB::getInstance();
        
        $name = $db->real_escape_string($name);
		$name = str_replace("/","",$name);
        $page--;
        if ($page < 0) {
            $page = 0;
        }
        $start = $page * $limit;
        
        $clause = '';
        if (is_array($allowed_countries)) {
            $clause = 'country_id IN (' . implode(',', $allowed_countries) . ') AND';
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS *,".
                "(SELECT `name` FROM `". TABLE_COUNTRY."` WHERE `country_id`=c.`country_id` LIMIT 1) AS `country`, ".
                "(SELECT `name` FROM `". TABLE_REGION."` WHERE `region_id`=c.`region_id` LIMIT 1) AS `region`, ".
                "(SELECT `name` FROM `". TABLE_CITY."` WHERE `city_id`=c.`city_id` LIMIT 1) AS `city` ".
                "FROM `" . TABLE_COMPANY . "` c WHERE $clause short_name like '%$name%' LIMIT $start,$limit";
        //echo $sql;
        $result = $db->query($sql);
        if (!is_resource($result) || !$db->num_rows($result)) {
            return false;
        }
        
        while($row = $db->fetch_assoc($result)) {
            if (is_null($row['city'])) {
                $row['city'] = '';
            }
            self::seoNames($row);
            $rows[] = $row;
        }
        if (!isset($rows)) {
            return false;
        }
        $result = $db->query("SELECT FOUND_ROWS()");
        $row = $db->fetch_array($result);
        $rows[0]['number'] = $row[0];
        return $rows;
    }
    
    private static function seoNames(&$row) {
        $row['short_name_seo'] = cleanName(toTranslit($row['short_name']));
        $row['country_seo'] = cleanName(toTranslit($row['country']));
        if ($row['country_seo'] == '') {
            $row['country_seo'] = 'c';
        }
        $row['region_seo'] = cleanName(toTranslit($row['region']));
        if ($row['region_seo'] == '') {
            $row['region_seo'] = 'r';
        }
		
        $row['city_seo'] = cleanName(toTranslit($row['city']));
        if ($row['city_seo'] == '') {
            $row['city_seo'] = 'c';
        }
		$row['request_url'] = $row['country_seo'] . '/' . $row['region_seo'] . '/' . 
                        $row['city_seo'] . '/' . $row['company_id'] . '-' . $row['short_name_seo'] . '/request.html';
        $row['send_parameters_url'] = $row['country_seo'] . '/' . $row['region_seo'] . '/' . 
                        $row['city_seo'] . '/' . $row['company_id'] . '-' . $row['short_name_seo'] . '/counters.html';
        $row['company_url'] = $row['country_seo'] . '/' . $row['region_seo'] . '/' . 
                        $row['city_seo'] . '/' . $row['company_id'] . '-' . $row['short_name_seo'] . '.html';
		$row['check_details_url'] = $row['country_seo'] . '/' . $row['region_seo'] . '/' . 
				$row['city_seo'] . '/' . $row['company_id'] . '-' . $row['short_name_seo'] . '/details.html';
        
    }
    
    public static function getCompaniesForSitemap($page, $limit = ITEMS_NUMBER) {
        $db = DB::getInstance();
        
        $limit = intval($limit);
        $page--;
        if ($page < 0) {
            $page = 0;
        }
        $start = $page * $limit;
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS *,"
                . "(SELECT `name` FROM `". TABLE_COUNTRY."` WHERE `country_id`=c.`country_id`) AS `country`, "
                . "(SELECT `name` FROM `". TABLE_REGION."` WHERE `region_id`=c.`region_id`) AS `region`, "
                . "(SELECT `name` FROM `". TABLE_CITY."` WHERE `city_id`=c.`city_id`) AS `city` "
                . "FROM `" . TABLE_COMPANY . "` c WHERE c.city_id>0 LIMIT $start," . $limit;
        //echo $sql;
        $result = $db->query($sql);
        if (!is_resource($result) || !$db->num_rows($result)) {
            return false;
        }
        
        while($row = $db->fetch_assoc($result)) {
            $row['short_name_short'] = false;
            if (mb_strlen($row['short_name']) > COMPANY_NAME_LENGTH) {
                $row['short_name_short'] = true;
            }
            $row['address_short'] = false;
            if (mb_strlen($row['address']) > COMPANY_ADDRESS_LENGTH) {
                $row['address_short'] = true;
            }
            self::seoNames($row);
            $rows[] = $row;
        }
        if (!isset($rows)) {
            return false;
        }
        $result = $db->query("SELECT FOUND_ROWS() AS `n`");
		$arr = mysql_fetch_assoc($result);
        $rows[0]['number'] = $arr['n'];
        return $rows;
    }
}