<?php

use CabinetForm\Places;
use CabinetForm\Companies;
use CabinetForm\Sitemaps;
use CabinetForm\Template;

namespace CabinetForm;

class Admin {
    
    public function __construct() {
        //session_start();
    }
    
    public function home_page() {
        if (!$this->isAuth()) {
            $this->login_page();
        }
		$this->checkCSVFile();
        $template = new Template();
        
        $template->setTitle('');
        $template->setPage('admin_home');
        $template->setIndex('admin');
        $template->Output();
    }
	
	private function checkCSVFile(){
		if(isset($_FILES['csv'])){
			$uploaddir = LOADS_DIR;
			$uploadfile = $uploaddir . basename($_FILES['csv']['name']);

			echo '<pre>';
			if (move_uploaded_file($_FILES['csv']['tmp_name'], $uploadfile)) {
				echo "Файл корректен и был успешно загружен.\n";
			} else {
				echo "Возможная атака с помощью файловой загрузки!\n";
			}
			
			$this->addCompaniesArr($this->parseCSV($uploadfile));
			
			echo "</pre>";
		}
	}
	
	private function parseCSV($file_path){
		 $csv_lines  = file($file_path);
		  if(is_array($csv_lines))
		  {
			$cnt = count($csv_lines);
			for($i = 0; $i < $cnt; $i++)
			{
			  $line = $csv_lines[$i];
			  $line = trim($line);
			  $first_char = true;
			  $col_num = 0;
			  $length = strlen($line);
			  for($b = 0; $b < $length; $b++)
			  {
				 if($skip_char != true)
				{
				   $process = true;
				  if($first_char == true)
				  {
					if($line[$b] == "'")
					{
					  $terminator = "',";
					  $process = false;
					}
					else
					  $terminator = ',';
					$first_char = false;
				  }

				  if($line[$b] == "'")
				  {
					$next_char = $line[$b + 1];
					if($next_char == "'")
					  $skip_char = true;
				   elseif($next_char == ',')
					{
					  if($terminator == "',")
					  {
						$first_char = true;
						$process = false;
						$skip_char = true;
					  }
					}
				  }
				if($process == true)
				  {
					if($line[$b] == ',')
					{
					   if($terminator == ',')
					   {

						  $first_char = true;
						  $process = false;
					   }
					}
				  }

				  if($process == true)
					$column .= $line[$b];

				  if($b == ($length - 1))
				  {
					$first_char = true;
				  }

				  if($first_char == true)
				  {

					$values[$i][$col_num] = $column;
					$column = '';
					$col_num++;
				  }
				}
				else
				  $skip_char = false;
			  }
			}
		  }
		  return $values;
	}
	
	public function addCompaniesArr($arr){
		if($arr){
			$errArr = null;
			$db = DB::getInstance();
			$total = count($arr);
			$good = 0;
			$bad = 0;
			$cnt = 0;
			foreach($arr as $c){
				$cnt++;
				$country = $c[0];
				$region = $c[1];
				$city = trim($c[2]);
				$name = $c[3];
				$short_name = $name;
				$manager = $c[4];
				$phone = $c[5];
				$address = addslashes($c[6]);
				$email = $c[7];
				$site = $c[8];
				$arri = $this->getIdsByNameCity($db, $city);
				if($arri){
					$country_id = $arri['country_id'];
					$region_id = $arri['region_id'];
					$city_id = $arri['city_id'];
					
					$res = $db->query("INSERT INTO `form_company` (name, country_id, region_id, city_id, region, county, address, phone, short_name, email, site, manager, isblocked, status) 
									 VALUES('$name', $country_id, $region_id, $city_id, '$region', '$city', '$address', '$phone', '$short_name', '$email', '$site', '$manager', 0, `". CHECK_STATUS ."`)");
					if($res){
						$good++;
					}
					else{
						$bad++;
						$errArr[] = array("city"=>$city, "name"=>$name, "row"=>$cnt, "type"=>"add err");
					}
					
				}else{
					$errArr[] = array("city"=>$city, "name"=>$name, "row"=>$cnt, "type"=>"city err");
				}
			}
			echo "Всего строк: $total из них успешно добавлены: $good, с ошибками: $bad<br>";
			if($errArr){
				echo "Список ошибок: город, название, строка в файле, type";
				print_r($errArr);
			}
			Companies::updateCompaniesCache();
		}else
			echo " произошла ошибка парсинга ";
	}
	
	private function getIdsByNameCity(&$db, $city){
		$res = $db->query("SELECT city_id, region_id, country_id FROM `form_city` WHERE name = '$city'");
		if($res && mysql_num_rows($res) ){
			return mysql_fetch_assoc($res);
		}
		return false;
	}
    
    public function update_page() {
        if (!$this->isAuth()) {
            $this->login_page();
        }
        Places::updateCountriesCache();
		Places::updateCitiesCache();
        Places::updateRegionsCache();
        Places::updateRegionsByCountryCache();
        Places::updateCitiesByRegionsCache();
        Places::updateCitiesByCountriesCache();
        Companies::updateCompaniesCache();
        echo 'Cache updated';
        die;
    }
    
    public function sitemaps_page() {
        if (!$this->isAuth()) {
            $this->login_page();
        }
        Sitemaps::updateCompanies();
        die;
    }
    
    public function login_page() {
        if ($this->isAuth()) {
            header('Location: ' . SITE_URL . 'admin');die;
        }
        if (isset($_POST['login']) && isset($_POST['password'])) {
            if ($_POST['login'] == SITE_USER && sha1($_POST['password']) == SITE_PASSWORD) {
                setcookie('isAdmin_d3', "1_1", time() + 31536000, COOKIE_WAY);
				header( "Location: ". SITE_URL ."admin/home/");
            }
        }
		//echo "here ".$_POST['login'];
		//exit;
        $template = new Template();
        
        $template->setTitle('Login');
        $template->setPage('login');
        $template->setIndex('admin');
        $template->Output();
    }
    
    private function isAuth(){
        if (isset($_COOKIE['isAdmin_d3'])) {
            if ($_COOKIE['isAdmin_d3'] == "1_1") {
                return true;
            }
        }
        return false;
    }
}