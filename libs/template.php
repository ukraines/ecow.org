<?php

namespace CabinetForm;

class Template {

    private $index;
    private $placeholders = array();

    public function Output($type = '') {
        global $lang;

		 if (!isset($this->placeholders['error'])) {
            $this->setError('');
        }

        switch ($type) {
            case 'json':
                $this->placeholders['memory'] = memory_get_peak_usage();
                $this->placeholders['page_time'] = (microtime(true) - $GLOBALS['start_time']);
                echo json_encode($this->placeholders);
                break;
            default:
				$this->preload();
                extract($this->placeholders);
                extract($lang);
                if (!isset($title)) {
                    $title = '';
                }
                
                include(TEMPLATE_DIR . '/' . $this->index);
                echo '<!--' . memory_get_peak_usage() . '-->' . "\n";
                echo '<!--' . (microtime(true) - $GLOBALS['start_time']) . '-->';
                
        }
        die;
    }

    function __construct() {
        $this->index = 'index.php';
    }

    public function setPage($page) {
        $this->placeholders['page'] = TEMPLATE_DIR . '/' . $page . '.php';
    }

    public function setPlaceholder($name, $value) {
        $this->placeholders[$name] = $value;
    }
    
    public function isPlaceholderExists($name) {
        return isset($this->placeholders[$name]);
    }

    public function setIndex($index) {
        $this->index = $index . '.php';
    }

    public function setTitle($title) {
        $this->setPlaceholder('title', $title);
    }

    public function setError($error, $error_type = '') {
        global $lang;
        if ($error_type == '') {
            $error_type = ERROR_TYPE_SUCCESS;
        }
        if (strlen($error) < 1) {
            $error_type .= ' hide';
        }
        /*if (strlen($error) > 0 && $error_type == ERROR_TYPE_WARNING) {
            $datetime = date("d-m-Y H:i:s");
            $ip = $_SERVER['REMOTE_ADDR'];
            $data['datetime'] = $datetime;
            $data['ip'] = $ip;
            $send_data = array_merge($_REQUEST, $data);
            $text_content = $lang['lang_error'] . ": " . $error . "\n" . print_r($send_data, true);
            $html_content = nl2br($text_content);
            emailingSendMessage(ADMIN_EMAIL, ADMIN_EMAIL, $lang['lang_error'], $html_content, $text_content);
        }*/
        $this->setPlaceholder('error_type', $error_type);
        $this->setPlaceholder('error', $error);
    }
    
    static public function JSONFailResult($message) {
        echo json_encode(array('status' => 'FAIL', 'message' => $message));
        die;
    }
    
    static public function JSONOKResult($message) {
        echo json_encode(array('status' => 'OK', 'message' => $message));
        die;
    }
    
    static public function JSONResult($assoc_array) {
        echo json_encode($assoc_array);
        die;
    }

    public function preload() {
        global $lang;
        $countries = Places::getCountriesByCountryId();
        $this->setPlaceholder('countries', $countries);
        
        $regions = Places::getRegionsByRegionId();
        $this->setPlaceholder('regions', $regions);
        
        $cities = Places::getCitiesByCityId();
        $this->setPlaceholder('cities', $cities);
        
       
	
        
        if (isset($_COOKIE['city_id']) && intval($_COOKIE['city_id']) > 0) {
            $id = intval($_COOKIE['city_id']);
            $choosen_place = isset($cities[$id])?$cities[$id]['name']:'';
            $this->setPlaceholder('choosen_place', $choosen_place);
        } else if (isset($_COOKIE['region_id']) && intval($_COOKIE['region_id']) > 0){
            $id = intval($_COOKIE['region_id']);
            $choosen_place = isset($regions[$id])?$regions[$id]['name']:'';
            $this->setPlaceholder('choosen_place', $choosen_place);
        } else if (isset($_COOKIE['country_id']) && intval($_COOKIE['country_id']) > 0){
            $id = intval($_COOKIE['country_id']);
            $choosen_place = isset($countries[$id])?$countries[$id]['name']:'';
            $this->setPlaceholder('choosen_place', $choosen_place);
        } else {
            $id = DEFAULT_COUNTRY;
            $choosen_place = isset($countries[$id])?$countries[$id]['name']:'';
            $this->setPlaceholder('choosen_place', $choosen_place);
        }
        
        $this->setPlaceholder('city_id', 0);
        $this->setPlaceholder('region_id', 0);
        $this->setPlaceholder('country_id', DEFAULT_COUNTRY);
        
        $this->setPlaceholder('selected_city', $lang['lang_select_city']);
        $this->setPlaceholder('selected_region', $lang['lang_select_region']);
        $this->setPlaceholder('selected_country', $countries[DEFAULT_COUNTRY]['name']);
		if (isset($_COOKIE['city_id'])) {
            $id = intval($_COOKIE['city_id']);
            $this->setPlaceholder('city_id', $id);
            $this->setPlaceholder('selected_city', isset($cities[$id])?$cities[$id]['name']:$lang['lang_select_city']);
        }
        if (isset($_COOKIE['region_id'])){
            $id = intval($_COOKIE['region_id']);
            $this->setPlaceholder('region_id', $id);
            $this->setPlaceholder('selected_region', isset($regions[$id])?$regions[$id]['name']:$lang['lang_select_region']);
        }
        if (isset($_COOKIE['country_id'])){
			$id = intval($_COOKIE['country_id']);
            $this->setPlaceholder('country_id', $id);
            $this->setPlaceholder('selected_country', isset($countries[$id])?$countries[$id]['name']:$countries[DEFAULT_COUNTRY]['name']);
        }
		
    }
}