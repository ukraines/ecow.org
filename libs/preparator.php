<?
	namespace CabinetForm;

	class Preparator {
		
		private $ip;
		private $country_code;
		private $lang_now;
		private $lang_code;
		private $isAdmin;
		private $needRedir;
		
		public function __construct($_ip, $_country_code){
			$this->ip = $_ip;
			$this->country_code = strtolower($_country_code);
			$this->lang_code = strtolower($_country_code);
			$this->needRedir = false;
			$this->isAdmin = false;
			$this->lang_now = "Русский";
		}
		
		public function prepareFirstTimeByLink(){
			$routes = count(explode('/', $_SERVER['REQUEST_URI']));
			$startnum = count(explode('/',SITE_URL));
			if(!isset($_COOKIE['isft']) && $routes > $startnum ){
				$arrLangs = array("/ru/","/ua/","/by/","/ro/");
				for($i=0; $i<count($arrLangs); $i++){
					if(strpos($_SERVER['REQUEST_URI'],$arrLangs[$i]) !== false){
						$this->country_code = str_replace("/","",$arrLangs[$i]);
						$this->lang_code = $this->country_code;
						setcookie('isft',  1, time() + 31536000, COOKIE_WAY);
						switch(strtolower($this->lang_code)){
							case "ro":
								setcookie('lang',  "Молдовеняскэ", time() + 31536000, COOKIE_WAY);
								break;
							case "ru":
								setcookie('lang',  "Русский", time() + 31536000, COOKIE_WAY);
								break;
							case "ua":
								setcookie('lang',  "Українська", time() + 31536000, COOKIE_WAY);
								break;
							default:
								setcookie('lang',  "Русский", time() + 31536000, COOKIE_WAY);
								break;	
						}
						break;
					}
				}
			}
		}
		
		public function prepareCountryCode(){
			switch($this->country_code){							//УТОЧНИТЬ
				case "ro":
					$this->lang_now =  "Молдовеняскэ";
					if(!isset($_COOKIE['isft'])){
						setcookie('country_id',  2788, time() + 31536000, COOKIE_WAY);
						setcookie('isft',  1, time() + 31536000, COOKIE_WAY);
					}
					break;
				case "ru":
					$this->lang_now =  "Русский";
					if(!isset($_COOKIE['isft'])){
						setcookie('country_id',  3159, time() + 31536000, COOKIE_WAY);
						setcookie('isft',  1, time() + 31536000, COOKIE_WAY);
					}
					break;
				case "ua":
					$this->lang_now =  "Українська";
					if(!isset($_COOKIE['isft'])){
						setcookie('country_id',  9908, time() + 31536000, COOKIE_WAY);
						setcookie('isft',  1, time() + 31536000, COOKIE_WAY);
					}
					break;
				case "by":
					if(!isset($_COOKIE['isft'])){
						setcookie('country_id',  248, time() + 31536000, COOKIE_WAY);
						setcookie('isft',  1, time() + 31536000, COOKIE_WAY);
					}
					break;
				default:
					$this->lang_code = "ru";
					break;
			}
		}
		
		public function prepareAdmin(){
			if(isset($_GET['admin'])){
				if($_GET['admin'] == "admin" && strpos(ADMIN_IP,$this->ip) !== false && SITE_MODE == "DEV"){
					$this->isAdmin = true;
					setcookie('isAdmin_d3', "1_1", time() + 31536000, COOKIE_WAY);
				}
			}else{
				if(isset($_COOKIE['isAdmin_d3'])){
					if($_COOKIE['isAdmin_d3'] == "1_1"){
						$this->isAdmin = true;
					}
				}//check cookie
			}
		}
		
		public function isAdmin(){
				return $this->isAdmin;
		}
		
		public function prepareCookieLang(){
			if(isset($_COOKIE['lang'])){
				switch($_COOKIE['lang']){
					case "Молдовеняскэ":
						$this->lang_now =  "Молдовеняскэ";
						$this->lang_code = "ro";
						break;
					case "Русский":
						$this->lang_now =  "Русский";
						$this->lang_code = "ru";
						break;
					case "Українська":
						$this->lang_code = "ua";
						$this->lang_now =  "Українська";
						break;
					default:
						$this->lang_now =  "Русский";
						$this->lang_code = "ru";
						break;
				}
			}
		}
		
		public function cancelCache(){
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
				header("Last-Modified: " . gmdate("D, d M Y H:i:s")." GMT");
				header("Cache-Control: no-cache, must-revalidate");
				header("Cache-Control: post-check=0,pre-check=0", false);
				header("Cache-Control: max-age=0", false);
				header("Pragma: no-cache");
		}
		
		public function prepareLang(){
			$this->lang_code = strtolower($this->lang_code)."/";
			$arrLangs = array("/ru/","/ua/","/by/","/ro/");
			if((strpos($_SERVER['REQUEST_URI'],$this->lang_code) === false || $this->moreThenOne($_SERVER['REQUEST_URI'],$arrLangs)) && strpos($_SERVER['REQUEST_URI'],"json") === false
							&& strpos($_SERVER['REQUEST_URI'],"favicon.ico") === false && strpos($_SERVER['REQUEST_URI'],"admin") === false){		//not json
				$this->needRedir = true;
				$addr = SITE_URL . $this->lang_code;
				$routes = explode('/', $_SERVER['REQUEST_URI']);
				foreach($routes as $part){
					if($part){
						//echo $part."<br>";
						if(strlen($part)>3)
							if(strpos(SITE_URL,$part) !== false){
								unset($part);
								continue;
							}
						if(!in_array("/".$part."/",$arrLangs))
							$addr .= $part."/";
					}
				}
				
				if(strpos($routes[count($routes)-1],'.html') !== false)
					$addr = substr($addr,0,strlen($addr)-1);
				
				$this->makeRedir($addr);										//BUG
			}
		}
		
		private function moreThenOne($addr, $arr){
			$cnt = 0;
			for($i=0; $i<count($arr); $i++){
				if(strpos($addr,$arr[$i]) !== false)
					$cnt++;
			}
			
			return $cnt > 1;
		}
		
		public function getLangCode(){
			return strtolower($this->lang_code);
		}
		
		public function getLangNow(){
			return $this->lang_now;
		}
		
		public function loadLang(){
			global $lang;
			
			$lang_file = "lang.".str_replace("/","",$this->lang_code).".php";
			
			if(file_exists($lang_file))
				require_once $lang_file;
			else
				require_once "lang.ru.php";
		}
		
		public function getLang(){
			return $this->lang_now;
		}
				
		public function prepareSEO(){
			$routes = explode('/', $_SERVER['REQUEST_URI']);
			if($routes){
				$startnum = count(explode('/',SITE_URL));
				$rtnym = $startnum - 3;
						//echo $rtnym."<br>";
						//print_r($routes);
						//echo $routes[$rtnym+1]."<br";
						//echo $routes[$rtnym+2]."<br";
						//echo count($routes)." ".$startnum;
				if(count($routes) >= $startnum )
					if ( !empty($routes[count($routes)-2]))
					{	
						//echo "come";
						$startnum -= 1;
						$place = $routes[$rtnym+1];
						$ent = "`form_country`";
						$field = "country_id";
						$data_type = 0;
						$data_id = '';
						$name = backTranslit($place);
						$sql = "SELECT $field FROM $ent WHERE name LIKE '$name%'";
						//echo $sql."<br>";
						$db = DB::getInstance();
						
						$result = $db->query($sql);
						
						if ($db->num_rows($result)) {
							$result = mysql_fetch_assoc($result);
							if(count($routes) > $startnum+1){
								$data_id = $result['country_id'];
								$place = $routes[$rtnym+2];
								$name = backTranslit($place);
								$ent = "`form_region`";
								$field = "country_id, region_id";
								$data_type = 1;
								$sql = "SELECT $field FROM $ent WHERE name LIKE '$name%' AND country_id = $data_id";
								$result = $db->query($sql);
								//echo $sql."<br>";
								if ($db->num_rows($result)) {
									$result = mysql_fetch_assoc($result);
									if(count($routes) > $startnum+2  && strlen($routes[count($routes)-2]) > 2){
										$place = $routes[$rtnym+3];
										$name = backTranslit($place);
										$ent = "`form_city`";
										$data_id = $result['region_id'];
										$field = "country_id, region_id, city_id";
										$data_type = 2;
										$sql = "SELECT $field FROM $ent WHERE name LIKE '$name%' AND region_id = $data_id";
										//echo $sql."<br>";
										$result = $db->query($sql);
						
										if ($db->num_rows($result)) {
											$result = mysql_fetch_assoc($result);
											
										}
									}
								}
							}
							
						}
						//echo "$data_type";
						//print_r($result);
							/*echo $_COOKIE['country_id']."<br>";
								echo $_COOKIE['region_id']."<br>";
								echo $_COOKIE['city_id']."<br>";*/
						switch($data_type){
							case 0:
								setcookie('country_id',  $result['country_id'], time() + 31536000, COOKIE_WAY);
								setcookie('region_id', 0, time() + 31536000, COOKIE_WAY);
								setcookie('city_id', 0, time() + 31536000, COOKIE_WAY);
								$_COOKIE['country_id'] = $result['country_id'];
								$_COOKIE['region_id'] = 0;
								$_COOKIE['city_id'] = 0;
								break;
							case 1:
								setcookie('country_id',  $result['country_id'], time() + 31536000, COOKIE_WAY);
								setcookie('region_id', $result['region_id'], time() + 31536000, COOKIE_WAY);
								setcookie('city_id', 0, time() + 31536000, COOKIE_WAY);
								$_COOKIE['country_id'] = $result['country_id'];
								$_COOKIE['region_id'] =$result['region_id'];
								$_COOKIE['city_id'] = 0;
								break;
							case 2:
								setcookie('country_id',  $result['country_id'], time() + 31536000, COOKIE_WAY);
								setcookie('region_id', $result['region_id'], time() + 31536000, COOKIE_WAY);
								setcookie('city_id',  $result['city_id'], time() + 31536000, COOKIE_WAY);
								$_COOKIE['country_id'] = $result['country_id'];
								$_COOKIE['region_id'] = $result['region_id'];
								$_COOKIE['city_id'] = $result['city_id'];
								//print_r($result);
								/*echo $_COOKIE['country_id']."<br>";
								echo $_COOKIE['region_id']."<br>";
								echo $_COOKIE['city_id']."<br>";*/
								break;
						}
						
						
						
					}
			}
		}
		
		
		private function makeRedir($addr){
			if(!$this->needRedir)
				echo "q";
			header( "HTTP/1.1 301 Moved Permanently" );
			header( "Location: {$addr}" );
			die;
		}
	}
?>