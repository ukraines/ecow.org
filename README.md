## Обновлен Sitemaps ##

Добавлена страница уточнения деталей компании

Залил на сервер обновленный файл functions.php в директории lib/ . На всякий случай сделал бэкап файла functions.1.php. Вы его удалите, если не нужен.
Генерация нового sitemaps происходит в админке. Там есть некая страница sitemaps_page() . Ее посещение и создает пачку файлов в cache/sitemaps и всю остальную магию тоже запускает. Я не запускал ничего, поскольку не знаю пароля админа. Но, думаю, там все нормально должно сгенерироваться.
Если это так, то потом сможете обновить гит-репо, чтобы все-таки он был актуальным.

## Пример CSV ##


```
#!csv
Поля включающие запятые должны быть обернуты в одинарные кавычки ''

страна,регион,населенный пункт,название,руководитель,телефон,полный адрес,емейл,сайт
Россия,Адыгея,Адыгейск,название,руководитель,1111111,полный адрес,mama@mail.com,site.ru
Латвия,Латвия,Алуксне,название,руководитель,1111111,'Бизнес центр "Технопарк-Рига ЛТД.", ул. мазарискас, дом 34, офис 78 ',mama@mail.com,site.ru
```


## Добавление карточки ##

доступно по урлу https://ecow.org/dev2/ro/new_company/

## Лимиты ##

Лимиты по отправке форм устанавливаются в конфиге - пока всем поставил 1 день. 
Лимиты для “уточнения” и “добавления компании” для админов отключены.

## Вопросы ##

**Как самостоятельно править шаблоны емейлов?**

Правится в 3 местах - языковой файл, dev2\assets\chunks\parametrs_tpl, frontend.php.
2 шаблона, один для показаний, другой для сообщений

**Что происходит после отправки уточненных данных о какой-либо компании гостем?** 
Попадает в check_details таблицу и в логи. 

**Как посмотреть журнал?**
[https://ecow.org/dev2/phpminiadmin.php?XSS=37E1C1EA00Adf1&db=ecoworg_dev2&q=select+*+from+`log_events`](https://ecow.org/dev2/phpminiadmin.php?XSS=37E1C1EA00Adf1&db=ecoworg_dev2&q=select+*+from+`log_events`)

**Как поменять настройки контроля логов**
[https://ecow.org/dev2/phpminiadmin.php?XSS=5d8A89d2Dfe7874&db=ecoworg_dev2&q=select+*+from+`log_settings` ](https://ecow.org/dev2/phpminiadmin.php?XSS=5d8A89d2Dfe7874&db=ecoworg_dev2&q=select+*+from+`log_settings` )

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact