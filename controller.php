<?php

namespace CabinetForm;

use CabinetForm\Admin;
use CabinetForm\Frontend;

$data_type = -1;	//0 - country, 1 - region, 2 - city
$isAdmin = false;
$lang_now = "Русский";
$row_numbers = 0;


if(SITE_MODE == "DEV"){
	$country_code = "UA";	//for dev
	$ip = $_SERVER['REMOTE_ADDR'];	
	
}else{
	$country_code = $_SERVER["HTTP_CF_IPCOUNTRY"];
	$ip = get_client_ip();
}

$lang_code = $country_code;




$preparator = new Preparator($ip, $country_code);			
													//order is important!!!
$preparator->prepareAdmin();
$preparator->cancelCache();
$preparator->prepareFirstTimeByLink();
$preparator->prepareCountryCode();
$preparator->prepareCookieLang();
$preparator->prepareLang();
$preparator->loadLang();
$preparator->prepareSEO();

$isAdmin = $preparator->isAdmin();
$lang_code = $preparator->getLangCode();
$lang_now =  $preparator->getLangNow();

//print_r($_POST);
//echo "LANG_NOW = $lang_now<br>";
//echo "LANG_CODE = $lang_code<br>";

$company = isset($_REQUEST['q'])?trim($_REQUEST['q']):'';
if ($company != '') {
	$parts = preg_split('#/#', $company, -1, PREG_SPLIT_NO_EMPTY);
    if ($parts[0] == 'admin') {
        $admin = new Admin();
        $method = isset($parts[1])?$parts[1]:'home';
		$method .= '_page';
		//echo $method;
        if (method_exists($admin, $method)) {
            $admin->$method();
        } else {
            $admin->home_page();
        }
    } else {
        
        //$company = getCompanyDetails($company);
        
        $method = 'home';
		$num = 1;
		if( strpos($_REQUEST['q'],"json") !== false )
			$num = 0;
        if (isset($parts[$num]) && strlen($parts[$num])) {			//here is bug with json and static pages for static need number 1 because now we have /%lang%/ also
            $method = $parts[$num];
            $method .= '_page';
			//echo $_REQUEST['q']."<br>";
			//echo $method."<br>";
			//echo $parts[5]."<br>";
			//echo count($parts)."<br>";
			//exit;
            if (method_exists('CabinetForm\Frontend', $method)) {
				Frontend::$method();
            }
			//echo "params1";
            if (isset($parts[5]) && preg_match('/counters.html$/', $_REQUEST['q'])) {
				//echo "params2";
                Frontend::send_parameters_page();
            }
            if (isset($parts[5]) && preg_match('/request.html$/', $_REQUEST['q'])) {
				 Frontend::send_request_page();
            }
			if (isset($parts[5]) && preg_match('/details.html$/', $_REQUEST['q'])) {
                Frontend::send_details_page();
            }
            if (isset($parts[4]) && count($parts) == 5 && preg_match('/.html$/', $_REQUEST['q'])) {
                Frontend::company_page();
            }
        }
        Frontend::home_page();
    }
}



if (!is_array($company)) {
    Frontend::home_page();
}

