<?php

$start_time = microtime(true);
require_once 'configs.php';
require_once 'libs/db.php';
require_once 'libs/template.php';
require_once 'libs/functions.php';
require_once 'libs/frontend.php';
require_once 'libs/preparator.php';
require_once 'libs/admin.php';
require_once 'libs/places.php';
require_once 'controller.php';
