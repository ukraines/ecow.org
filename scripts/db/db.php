<?
include_once("config.php");
class DB{
	
	private $link;
	private static $db;
	
	private function __construct(){
		global $hostDb;
		global $loginDb;
		global $passDb;
		global $nameDb;
		$this->link = mysql_connect($hostDb,$loginDb,$passDb);
		mysql_select_db($nameDb);				//название бд
		mysql_set_charset("utf8");
	}
	
	public static function getInstance(){
		if(self::$db === null){
			self::$db = new DB();
			//echo "<h3 style='color:green;'>create</h3>";
		}else{
			//echo "<h3 style='color:blue;'>create before</h3>";
		}
		return self::$db;
	}
	
	public function select($str){								//false если не открыто соединение, если не верный запрос, если строк 0
		if($this->link !== null){
			$res = mysql_query($str);
			if($res){
				if(mysql_num_rows($res) === 0)
					return false;
				return $res;
			}	
			else
				return false;
		}
		return false;
	}
	
	public function insert($str){								//false если не открыто соединение, если не верный запрос, query вернул false
		if($this->link !== null){
			$res = mysql_query($str);
			if($res){
				return $res;
			}
			else
				return false;
		}
		return false;
	}
	
	public function update($str){								//false если не открыто соединение, если не верный запрос, query вернул false
		if($this->link !== null){
			$res = mysql_query($str);
			if($res){
				return $res;
			}
			else
				return false;
		}
		return false;
	}
	
	public function delete($str){								//false если не открыто соединение, если не верный запрос, query вернул false
		if($this->link !== null){
			$res = mysql_query($str);
			return $res;
		}
		return false;
	}
	
	public function close(){
		if(gettype($this->link) !== "unknown type" && $this->link !== null)
			mysql_close($this->link);
		self::$db = null;
		//echo "<h3 style='color:gray;'>close</h3>";
	}
	
	public function __destruct(){
		if(gettype($this->link) !== "unknown type" && $this->link !== null)
			mysql_close($this->link);
	}

}
?>