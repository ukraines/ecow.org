<?php

//ini_set('display_errors', 1);
define('ENGINE_ON', true);
define('ROOT_DIR', dirname(__FILE__));
define('DATA_FILE', ROOT_DIR . '/../companies');
define('FILELOCATION', ROOT_DIR . "/../report.txt");
// Тема письма будет проставляться автоматически
define("SUBJECT","Показания счетчиков");

define('ASSETS_DIR', ROOT_DIR . '/assets');
define('CACHE_DIR', ROOT_DIR . '/cache');
define('LOADS_DIR', ASSETS_DIR . '/loads/');

define('TEMPLATE_DIR', ASSETS_DIR . '/html');
define('PARTS_DIR', ASSETS_DIR . '/parts');
define('PARTS_USER_DIR', ASSETS_DIR . '/parts/user');
define('PARTS_ADMIN_DIR', ASSETS_DIR . '/parts/admin');
define('COUNTRIES_FILE', 'countries');
define('COUNTRIES_BY_COUNTRY_ID_FILE', 'countries_by_country_id');
define('REGIONS_FILE', 'regions');
define('CITIES_FILE', 'cities');
define('CITIES_BY_CITY_ID_FILE', 'cities_by_city_id');
define('REGIONS_BY_COUNTRIES_FILE', 'regions_by_countries');
define('REGIONS_BY_REGION_ID_FILE', 'regions_by_region_id');
define('CITIES_BY_REGIONS_FILE', 'cities_by_regions');
define('CITIES_BY_COUNTRIES_FILE', 'cities_by_countries');
define('COMPANIES_BY_COUNTRIES_FILE', 'companies_by_countries');
define('COMPANIES_BY_REGIONS_FILE', 'companies_by_regions');
define('COMPANIES_BY_CITIES_FILE', 'companies_by_cities');
define('COMPANIES_FILE', 'companies');
define('COMPANY_DIR', 'company');
define('SITEMAPS_DIR', 'sitemaps');
define('ITEMS_NUMBER', 12);
define('ITEMS_SUGGESTS_NUMBER', 9);
define('CHARSET', 'UTF-8');
define('COMPANY_NAME_LENGTH', 28);
define('COMPANY_ADDRESS_LENGTH', 33);
define('SITE_USER', 'admin');
define('SITE_PASSWORD', '1cfa7170e780d7bdf3fc7d7dbf66ae73b4f0aca1');//sha1 hash value  6xCKna7Dj8NW


define('ERROR_TYPE_WARNING', 'alert-warning');
define('ERROR_TYPE_SUCCESS', 'alert-success');

define('SITE_MODE', 'DEV');

define('ADMIN_IP', '127.0.0.1;37.212.129.192;94.248.26.19;104.236.117.174');
define('COOKIE_WAY', '/');
if (SITE_MODE == 'DEV') {
    //define('DB_HOST', 'rustam:3307');
   // define('DB_USERNAME', 'root');
   // define('DB_PASSWORD', 'tritonnt');
   // define('DB_NAME', 'cab_form');
    define('DB_HOST', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', '');
    define('DB_NAME', 'ecow');
    define('ADMIN_EMAIL', 'rustamonline@gmail.com');
    define('SITE_URL', 'http://ecow.loc/dev2/');
	define('PATH_URL', '/dev2/');
	
} else {
    //эти данные используется на сервере
    define('DB_HOST', 'localhost');
    define('DB_USERNAME', 'ecoworg_dev2');
    define('DB_PASSWORD', '2TquEwpnbF');
    define('DB_NAME', 'ecoworg_dev2');
    define('ADMIN_EMAIL', 'yuriy.yatsiv@gmail.com');
	define('PATH_URL', '/dev2/');
    define('SITE_URL', 'https://ecow.org/dev2/');
}

mb_internal_encoding(CHARSET);

define('DEFAULT_COUNTRY', 3159);
define('JSON', 'json');

define('TABLE_CITY', 'form_city');
define('TABLE_COUNTRY', 'form_country');
define('TABLE_REGION', 'form_region');
define('TABLE_COMPANY', 'form_company');

define('TABLE_REQUEST', 'send_requests');
define('TABLE_DETAIL', 'check_details');
define('TABLE_PARAMETR', 'send_parametrs');
define('TABLE_BLOCK', 'blocked_companies');

define('TABLE_LOG', 'log_events');
define('TABLE_LOG_SETTINGS', 'log_settings');

define('REQ_EVENT', 're_ev');
define('PARAM_EVENT', 'pa_ev');
define('BLOCK_EVENT', 'bl_ev');
define('UNBLOCK_EVENT', 'ub_ev');
define('DETAIL_EVENT', 'de_ev');
define('ADD_NEW_EVENT', 'ad_ev');

define('ALL_STATUS', 'all_st');
define('CHECK_STATUS', 'ch_st');
define('WAIT_STATUS', 'wt_st');
define('BAD_STATUS', 'bd_st');
define('BLOCK_STATUS', 'bl_st');

define('REQUEST_LIMIT', 86400);
define('VALUES_LIMIT', 86400);
define('DETAILS_LIMIT', 86400);
define('ADD_LIMIT', 86400);

define('RANDI',md5(rand(-100,100)));

define('EMAIL_SEND_OVER_GATEWAY', 1);
define('MANDRILL_API_KEY', 'SGLFwbyzJB7YHIs7YdKp0g');

$allowed_countries = array(3159, 9908, 2788, 248, 1280, 2514, 2448);
$search_bad_words = array('оо', 'ооо', 'ук');