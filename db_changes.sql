CREATE TABLE IF NOT EXISTS `check_details` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `manager` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `who` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `bdate` date NOT NULL,
  `company_id` int(7) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `site` varchar(100) NOT NULL,
  `name_sender` varchar(100) NOT NULL,
  `mail_sender` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `form_company_temp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `manager` varchar(200) NOT NULL DEFAULT '',
  `short_name` varchar(200) NOT NULL,
  `name` varchar(500) NOT NULL,
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `region_id` int(10) unsigned NOT NULL DEFAULT '0',
  `city_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(500) NOT NULL DEFAULT '',
  `phone` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `site` varchar(200) NOT NULL DEFAULT '',
  `name_sender` varchar(100) NOT NULL,
  `mail_sender` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `company_id_original` int(7) NOT NULL,
  `who` varchar(100) NOT NULL,
  `bdate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `region_id` (`region_id`,`city_id`),
  KEY `country_id` (`country_id`),
  KEY `manager` (`manager`),
  KEY `name` (`name`(333)),
  KEY `position` (`position`),
  KEY `address` (`address`(333)),
  KEY `phone` (`phone`),
  KEY `email` (`email`),
  FULLTEXT KEY `short_name` (`short_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blocked_companies` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `companyid` int(6) NOT NULL,
  `who` varchar(50) NOT NULL,
  `why` varchar(100) NOT NULL,
  `why_more` text NOT NULL,
  `bdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `form_company` ADD  `add_date` DATETIME NOT NULL , ADD  `mod_date` DATETIME NOT NULL;
ALTER TABLE  `form_company_temp` ADD  `comment`  varchar(255) DEFAULT '', ADD  `main_form_id` int(8) NOT NULL;
ALTER TABLE `form_company` ADD `status` varchar(5) NOT NULL DEFAULT '';
ALTER TABLE `check_details` ADD `ip` varchar(50) NOT NULL DEFAULT '';
ALTER TABLE `form_company` ADD `isblocked` tinyint(1) NOT NULL;
ALTER TABLE  `form_company` CHANGE  `phone`  `phone` VARCHAR( 51 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  '';
